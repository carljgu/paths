# Paths
### A choose your own adventure game
2023 Callum Gran and Carl Johan Gützkow

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Further development](#further-development)
- [Documentation](#documentation)
- [License](#license)

## Description

A JavaFX application for the semester project in Programming 2 IDATT2001

<div align="center">
  <img src="https://gitlab.stud.idi.ntnu.no/carljgu/paths/-/raw/main/gif_paths.gif" width="900">
</div>

## Installation

To be able to run and install it properly, you will need to have the following programs installed on you computer. To do this there are two different options, both of which are dependent on the user having a JDK.

### Java Development Kit
Java Development Kit (JDK) is needed to run this program. You will at least need version 1.7 or higher to run this program. To install JDK, you can go to the [JDK download page](https://www.oracle.com/java/technologies/downloads/#jdk18-windows). Here you will need to download and install the suitable JDK for you operating system.

<br />

#### Installation Option 1 ###

---
### Running from JAR file
This project is available in a cross platform executable JAR file. The only requirement is the JDK mentioned above and 10mb of storage space. Press the following link to download the JAR file and run it on your computer. Simple as.

[JAR-file download link](https://drive.google.com/file/d/1b17lqXXp6jISHrnwaXw-WgLnhHNmGsTz/view?usp=sharing).

<br />

#### Installation Option 2 ###

### Git
You will need Git to be able to clone the repository to your own computer.

[Here is the download page for Git](https://git-scm.com/downloads).

### Maven
Maven is a build tool used for dependencies in Java. This means that you have to install maven on your computer to run Paths.

[Maven download page](https://maven.apache.org/download.cgi)

[Maven installation guide](https://maven.apache.org/install.html)

### SSH key
A SSH is needed to clone the repository. Here are tutorials on how to set up a SSH key on you GitLab profile. This tutorial also contains a Git tutorial.

[MacOS tutorial](https://medium.com/devops-with-valentine/2021-how-to-setup-your-ssh-key-for-gitlab-on-macos-dfccec6904fb#:~:text=Copy%20the%20entire%20contents%20of,and%20finally%20click%20Add%20key)

[Windows tutorial](https://medium.com/devops-with-valentine/2021-how-to-your-ssh-key-for-gitlab-on-windows-10-587579192be0)

### Cloning the repository
After downloading all the required software above, you can now begin this step.

Option 1:
Download the project as a zip and extract it to the desired location. Then open the folder in the terminal and write this command:

`mvn javafx:run`

Option 2:
Create a folder where you want to install Paths. Then you must open the folder in the terminal. When you have opened the folder in the terminal, you must write these two commands in order:

`git clone https://gitlab.stud.idi.ntnu.no/carljgu/paths.git`

`mvn javafx:run`

After writing `mvn javafx:run`, the program should open.

## Usage

To use the program simply download it and read the tutorial in the application.
There are example files in the folder example_files which can be loaded in to the program to see how it works. The files are in a custom format, and the program will not be able to read other files. The files are also not very well written, and are only meant to be used as examples, although they are fully functional.

## Further development

GUI and more storylines are a must for further development. Currently the interface is functional, but not very pretty. The storylines are also very short and not very interesting. The game is also very linear, and there is no way to go back and change your choices. This is something that should be implemented in the future. The story editor is also not as intuitive as it could be, and should be improved. A database for storing the stories would also be a good idea, as it would make it easier to share stories with other users and you would only need to import the story once. Currently the stories are stored in a custom formatted file, and the program reads the file every time you want to play the story. This is not very efficient, and a database would be a better solution.

## Documentation

The java documentation for this project can be found [here](https://carljgu.pages.stud.idi.ntnu.no/paths/edu.ntnu.idatt2001.paths/module-summary.html).

## License

MIT License. See [LICENCE](./LICENCE) file
