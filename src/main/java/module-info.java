/**
 * The module-info.java file is used to declare the module name and the packages that are exported.
 * The module name is the same as the project name.
 *
 * @author Callum G., Carl G.
 * @version 1.3 22.05.2023
 */
module edu.ntnu.idatt2001.paths {
  requires transitive javafx.controls;
  requires transitive javafx.graphics;
  requires transitive javafx.base;

  exports edu.ntnu.idatt2001.paths;
  exports edu.ntnu.idatt2001.paths.views;
  exports edu.ntnu.idatt2001.paths.views.startmenu;
  exports edu.ntnu.idatt2001.paths.views.storyplayer;
  exports edu.ntnu.idatt2001.paths.views.storystarter;
  exports edu.ntnu.idatt2001.paths.views.storyimporter;
  exports edu.ntnu.idatt2001.paths.views.components;
  exports edu.ntnu.idatt2001.paths.views.tutorial;
  exports edu.ntnu.idatt2001.paths.views.storyeditor;
  exports edu.ntnu.idatt2001.paths.models;
  exports edu.ntnu.idatt2001.paths.models.player;
  exports edu.ntnu.idatt2001.paths.models.goals;
  exports edu.ntnu.idatt2001.paths.models.game;
  exports edu.ntnu.idatt2001.paths.models.actions;
  exports edu.ntnu.idatt2001.paths.models.tutorial;
  exports edu.ntnu.idatt2001.paths.exceptions;
  exports edu.ntnu.idatt2001.paths.file;
  exports edu.ntnu.idatt2001.paths.controllers;
  exports edu.ntnu.idatt2001.paths.controllers.storyeditor;
  exports edu.ntnu.idatt2001.paths.controllers.playgame;
  exports edu.ntnu.idatt2001.paths.controllers.storyimporter;
  exports edu.ntnu.idatt2001.paths.controllers.storystarter;
  exports edu.ntnu.idatt2001.paths.controllers.startmenu;
  exports edu.ntnu.idatt2001.paths.validation;
}
