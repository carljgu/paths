package edu.ntnu.idatt2001.paths.file;

import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.models.tutorial.TutorialParagraph;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * A class to read the tutorial text file.
 * The file should be formatted as follows: Title;Text;Image path
 * Follows the singleton design pattern to not load in the file multiple times if it is already loaded in.
 * @author Carl G.
 * @version 1.0 22.05.2023
 */
public class TutorialFileReader {

  private final String DELIMITER = ";";
  private static TutorialFileReader instance;
  private final List<TutorialParagraph> tutorialParagraphs;

  /**
   * Creates a new TutorialFileReader.
   * A private constructor to prevent instantiation.
   */
  private TutorialFileReader() {
    tutorialParagraphs = new ArrayList<>();
  }

  /**
   * Returns the instance of the TutorialFileReader.
   * @return The instance of the TutorialFileReader.
   */
  public static TutorialFileReader getInstance() {
    if (instance == null) instance = new TutorialFileReader();
    return instance;
  }

  /**
   * Reads the tutorial text file and converts it into tutorial paragraphs.
   * Any errors in reading the tutorial file will be ignored to continue reading the file.
   * If an image path is not provided, the image path will be set to null.
   * If the file has already been read, the list of paragraphs will be returned.
   * @param file The tutorial file to read.
   * @return A list of tutorial paragraphs.
   * @throws IOException if an I/O error occurs
   * @throws WrongFileExtensionException if the file extension is not .csv
   */
  public List<TutorialParagraph> readParagraphsFromFile(String file)
    throws IOException, WrongFileExtensionException {
    if (!tutorialParagraphs.isEmpty()) return tutorialParagraphs;

    try (
      final BufferedReader reader = new BufferedReader(
        new InputStreamReader(getClass().getResourceAsStream(file))
      )
    ) {
      reader
        .lines()
        .map(line -> line.split(DELIMITER))
        .forEach(split -> {
          try {
            String imagePath = null;
            if (split.length > 2) {
              imagePath = split[2].trim();
            } else if (split.length < 2) {
              return;
            }

            final TutorialParagraph paragraph = new TutorialParagraph(
              split[0].trim(),
              split[1].trim(),
              imagePath
            );

            tutorialParagraphs.add(paragraph);
          } catch (Exception ignoredException) {}
        });

      return tutorialParagraphs;
    }
  }
}
