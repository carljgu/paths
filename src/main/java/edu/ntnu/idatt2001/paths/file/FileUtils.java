package edu.ntnu.idatt2001.paths.file;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import java.io.File;
import java.util.Objects;

/**
 * Class for file utilities.
 * Collection of static methods for various file operations.
 *
 * @author Callum G.
 * @version 0.2 29.04.2023
 */
public class FileUtils {

  /**
   * Method to get the file extension from a file name.
   * @param <T> is a generic type that extends File.
   * @param file T - the file to get the extension from
   * @return String - the file extension without the dot
   * @throws BadFileNameException if the file name does not contain an extension
   * @throws NullPointerException if the file name is null
   */
  public static <T extends File> String getFileExtension(final T file)
    throws BadFileNameException, NullPointerException {
    final String fileName = Objects.requireNonNull(file, "The file name cannot be null").getName();
    if (!fileName.contains(".")) {
      throw new BadFileNameException("The file name does not contain an extension");
    }
    return fileName.substring(fileName.lastIndexOf(".") + 1);
  }
}
