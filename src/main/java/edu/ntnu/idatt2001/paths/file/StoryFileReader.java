package edu.ntnu.idatt2001.paths.file;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import edu.ntnu.idatt2001.paths.exceptions.StoryFormatException;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.ActionTypeEnum;
import edu.ntnu.idatt2001.paths.models.actions.GoldAction;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryAddAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAllAction;
import edu.ntnu.idatt2001.paths.models.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.validation.StoryFileValidation;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handles reading stories to and from a file.
 * Also keeps track of a list of passages, links and actions
 * that are skipped because of bad formatting and the
 * line number of the file they were on.
 *
 * @author Carl G. Callum G.
 * @version 0.4 29.04.2023
 */
public class StoryFileReader {

  private final List<String> actionsSkipped;
  private final List<String> linksSkipped;
  private final List<String> passagesSkipped;
  private final File file;
  private int currentLine;

  /**
   * Constructor for the story file reader.
   * Checks if the file extension is valid.
   * Initializes the list of passages skipped.
   *
   * @param file  File - the file to read from
   * @throws WrongFileExtensionException thrown if the file extension is not .paths
   * @throws BadFileNameException thrown if the file name does not contain an extension
   * @throws NullPointerException thrown if the file name is null
   */
  public StoryFileReader(final File file) throws WrongFileExtensionException, BadFileNameException {
    if (!StoryFileValidation.isValidFileType(file)) throw new WrongFileExtensionException(
      "The file extension is not ." + FileExtensionEnum.PATHS.getExtension()
    );

    this.file = file;
    this.currentLine = 1;
    this.actionsSkipped = new ArrayList<>();
    this.linksSkipped = new ArrayList<>();
    this.passagesSkipped = new ArrayList<>();
  }

  /**
   * Method to read the next action from the file.
   * Actions must be formatted as following:
   * <pre>
   * {ActionClass}(ActionValue)
   * </pre>
   * @return Action - the next action.
   * @throws StoryFormatException if the action is not formatted correctly
   * @throws NumberFormatException if the action value is not a number
   */
  private Action<?> readNextAction(final String action)
    throws StoryFormatException, NumberFormatException {
    final String actionTypeString = action.substring(1, action.indexOf("}")).trim();
    final String actionValueString = action
      .substring(action.indexOf("(") + 1, action.indexOf(")"))
      .trim();

    switch (ActionTypeEnum.getActionType(actionTypeString)) {
      case GOLD -> {
        return new GoldAction(Integer.parseInt(actionValueString));
      }
      case HEALTH -> {
        return new HealthAction(Integer.parseInt(actionValueString));
      }
      case INVENTORY_ADD -> {
        return new InventoryAddAction(actionValueString);
      }
      case INVENTORY_REMOVE_ALL -> {
        return new InventoryRemoveAllAction(actionValueString);
      }
      case SCORE -> {
        return new ScoreAction(Integer.parseInt(actionValueString));
      }
      default -> {
        throw new StoryFormatException();
      }
    }
  }

  /**
   * Method to read the next link from the file.
   * Links must be formatted as any of the following:
   * <pre>
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   * </pre>
   * @return Link - the next link.
   * @throws StoryFormatException if the link is not formatted correctly
   */
  private Link readNextLink(final String line) throws StoryFormatException {
    final String linkTitle = line.substring(1, line.indexOf("]"));
    final String linkPassage = line.substring((line.indexOf("(")) + 1, line.indexOf(")"));

    final ArrayList<Action<?>> actions = new ArrayList<>();

    if (StoryFileValidation.containsActions(line)) {
      final String actionsString = line.substring(line.indexOf("<") + 1, line.indexOf(">"));
      if (actionsString.contains("|")) {
        for (final String action : actionsString.split("\\|")) {
          if (StoryFileValidation.isValidAction(action)) {
            try {
              actions.add(readNextAction(action));
            } catch (StoryFormatException e) {
              this.actionsSkipped.add(
                  "Line " + currentLine + ": " + action + " is not a valid action class."
                );
            } catch (NumberFormatException e) {
              this.actionsSkipped.add(
                  "Line " + currentLine + ": " + action + " did not contain a number as a value."
                );
            }
          } else {
            this.actionsSkipped.add(
                "Line " + currentLine + ": " + action + " did not have a valid format."
              );
          }
        }
      } else {
        if (StoryFileValidation.isValidAction(actionsString)) {
          try {
            actions.add(readNextAction(actionsString));
          } catch (StoryFormatException e) {
            this.actionsSkipped.add(
                "Line " + currentLine + ": " + actionsString + " was not a valid action class."
              );
          } catch (NumberFormatException e) {
            this.actionsSkipped.add(
                "Line " +
                currentLine +
                ": " +
                actionsString +
                " did not contain a number as a value."
              );
          }
        } else {
          this.actionsSkipped.add(
              "Line " + currentLine + ": " + actionsString + " did not have a valid format."
            );
        }
      }
    }

    return new Link(linkTitle, linkPassage, actions);
  }

  /**
   * Method to read the next passage from the file.
   * Passages must atleast have a title as follows:
   * <pre>
   * ::Title
   *
   *
   * </pre>
   * The title must be on a new line and start with "::" and directly followed by the title.
   * But it can also have a content as follows:
   * <pre>
   * ::Title
   * Content
   *
   *
   * </pre>
   * The body must be on a new line and directly follow the title.
   * It can also have indefinite amount of links after the content as follows:
   * <pre>
   * ::Title
   * Content
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   *
   * </pre>
   * @return Passage - the next passage.
   * @throws StoryFormatException if the passage is not formatted correctly
   */
  private Passage readNextPassage(final String passage) {
    String passageTitle;
    String passageText = "";
    currentLine++;

    String[] passageArray = passage.split(System.lineSeparator());

    if (StoryFileValidation.isValidPassageStart(passageArray[0])) {
      passageTitle = passageArray[0].substring(2);
      currentLine++;
    } else {
      currentLine += passageArray.length;
      throw new StoryFormatException(
        "Line " +
        (currentLine - (passageArray.length - 1)) +
        ": " +
        passage +
        " -> did not start with a valid passage start."
      );
    }

    if (passageArray.length > 1) {
      passageText = passageArray[1];
      currentLine++;
    }

    final List<Link> links = new ArrayList<>();

    for (int i = 2; i < passageArray.length; i++) {
      if (StoryFileValidation.isValidLink(passageArray[i])) {
        try {
          links.add(readNextLink(passageArray[i]));
        } catch (NullPointerException | IllegalArgumentException e) {
          this.linksSkipped.add(
              "Line " +
              currentLine +
              ": " +
              passageArray[i] +
              " was not a valid link because: " +
              e.getMessage()
            );
        }
      } else {
        this.linksSkipped.add(
            "Line " + currentLine + ": " + passageArray[i] + " did not have a valid format."
          );
      }
      currentLine++;
    }

    return new Passage(passageTitle, passageText, links);
  }

  /**
   * Method to the story from the file.
   * Stories must be formatted as follows with atleast a title and an opening passage:
   * <pre>
   * Story Title
   *
   * ::Passage 1
   * Passage 1 Content
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)>
   * [Link](Passage)
   *
   * ::Passage 2
   * Passage 2 Content
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   * ::Passage 3
   * Passage 3 Content
   * [Link](Passage)
   *
   * </pre>
   * @return Story - the story in the file.
   * @throws StoryFormatException if the file is not formatted correctly
   * @throws IOException if the file cannot be read
   */
  public Story readStory() throws StoryFormatException, IOException {
    String title;
    Passage openingPassage;
    Stream<String> lines = null;
    String[] storyStrings;

    try (
      BufferedReader reader = new BufferedReader(new FileReader(file, Charset.forName("UTF-8")))
    ) {
      String line;

      if ((line = reader.readLine()) != null) {
        title = line;
      } else {
        throw new StoryFormatException("The file is empty.");
      }

      lines = reader.lines();
      storyStrings =
        lines
          .collect(Collectors.joining(System.lineSeparator()))
          .split(System.lineSeparator() + System.lineSeparator());
    } catch (IOException e) {
      throw new IOException(
        "Could not read the characters from the file: '" + file.getPath() + "'."
      );
    }

    try {
      openingPassage = readNextPassage(storyStrings[0].trim());
    } catch (StoryFormatException e) {
      passagesSkipped.add(e.getMessage());
      throw new StoryFormatException("The file does not contain a valid opening passage.");
    }

    final Map<Link, Passage> passages = new HashMap<>();
    final Story story = new Story(title, passages, openingPassage);

    Arrays
      .stream(storyStrings, 1, storyStrings.length)
      .forEach(passage -> {
        try {
          story.addPassage(readNextPassage(passage.trim()));
        } catch (StoryFormatException e) {
          passagesSkipped.add(e.getMessage());
        }
      });

    return story;
  }

  /**
   * Gets the list of passages
   * that were skipped when reading from file.
   *
   * @return passagesSkipped - List - amount of lines skipped when
   * reading story from file because of invalid passage.
   */
  public List<String> getPassagesSkipped() {
    return passagesSkipped;
  }

  /**
   * Gets the list of links
   * that were skipped when reading from file.
   *
   * @return linksSkipped - List - amount of lines skipped when
   * reading story from file because of invalid link.
   */
  public List<String> getLinksSkipped() {
    return linksSkipped;
  }

  /**
   * Gets the list of actions
   * that were skipped when reading from file.
   *
   * @return actionsSkipped - List - amount of lines skipped when
   * reading story from file because of invalid action.
   */
  public List<String> getActionsSkipped() {
    return actionsSkipped;
  }

  /**
   * Gets the file that was read from.
   *
   * @return file - File - the file that was read from.
   */
  public File getFile() {
    return file;
  }
}
