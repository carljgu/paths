package edu.ntnu.idatt2001.paths.file;

/**
 * Enum for the different valid file extensions.
 * Extensions are stored as strings and do not include the dot.
 *
 * @author Callum G., Carl G.
 * @version 0.3 22.05.2023
 */
public enum FileExtensionEnum {
  /**
   * The extension for the story file.
   */
  PATHS("paths"),

  /**
   * The extension for the tutorial file.
   */
  TUTORIAL("csv");

  private final String extension;

  /**
   * Constructor for the enum.
   *
   * @param extension String - the extension
   */
  FileExtensionEnum(final String extension) {
    this.extension = extension;
  }

  /**
   * Method to get the extension.
   *
   * @return String - the extension
   */
  public String getExtension() {
    return extension;
  }
}
