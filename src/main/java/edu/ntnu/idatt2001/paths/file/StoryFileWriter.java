package edu.ntnu.idatt2001.paths.file;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.validation.StoryFileValidation;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * Class for writing a story to a file.
 *
 * @author Carl G. Callum G.
 * @version 0.4 29.04.2023
 */
public class StoryFileWriter {

  private final File file;

  /**
   * Constructor for the class.
   *
   * @param file File - the file to write to
   * @throws BadFileNameException thrown if the file name is invalid
   * @throws WrongFileExtensionException thrown if the file extension is invalid
   * @throws NullPointerException thrown if the file is null
   */
  public StoryFileWriter(final File file)
    throws BadFileNameException, WrongFileExtensionException, NullPointerException {
    if (!StoryFileValidation.isValidFileType(file)) throw new WrongFileExtensionException(
      "File must be of type '." + FileExtensionEnum.PATHS + "'."
    );
    this.file = file;
  }

  /**
   * Method to write an action to a file.
   * Writes in the following format:
   * <pre>
   * {ActionType}(ActionValue)
   * <pre>
   * @param action Action - the action to write
   * @param writer BufferedWriter - the writer to write to
   * @throws IOException thrown if the writer fails to write
   */
  private void writeAction(final Action<?> action, final BufferedWriter writer) throws IOException {
    writer.write("{" + action.getClass().getSimpleName() + "}(" + action.getValue() + ")");
  }

  /**
   * Method to write a link to a file.
   * Writes in the any of the following formats:
   * <pre>
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   * </pre>
   * @param link Link - the link to write
   * @param writer BufferedWriter - the writer to write to
   * @throws IOException if an error occurs while writing to the file
   */
  private void writeLink(final Link link, final BufferedWriter writer) throws IOException {
    writer.write("[" + link.getText() + "](" + link.getReference() + ")");

    if (link.getActions().size() > 0) {
      writer.write("<");
      final Iterator<Action<?>> iter = link.getActions().iterator();

      while (true) {
        writeAction(iter.next(), writer);
        if (iter.hasNext()) {
          writer.write("|");
        } else {
          break;
        }
      }

      writer.write(">");
    }

    writer.newLine();
  }

  /**
   * Method to write a passage to a file.
   * Writes in the following format:
   * Passages must atleast have a title as follows:
   * <pre>
   * ::Title
   *
   *
   * </pre>
   * The title must be on a new line and start with "::" and directly followed by the title.
   * But it can also have a content as follows:
   * <pre>
   * ::Title
   * Content
   *
   *
   * </pre>
   * The body must be on a new line and directly follow the title.
   * It can also have indefinite amount of links after the content as follows:
   * <pre>
   * ::Title
   * Content
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage){@literal <}{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   *
   * </pre>
   * @param passage Passage - the passage to write
   * @param writer BufferedWriter - the writer to write to
   * @throws IOException if an error occurs while writing to the file
   */
  private void writePassage(final Passage passage, final BufferedWriter writer) throws IOException {
    writer.write("::" + passage.getTitle());
    writer.newLine();
    writer.write(passage.getContent());
    writer.newLine();

    for (final Link link : passage.getLinks()) {
      writeLink(link, writer);
    }

    writer.newLine();
  }

  /**
   * Method to write a story to a file.
   * Writes in the following format:
   * <pre>
   * StoryTitle
   *
   * ::PassageTitle
   * PassageContent
   * [LinkText](LinkReference){@literal <}Action1|Action2|...{@literal >}
   *
   * ::PassageTitle
   * PassageContent
   * [LinkText](LinkReference){@literal <}Action1|Action2|...{@literal >}
   *
   * </pre>
   * @param story Story - the story to write
   * @throws IOException if an error occurs while writing to the file
   */
  public void writeStory(final Story story) throws IOException {
    try (
      BufferedWriter writer = new BufferedWriter(
        new FileWriter(this.file, Charset.forName("UTF-8"))
      )
    ) {
      writer.write(story.getTitle());
      writer.newLine();
      writer.newLine();
      Passage openingPassage = story.getOpeningPassage();
      Objects.requireNonNull(openingPassage, "Story must have an opening passage.");
      writePassage(openingPassage, writer);

      final List<Passage> reversedPassages = new ArrayList<>(story.getPassages().stream().toList());
      reversedPassages.remove(openingPassage);

      Collections.reverse(reversedPassages);

      for (final Passage passage : reversedPassages) {
        writePassage(passage, writer);
      }
    } catch (IOException e) {
      e.printStackTrace();
      throw new IOException("Failed to write to file: " + this.file.getName() + ".");
    }
  }

  /**
   * Method to get the file.
   *
   * @return File - the file
   */
  public File getFile() {
    return file;
  }
}
