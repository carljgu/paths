package edu.ntnu.idatt2001.paths.models.actions;

import edu.ntnu.idatt2001.paths.models.player.Player;

/**
 * Action interface.
 * Creates a method for executing an action
 * when a player goes through a link.
 * Actions modify a player's attributes.
 *
 * @author Carl G. Callum G.
 * @version 0.4 - 29.04.2023
 */
public interface Action<T> {
  /**
   * Executes an action on a player.
   * Modifies the player's attributes.
   *
   * @param player Player - the player to execute the action on
   * @throws NullPointerException thrown if player is null
   */
  void execute(final Player player) throws NullPointerException;

  /**
   * Get method for the action's value.
   *
   * @return T - the value of the action
   */
  T getValue();
}
