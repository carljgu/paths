package edu.ntnu.idatt2001.paths.models;

import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Story class.
 * Contains the passages and the links connect to them.
 * Also has the opening passage which also exists in the passages map.
 * The story is identified by its memory position and therefore no equals-method is implemented.
 * A story can exist without any passages, but must have a title.
 * If the opening passage is null or if the passage is not in the passages map, the story cannot be played.
 *
 * @author Carl G. Callum G.
 * @version 0.6 - 29.04.2023
 */
public class Story {

  private String title;
  private final Map<Link, Passage> passages;
  private Passage openingPassage;

  /**
   * Adds the opening passage to the map of passages.
   * The link is created with the title of the passage as the link text and reference.
   *
   * @param passage Passage - the opening passage of the story
   */
  private void addOpeningPassage(final Passage passage) {
    removePassage(new Link(passage.getTitle(), passage.getTitle()));
    addPassage(passage);
  }

  /**
   * The main constructor for the Story class.
   * Title cannot be blank or null and passages cannot be null, but can be empty.
   * opening passage can be null
   *
   * @param title          String - name of the story
   * @param passages       Map - links connected to passages
   * @param openingPassage Passage - passage to start the story with
   * @throws NullPointerException thrown if passages or title is null
   * @throws IllegalArgumentException thrown if the title is blank
   */
  public Story(final String title, final Map<Link, Passage> passages, final Passage openingPassage)
    throws NullPointerException, IllegalArgumentException {
    this.title =
      Validation.requireNonNullOrBlank(title, "Title cannot be null", "Title cannot be blank");
    this.openingPassage = openingPassage;
    this.passages =
      Validation.requireNonNullOrContainsNull(
        passages,
        "Map of passages cannot be null",
        "Map of passages cannot contain null values"
      );
    if (openingPassage != null) addOpeningPassage(openingPassage);
  }

  /**
   * Creates a new story with an empty passages map.
   *
   * @param title          String - name of the story
   * @param openingPassage Passage - passage to start the story with
   * @throws IllegalArgumentException thrown if title is either blank or null
   * @throws NullPointerException thrown if the title is null
   */
  public Story(final String title, final Passage openingPassage) throws IllegalArgumentException {
    this(title, new HashMap<>(), openingPassage);
  }

  /**
   * Retrieves the title of the story.
   *
   * @return String - title of the story
   */
  public String getTitle() {
    return title;
  }

  /**
   * Updates the title of the story.
   *
   * @param title String - new title of the story
   * @throws IllegalArgumentException thrown if the title is blank
   * @throws NullPointerException thrown if the title is null
   */
  public void setTitle(String title) throws IllegalArgumentException, NullPointerException {
    Objects.requireNonNull(title, "Title cannot be null");
    if (title.isBlank()) {
      throw new IllegalArgumentException("Title cannot be whitespace only");
    }
    this.title = title;
  }

  /**
   * Retrieves the passages of the story in the form
   * of a collection.
   *
   * @return Collection - a group of passages which are in the map of passages
   */
  public Collection<Passage> getPassages() {
    return passages.values();
  }

  /**
   * Adds a passage to the story.
   * A link is created with the title of the passage as the link text and reference.
   * The text of the link should be edited.
   * cannot add a passage which is null or already exists in the map.
   *
   * @param passage Passage - non-null passage to add to the story
   * @throws NullPointerException thrown if the passage is null
   * @throws IllegalArgumentException thrown if the passage already exists in the map
   */
  public void addPassage(final Passage passage)
    throws NullPointerException, IllegalArgumentException {
    if (
      passages.containsValue(Objects.requireNonNull(passage, "Passage cannot be null in the story"))
    ) throw new IllegalArgumentException("Passage already exists in the story");
    final Link link = new Link(passage.getTitle(), passage.getTitle());
    passages.put(link, passage);
  }

  /**
   * Removes a passage from the story given a passage
   * If the passage is the opening passage, the opening passage is set to null.
   *
   * @param passage Passage - passage to remove from the story
   * @return boolean - predicate if the passage was removed or not
   * @throws NullPointerException thrown if the passage is null
   */
  public boolean removePassageWithPassage(Passage passage) throws NullPointerException {
    Objects.requireNonNull(passage, "The passage to remove cannot be null");
    boolean removed = this.removePassage(new Link(passage.getTitle(), passage.getTitle()));
    if (removed && passage.equals(openingPassage)) openingPassage = null;
    return removed;
  }

  /**
   * Retrieves a passage form a given link.
   * If the link is not in the map, null is returned.
   *
   * @param link Link - a link which references a passage
   * @return Passage/null - the passage referenced by the link or null if the link is not in the map
   * @throws NullPointerException thrown if the link is null
   */
  public Passage getPassage(final Link link) throws NullPointerException {
    return passages.get(Objects.requireNonNull(link, "Link cannot be null"));
  }

  /**
   * Method to remove a link from the map of passages.
   * If the link is not in the map, false is returned.
   * The link cannot be removed if other passages reference it.
   *
   * @param link Link - a link which references a passage,
   * the link is removed from the map.
   * @return boolean - predicate if the link was removed or not
   * @throws NullPointerException thrown if the link is null
   */
  public boolean removePassage(final Link link) throws NullPointerException {
    Objects.requireNonNull(link, "Link cannot be null");
    if (
      passages.values().stream().flatMap(p -> p.getLinks().stream()).noneMatch(l -> l.equals(link))
    ) return Objects.nonNull(passages.remove(link));

    return false;
  }

  /**
   * Method to get all broken links in the story.
   * A link is broken if it references a passage which is not in the story.
   * If there are no broken links, an empty collection is returned.
   *
   * @return Collection - a collection of broken links
   */
  public Collection<Link> getBrokenLinks() {
    return passages
      .values()
      .stream()
      .flatMap(p -> p.getLinks().stream())
      .filter(l -> Objects.isNull(getPassage(l)))
      .toList();
  }

  /**
   * Retrieves the opening passage of the story.
   * This passage is also in the passages map.
   *
   * @return Passage/null - the opening passage of the story.
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * Sets the opening passage of the story.
   * The passage must be in the passages map.
   *
   * @param openingPassage Passage - the passage to set as the opening passage
   */
  public void setOpeningPassage(Passage openingPassage) throws IllegalArgumentException {
    if (!passages.containsValue(openingPassage)) throw new IllegalArgumentException(
      "Passage is not in the story"
    );
    this.openingPassage = openingPassage;
  }

  /**
   * Checks if the story is playable
   * A game is playable if there exists an opening passage and that passage is in the passages map.
   *
   * @return boolean - true if the story is ready to be played, false if not
   */
  public boolean isPlayable() {
    return openingPassage != null && passages.containsValue(openingPassage);
  }

  @Override
  public String toString() {
    return (
      "Story: " +
      getTitle() +
      " - " +
      getPassages().size() +
      " passages with opening passage: " +
      getOpeningPassage() +
      "\n" +
      "Story is " +
      (isPlayable() ? "" : "not") +
      " playable"
    );
  }
}
