package edu.ntnu.idatt2001.paths.models;

import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class Passage representing a point in a story
 * where a choice has to be made. A passage can have
 * several links which points to other passages.
 * The title of the passage uniquely identifies it
 * and the content describes a type of conflict that
 * needs to be resolved.
 *
 * @author Carl G. Callum G.
 * @version 0.3 - 29.04.2023
 */
public class Passage {

  private final String title;
  private final String content;
  private final List<Link> links;

  /**
   * The main constructor for the passage class.
   * None of the parameters should be null. Links can be empty,
   * but title cannot be.
   *
   * @param title   String - title of the passage
   * @param content String - description and content of the passage
   * @param links   List - paths that are available from this passage
   * @throws NullPointerException thrown if any values are null.
   * @throws IllegalArgumentException thrown if the title is empty.
   */
  public Passage(final String title, final String content, final List<Link> links)
    throws NullPointerException, IllegalArgumentException {
    this.title =
      Validation.requireNonNullOrBlank(title, "Title cannot be null", "Title cannot be blank");
    this.content = Objects.requireNonNull(content, "Content cannot be null");
    this.links =
      Validation.requireNonNullOrContainsNull(
        links,
        "List of links cannot be null",
        "List of links cannot contain null values"
      );
  }

  /**
   * Instantiates a new Passage, but only takes in title and content.
   * Uses the main constructor with an empty list of links.
   *
   * @param title   String - title of the passage
   * @param content String - description and content of the passage
   * @throws NullPointerException thrown if title or content is null.
   * @throws IllegalArgumentException thrown if title is empty.
   */
  public Passage(final String title, final String content)
    throws NullPointerException, IllegalArgumentException {
    this(title, content, new ArrayList<>());
  }

  /**
   * Instantiates a new Passage, but only takes in title.
   * Uses the main constructor with an empty list of links and empty content.
   *
   * @param title String - title of the passage
   * @throws NullPointerException thrown if title is null.
   * @throws IllegalArgumentException thrown if title is empty.
   */
  public Passage(final String title) throws NullPointerException, IllegalArgumentException {
    this(title, "");
  }

  /**
   * Retrieves the title of the passage
   *
   * @return String - title of the passage
   */
  public String getTitle() {
    return title;
  }

  /**
   * Retrieves the content of the passage
   *
   * @return String - description and content of the passage
   */
  public String getContent() {
    return content;
  }

  /**
   * Retrieves the links the passage has connected from it
   *
   * @return List - paths that are available from this passage
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * Adds a link to the list of links for the passage
   * Checks if the link is already in the list of links
   * Returns true if the link was successfully added, false if not
   *
   * @param link List - paths that are available from this passage
   * @return boolean - true if the link was successfully added, false if not
   * @throws NullPointerException thrown if link is null
   */
  public boolean addLink(final Link link) throws NullPointerException {
    if (links.contains(Objects.requireNonNull(link, "Link cannot be null"))) {
      return false;
    }

    return links.add(link);
  }

  /**
   * Removes a link from the list of links
   *
   * @param link Link - link to remove
   * @return boolean - true if the link was successfully removed, false if not
   */
  public boolean removeLink(Link link) {
    return links.remove(link);
  }

  /**
   * Checks if there is at least one link in the list of links
   *
   * @return boolean - true if there is at least one link, false if not
   */
  public boolean hasLinks() {
    return !links.isEmpty();
  }

  /**
   * Returns a string representation of the passage including
   * the title and the size of the list of links.
   *
   * Example:
   * "My first passage. Connected links: 2"
   *
   * @return String - string representation of the passage
   */
  @Override
  public String toString() {
    return title + ". Connected links: " + links.size();
  }

  /**
   * Checks whether two passages are equal by comparing their titles.
   * The title of a passage uniquely identifies it.
   *
   * @param o Object - the object to compare to
   * @return boolean - true if the objects are equal, false if not
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Passage passage)) return false;
    return getTitle().equals(passage.getTitle());
  }

  /**
   * Returns the hashcode of the title of the passage.
   *
   * @return int - hashcode of the passage
   */
  @Override
  public int hashCode() {
    return Objects.hash(getTitle());
  }
}
