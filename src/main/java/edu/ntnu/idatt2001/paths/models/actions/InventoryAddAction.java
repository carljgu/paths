package edu.ntnu.idatt2001.paths.models.actions;

import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.Objects;

/**
 * InventoryAddAction class.
 * Adds an item to a player's inventory when executed.
 * Which item to add is defined when instantiated.
 *
 * @author Carl G. Callum G.
 * @version 0.5 - 29.04.2023
 */
public class InventoryAddAction implements Action<String> {

  private final String item;

  /**
   * Instantiates a new Inventory add action
   * and sets the item to add to the player's inventory.
   *
   * @param item String - a non-null and non-empty string representing the item to add
   * @throws IllegalArgumentException thrown if the item is null or only whitespace
   */
  public InventoryAddAction(final String item) throws IllegalArgumentException {
    this.item =
      Validation.requireNonNullOrBlank(
        item,
        "Item cannot be null",
        "Item cannot be only whitespace"
      );
  }

  /**
   * Executes an action on a player.
   * Calls the addToInventory method on the player with a set item.
   *
   * @param player Player - the player to execute the action on
   * @throws NullPointerException if player is null
   * @see Action#execute(Player)
   */
  @Override
  public void execute(final Player player) throws NullPointerException {
    Objects.requireNonNull(player, "The player cannot be null").addToInventory(item);
  }

  /**
   * Get method for the action's value.
   *
   * @return String - the item of the action
   * @see Action#getValue()
   */
  @Override
  public String getValue() {
    return item;
  }
}
