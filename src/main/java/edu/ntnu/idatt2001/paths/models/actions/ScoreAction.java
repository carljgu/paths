package edu.ntnu.idatt2001.paths.models.actions;

import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.Objects;

/**
 * ScoreAction class.
 * Adds or subtracts points from a player's score when executed.
 * Number of points is defined when instantiated.
 *
 * @author Carl G. Callum G.
 * @version 0.5 - 29.04.2023
 */
public class ScoreAction implements Action<Integer> {

  private final int points;

  /**
   * Creates a new score action which adds or subtracts
   * points from a player's score.
   *
   * @param points int - the amount of points to add or subtract
   */
  public ScoreAction(final int points) {
    this.points = points;
  }

  /**
   * Executes an action on a player.
   * Calls the addScore method on the player with a set amount of points.
   *
   * @param player Player - the player to execute the action on
   * @throws NullPointerException if player is null
   * @see Action#execute(Player)
   */
  @Override
  public void execute(final Player player) throws NullPointerException {
    Objects.requireNonNull(player, "The player cannot be null").addScore(points);
  }

  /**
   * Get method for the action's value.
   *
   * @return Integer - the points of the action
   * @see Action#getValue()
   */
  @Override
  public Integer getValue() {
    return points;
  }
}
