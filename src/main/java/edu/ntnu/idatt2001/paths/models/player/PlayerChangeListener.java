package edu.ntnu.idatt2001.paths.models.player;

/**
 * A listener for player changes.
 * Used to notify when the player changes and sends the updated player.
 *
 * @see Player
 * @author Carl G.
 * @version 1.0 29.04.2023
 */
public interface PlayerChangeListener {
  /**
   * Called when the player changes any of its attributes.
   * @param player The updated player.
   */
  void onPlayerChange(Player player);
}
