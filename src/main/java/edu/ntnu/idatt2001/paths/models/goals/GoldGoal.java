package edu.ntnu.idatt2001.paths.models.goals;

import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.Objects;

/**
 * GoldGoal class.
 * Checks if a player has a more than or equal to a certain amount of gold.
 * The amount of gold for fulfillment is defined when instantiated.
 * The gold goal cannot be less than 1.
 *
 * @author Carl G. Callum G.
 * @version 0.4 - 29.04.2023
 */
public class GoldGoal implements Goal<Integer> {

  private final int minimumGold;

  /**
   * Creates a new gold goal which checks the
   * player's gold with the minimum gold.
   * Players start with at least 0 gold so the
   * goal for gold cannot be less than 1.
   *
   * @param minimumGold int - the amount of gold for fulfillment
   * @throws IllegalArgumentException thrown if the minimum gold is less than 1
   */
  public GoldGoal(final int minimumGold) throws IllegalArgumentException {
    this.minimumGold =
      Validation.requireLargerThan(minimumGold, 0, "The minimum gold cannot be less than 1");
  }

  /**
   * Checks if a player has fulfilled the gold goal.
   * Calls the getGold method on the player and checks if it is more than or equal to the minimum gold.
   *
   * @param player Player - the player to check the goal with.
   * @throws NullPointerException if player is null
   * @see Goal#isFulfilled(Player)
   */
  @Override
  public boolean isFulfilled(final Player player) throws NullPointerException {
    return Objects.requireNonNull(player, "The player cannot be null").getGold() >= minimumGold;
  }

  /**
   * Get the gold goal's minimum gold criteria.
   * @see Goal#getFulfillmentCriteria()
   * @return Integer - the minimum gold criteria
   */
  @Override
  public Integer getFulfillmentCriteria() {
    return minimumGold;
  }
}
