package edu.ntnu.idatt2001.paths.models.actions;

import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.Objects;

/**
 * HealthAction class.
 * Adds or subtracts health points from a player's health when executed.
 * Number of health points is defined when instantiated.
 *
 * @author Carl G. Callum G.
 * @version 0.5 - 29.04.2023
 */
public class HealthAction implements Action<Integer> {

  private final int healthPoints;

  /**
   * Creates a new health action which adds or subtracts
   * health points from a player's health.
   *
   * @param healthPoints int - the amount of health points to add or subtract
   */
  public HealthAction(final int healthPoints) {
    this.healthPoints = healthPoints;
  }

  /**
   * Executes an action on a player.
   * Calls the addHealth method on the player with a set amount of health points.
   *
   * @param player Player - the player to execute the action on
   * @throws NullPointerException if player is null
   * @see Action#execute(Player)
   */
  @Override
  public void execute(final Player player) throws NullPointerException {
    Objects.requireNonNull(player, "The player cannot be null").addHealth(healthPoints);
  }

  /**
   * Get method for the action's value.
   *
   * @return Integer - the health points of the action
   * @see Action#getValue()
   */
  @Override
  public Integer getValue() {
    return healthPoints;
  }
}
