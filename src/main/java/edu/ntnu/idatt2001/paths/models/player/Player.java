package edu.ntnu.idatt2001.paths.models.player;

import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The Player class.
 * Represents who is playing the game and what they own.
 * The player has a name, health, score, gold and inventory.
 * When a player has 0 or less health, they are dead.
 * Contains a list of listeners which notifies when the player changes.
 * Uses an observable list for inventory to notify when the inventory changes
 * as the inventory is sent through objects with aggregation.
 *
 * @author Carl G. Callum G.
 * @version 0.9 - 30.04.2023
 */
public class Player {

  private final String name;
  private int health;
  private int score;
  private int gold;
  private final List<String> inventory;

  private final List<PlayerChangeListener> changeListeners;

  /**
   * The builder constructor for the Player class.
   * @param pb PlayerBuilder - the builder object
   *
   * @throws NullPointerException     thrown if name or inventory is null or contains null values
   * @throws IllegalArgumentException thrown if name is blank, health is zero or lower, gold is negative
   */
  public Player(final PlayerBuilder pb) throws NullPointerException, IllegalArgumentException {
    this(pb.getName(), pb.getHealth(), pb.getScore(), pb.getGold(), pb.getInventory());
  }

  /**
   * The main constructor for the Player class.
   * Name cannot be blank or null. Health, and gold cannot be negative.
   * A player can have zero health, but cannot start with zero.
   * Inventory cannot be null.
   * Loops through the inventory and converts all items to trimmed strings in lowercase.
   *
   * @param name      String - name of the player
   * @param health    int - positive number representing the player's starting health
   * @param score     int - number representing the player's starting score
   * @param gold      int - positive number representing the player's amount of starting gold
   * @param inventory List - list of starting items the player has represented by strings
   * @throws NullPointerException     thrown if name or inventory is null or contains null values
   * @throws IllegalArgumentException thrown if name is blank, health is zero or lower, gold is negative
   */
  public Player(
    final String name,
    final int health,
    final int score,
    final int gold,
    final List<String> inventory
  ) throws NullPointerException, IllegalArgumentException {
    this.inventory =
      new ArrayList<>(
        Validation
          .requireNonNullOrContainsNullOrBlank(
            inventory,
            "Inventory cannot be null",
            "Inventory cannot contain null values",
            "Inventory cannot contain blank values"
          )
          .stream()
          .map(String::trim)
          .map(String::toLowerCase)
          .toList()
      );
    this.name =
      Validation.requireNonNullOrBlank(name, "Name cannot be null", "Name cannot be blank");
    this.health = Validation.requireLargerThan(health, 0, "Health cannot be zero or lower");
    this.gold = Validation.requirePositive(gold, "Gold cannot be negative");
    this.score = score;
    this.changeListeners = new ArrayList<>();
  }

  /**
   * Creates a new player through the main constructor. Inventory is set to an empty list.
   *
   * @param name   String - name of the player
   * @param health int - positive number representing the player's starting health
   * @param score  int - number representing the player's starting score
   * @param gold   int - positive number representing the player's amount of starting gold
   * @throws NullPointerException     thrown if name is null
   * @throws IllegalArgumentException thrown if name is blank, health is zero or lower or if gold is negative
   */
  public Player(final String name, final int health, final int score, final int gold)
    throws NullPointerException, IllegalArgumentException {
    this(name, health, score, gold, new ArrayList<>());
  }

  /**
   * Creates a new player with only name. Health, score, gold and inventory are set to default values
   * which are set as static constants in the class.
   *
   * @param name String - name of the player
   * @throws NullPointerException     thrown if name is null
   * @throws IllegalArgumentException thrown if name is blank
   */
  public Player(final String name) throws IllegalArgumentException {
    this(
      name,
      PlayerStatEnum.DEFAULT_HEALTH.getValue(),
      PlayerStatEnum.DEFAULT_SCORE.getValue(),
      PlayerStatEnum.DEFAULT_GOLD.getValue()
    );
  }

  /**
   * Retrieves the name of the player
   *
   * @return String - name of the player
   */
  public String getName() {
    return name;
  }

  /**
   * Retrieves the player's health
   *
   * @return int - health of the player
   */
  public int getHealth() {
    return health;
  }

  /**
   * Checks if the player has health above 0
   *
   * @return boolean - true if player is alive, false if player is dead
   */
  public boolean isAlive() {
    return health > 0;
  }

  /**
   * Adds a number to the player's health.
   * This number can be positive or negative.
   * If health becomes negative, it is set to 0.
   *
   * @param health int - positive or negative number to add to the player's health
   */
  public void addHealth(final int health) {
    this.health += health;
    if (this.health < 0) {
      this.health = 0;
    }
    notifyPlayerChangeListeners();
  }

  /**
   * Retrieves the player's score
   *
   * @return int - score of the player
   */
  public int getScore() {
    return score;
  }

  /**
   * Adds a number to the player's score.
   * This number can be positive or negative.
   *
   * @param score int - positive or negative number to add to the player's score
   */
  public void addScore(final int score) {
    this.score += score;
    notifyPlayerChangeListeners();
  }

  /**
   * Retrieves the gold of the player
   *
   * @return String - gold of the player
   */
  public int getGold() {
    return gold;
  }

  /**
   * Adds a number to the player's gold count.
   * This number can be positive or negative.
   * If the amount of becomes negative, it is set to 0.
   *
   * @param gold int - positive or negative number to add to the player's gold
   */
  public void addGold(final int gold) {
    this.gold += gold;
    if (this.gold < 0) {
      this.gold = 0;
    }
    notifyPlayerChangeListeners();
  }

  /**
   * Retrieves the inventory of the player by aggregation
   *
   * @return List - list of items the player has
   */
  public List<String> getInventory() {
    return Collections.unmodifiableList(inventory);
  }

  /**
   * Adds an element to the player's inventory
   * Trims and converts to lowercase before adding as inventory is stored in lowercase
   *
   * @param item String - an item to add to the inventory
   * @throws NullPointerException thrown if item is null
   * @throws IllegalArgumentException thrown if item is blank
   */
  public void addToInventory(String item) throws NullPointerException, IllegalArgumentException {
    boolean added = inventory.add(
      Validation
        .requireNonNullOrBlank(item, "Item cannot be null", "Item cannot be blank")
        .trim()
        .toLowerCase()
    );
    if (added) notifyPlayerChangeListeners();
  }

  /**
   * Removes an element from the player's inventory
   * Trims and converts to lowercase before removing as inventory is stored in lowercase
   *
   * @param item String - an item to remove from the inventory
   * @throws NullPointerException thrown if item is null
   * @throws IllegalArgumentException thrown if item is blank
   */
  public void removeFromInventory(String item)
    throws NullPointerException, IllegalArgumentException {
    boolean removed = inventory.remove(
      Validation
        .requireNonNullOrBlank(item, "Item cannot be null", "Item cannot be blank")
        .trim()
        .toLowerCase()
    );
    if (removed) notifyPlayerChangeListeners();
  }

  /**
   * Removes all instances of an element from the player's inventory
   * Trims and converts to lowercase before checking as inventory is stored in lowercase
   *
   * @param item String - an item to remove from the inventory
   * @throws NullPointerException thrown if item is null
   * @throws IllegalArgumentException thrown if item is blank
   */
  public void removeInstancesFromInventory(String item)
    throws NullPointerException, IllegalArgumentException {
    final String stringToCheck = Validation
      .requireNonNullOrBlank(item, "Item cannot be null", "Item cannot be blank")
      .trim()
      .toLowerCase();
    boolean removed = inventory.removeIf(x -> x.equals(stringToCheck));
    if (removed) notifyPlayerChangeListeners();
  }

  /**
   * Checks to see if a specified item is in the player's inventory
   * Possible to send null as parameter, in which case false is returned
   * item is trimmed and converted to lowercase before checking as inventory
   * is stored in lowercase
   *
   * @param item String - the item to check for
   * @return boolean - true if the item is in the inventory, false if not
   * @throws NullPointerException thrown if the item is null
   * @throws IllegalArgumentException thrown if item is blank
   */
  public boolean isInInventory(String item) throws NullPointerException, IllegalArgumentException {
    return inventory.contains(
      Validation
        .requireNonNullOrBlank(item, "Item cannot be null", "Item cannot be blank")
        .trim()
        .toLowerCase()
    );
  }

  /**
   * Adds a player change listener to the player
   * This listener will be notified when the player changes
   * @param changeListener PlayerChangeListener - the listener to add
   */
  public void addPlayerChangeListener(PlayerChangeListener changeListener) {
    changeListeners.add(changeListener);
  }

  /**
   * Removes a player change listener from the player
   * This listener will no longer be notified when the player changes
   * @param changeListener PlayerChangeListener - the listener to remove
   */
  public void removePlayerChangeListener(PlayerChangeListener changeListener) {
    changeListeners.remove(changeListener);
  }

  /**
   * Notifies all player change listeners that the player has changed
   * Loops over all player change listeners and calls onPlayerChange
   * Is a private method such that only the object can call it
   * @see PlayerChangeListener#onPlayerChange(Player)
   */
  private void notifyPlayerChangeListeners() {
    for (PlayerChangeListener changeListener : changeListeners) {
      changeListener.onPlayerChange(this);
    }
  }
}
