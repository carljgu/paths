package edu.ntnu.idatt2001.paths.models.actions;

import java.util.stream.Stream;

/**
 * Enum for the different types of actions.
 * Actions are stored as strings.
 *
 * @author Callum G.
 * @version 0.3 - 29.04.2023
 */
public enum ActionTypeEnum {
  /**
   * Gold action.
   */
  GOLD(GoldAction.class.getSimpleName()),
  /**
   * Health action.
   */
  HEALTH(HealthAction.class.getSimpleName()),
  /**
   * Inventory add action.
   */
  INVENTORY_ADD(InventoryAddAction.class.getSimpleName()),
  /**
   * Inventory remove action.
   */
  INVENTORY_REMOVE(InventoryRemoveAction.class.getSimpleName()),
  /**
   * Inventory remove all action.
   */
  INVENTORY_REMOVE_ALL(InventoryRemoveAllAction.class.getSimpleName()),
  /**
   * Score action.
   */
  SCORE(ScoreAction.class.getSimpleName()),
  /**
   * Unknown action.
   */
  UNKNOWN("Unknown");

  private final String action;

  /**
   * Constructor for the enum.
   *
   * @param action String - the action
   */
  ActionTypeEnum(final String action) {
    this.action = action;
  }

  /**
   * Method to get the action type.
   *
   * @param action String - the action
   * @return ActionTypeEnum - the action type
   */
  public static ActionTypeEnum getActionType(final String action) {
    return Stream
      .of(ActionTypeEnum.values())
      .filter(actionTypeEnum -> actionTypeEnum.getAction().equals(action))
      .findFirst()
      .orElse(UNKNOWN);
  }

  /**
   * Method to get the action.
   *
   * @return String - the action
   */
  public String getAction() {
    return action;
  }
}
