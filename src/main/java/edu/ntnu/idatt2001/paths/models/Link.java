package edu.ntnu.idatt2001.paths.models;

import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class link represents a link between two passages
 * The reference is a string that points to the name of the passage
 *
 * @author Carl G. Callum G.
 * @version 0.5 - 29.04.2023
 */
public class Link {

  private final String text;
  private final String reference;
  private final List<Action<?>> actions;

  /**
   * Instantiates a new Link. Both text and reference should be set,
   * but reference can be null if there is no passage to link to.
   *
   * @param text      String - text identification of the link
   * @param reference String - reference to the next passage
   * @param actions List - group of actions which are executed when the link is used
   * @throws NullPointerException thrown if text is null, reference is null or actions is null or contains null
   * @throws IllegalArgumentException thrown if text or reference is blank
   */
  public Link(final String text, final String reference, final List<Action<?>> actions)
    throws NullPointerException, IllegalArgumentException {
    (this.actions = new ArrayList<>()).addAll(
        Validation.requireNonNullOrContainsNull(
          actions,
          "Actions cannot be null",
          "Actions cannot contain null values"
        )
      );
    this.text =
      Validation.requireNonNullOrBlank(text, "Text cannot be null", "Text cannot be blank");
    this.reference =
      Validation.requireNonNullOrBlank(
        reference,
        "Reference cannot be null",
        "Reference cannot be blank"
      );
  }

  /**
   * Instantiates a new Link. Both text and reference should be set,
   * but reference can be null if there is no passage to link to.
   *
   * @param text      String - text identification of the link
   * @param reference String - reference to the next passage
   * @throws NullPointerException thrown if text or reference is null
   * @throws IllegalArgumentException thrown if text or reference is only whitespace
   */
  public Link(final String text, final String reference)
    throws NullPointerException, IllegalArgumentException {
    this(text, reference, new ArrayList<>());
  }

  /**
   * Retrieves the link text.
   *
   * @return String - text identification of the link
   */
  public String getText() {
    return text;
  }

  /**
   * Retrieves the passage reference.
   *
   * @return String - reference to the next passage
   */
  public String getReference() {
    return reference;
  }

  /**
   * Retrieves the list of actions
   *
   * @return List - list of actions to execute
   */
  public List<Action<?>> getActions() {
    return actions;
  }

  /**
   * Adds an action to the list of actions
   *
   * @param action Action - action to add
   * @throws NullPointerException thrown if the action is null
   */
  public void addAction(final Action<?> action) throws NullPointerException {
    actions.add(Objects.requireNonNull(action, "Action cannot be null"));
  }

  /**
   * Returns a string representation of the link
   * Uses text and reference
   * example: Link 'Go to the kitchen' connects to 'kitchen'
   *
   * @return String representation of the link
   */
  @Override
  public String toString() {
    return "Link '" + getText() + "' connects to '" + getReference() + "'";
  }

  /**
   * Two links are equal if they have the same reference
   * This is to make the required map of links to passages
   * point links with different text and actions to the same passage
   *
   * @param o Object - Any object equality checked with this link
   * @return boolean - true if the links are equal, false if not
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Link)) return false;
    Link link = (Link) o;
    return Objects.equals(getReference(), link.getReference());
  }

  /**
   * Returns a hashcode for the link based on the reference
   *
   * @return int - hashcode for the link
   */
  @Override
  public int hashCode() {
    return Objects.hash(getReference());
  }
}
