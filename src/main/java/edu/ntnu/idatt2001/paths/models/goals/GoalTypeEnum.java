package edu.ntnu.idatt2001.paths.models.goals;

import java.util.stream.Stream;

/**
 * Enum for the different types of goals.
 * Goals are stored as strings.
 *
 * @author Carl G.
 * @version 1.0 - 29.04.2023
 */
public enum GoalTypeEnum {
  /**
   * Gold goal.
   */
  GOLD(GoldGoal.class.getSimpleName()),
  /**
   * Health goal.
   */
  HEALTH(HealthGoal.class.getSimpleName()),
  /**
   * Inventory add goal.
   */
  INVENTORY(InventoryGoal.class.getSimpleName()),
  /**
   * Score goal.
   */
  MINIMUM_SCORE(MinimumScoreGoal.class.getSimpleName()),
  /**
   * Score goal.
   */
  MAXIUMUM_SCORE(MaximumScoreGoal.class.getSimpleName()),
  /**
   * Unknown goal.
   */
  UNKNOWN("Unknown");

  private final String goal;

  /**
   * Constructor for the enum.
   *
   * @param goal String - the goal
   */
  GoalTypeEnum(String goal) {
    this.goal = goal;
  }

  /**
   * Method to get the goal type.
   *
   * @param goal String - the goal
   * @return GoalTypeEnum - the goal type
   */
  public static GoalTypeEnum getGoalType(String goal) {
    return Stream
      .of(GoalTypeEnum.values())
      .filter(goalTypeEnum -> goalTypeEnum.getGoal().equals(goal))
      .findFirst()
      .orElse(UNKNOWN);
  }

  /**
   * Method to get the goal.
   *
   * @return String - the goal
   */
  public String getGoal() {
    return goal;
  }
}
