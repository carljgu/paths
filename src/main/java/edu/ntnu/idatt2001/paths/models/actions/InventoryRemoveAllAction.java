package edu.ntnu.idatt2001.paths.models.actions;

import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.Objects;

/**
 * InventoryRemoveAllAction class.
 * Removes items of a player's inventory when executed.
 * Which items to remove is defined when instantiated.
 * All instances of the item are removed.
 * If the item does not exist in the player's inventory, nothing happens.
 *
 * @author Carl G. Callum G.
 * @version 0.5 - 29.04.2023
 */
public class InventoryRemoveAllAction implements Action<String> {

  private final String item;

  /**
   * Instantiates a new Inventory remove all action
   * and sets the item to remove from the player's inventory.
   *
   * @param item String - a non-null and non-empty string representing the item to remove
   * @throws NullPointerException thrown if the item is null
   * @throws IllegalArgumentException thrown if the item is null or only whitespace
   */
  public InventoryRemoveAllAction(String item)
    throws NullPointerException, IllegalArgumentException {
    this.item =
      Validation.requireNonNullOrBlank(
        item,
        "Item cannot be null",
        "Item cannot be only whitespace"
      );
  }

  /**
   * Executes an action on a player.
   * Calls the removeFromInventory method on the player with a set item.
   *
   * @param player Player - the player to execute the action on
   * @throws NullPointerException if player is null
   * @see Action#execute(Player)
   */
  @Override
  public void execute(final Player player) throws NullPointerException {
    Objects.requireNonNull(player, "The player cannot be null").removeInstancesFromInventory(item);
  }

  /**
   * Get method for the action's value.
   *
   * @return String - the item of the action
   * @see Action#getValue()
   */
  @Override
  public String getValue() {
    return item;
  }
}
