package edu.ntnu.idatt2001.paths.models.player;

import edu.ntnu.idatt2001.paths.models.game.GameDifficultyEnum;
import java.util.List;

/**
 * The class Player builder.
 * This class is used to build a player object with the builder pattern.
 * @author Carl G. Callum G.
 * @version 0.2 - 29.04.2023
 */
public class PlayerBuilder {

  private String name;
  private int health;
  private int score;
  private int gold;
  private List<String> inventory;

  /**
   * Instantiates a new Player builder.
   * Sets the default values for the player.
   * @param difficulty the difficulty of the game.
   */
  public PlayerBuilder(final GameDifficultyEnum difficulty) {
    this.name = "Player";
    this.health = difficulty.getDefaultHealth();
    this.score = difficulty.getDefaultScore();
    this.gold = difficulty.getDefaultGold();
    this.inventory = List.of();
  }

  /**
   * Instantiates a new Player builder.
   * Sets the default values for the player.
   */
  public PlayerBuilder() {
    this(GameDifficultyEnum.DEFAULT);
  }

  /**
   * Method to build a player object.
   * @return the player object.
   */
  public Player build() {
    return new Player(this);
  }

  /**
   * Method to get the name of the player.
   * @return the player object.
   */
  public String getName() {
    return name;
  }

  /**
   * Method to get the health of the player.
   * @return the health of the player.
   */
  public int getHealth() {
    return health;
  }

  /**
   * Method to get the score of the player.
   * @return the score of the player.
   */
  public int getScore() {
    return score;
  }

  /**
   * Method to get the gold of the player.
   * @return the gold of the player.
   */
  public int getGold() {
    return gold;
  }

  /**
   * Method to get the inventory of the player.
   * @return the inventory of the player.
   */
  public List<String> getInventory() {
    return inventory;
  }

  /**
   * Method to set the name of the player.
   * @param name the name of the player.
   * @return the player builder object.
   */
  public PlayerBuilder setName(final String name) {
    this.name = name;
    return this;
  }

  /**
   * Method to set the health of the player.
   * @param health the health of the player.
   * @return the player builder object.
   */
  public PlayerBuilder setHealth(final int health) {
    this.health = health;
    return this;
  }

  /**
   * Method to set the gold of the player.
   * @param gold the gold of the player.
   * @return the player builder object.
   */
  public PlayerBuilder setGold(final int gold) {
    this.gold = gold;
    return this;
  }

  /**
   * Method to set the score of the player.
   * @param score the score of the player.
   * @return the player builder object.
   */
  public PlayerBuilder setScore(final int score) {
    this.score = score;
    return this;
  }

  /**
   * Method to set the inventory of the player.
   * @param items the items of the player.
   * @return the player builder object.
   */
  public PlayerBuilder setInventory(final List<String> items) {
    inventory = items;
    return this;
  }
}
