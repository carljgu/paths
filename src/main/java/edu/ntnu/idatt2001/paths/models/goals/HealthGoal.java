package edu.ntnu.idatt2001.paths.models.goals;

import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.Objects;

/**
 * HealthGoal class.
 * Checks if a player has a certain amount of health.
 * The amount of health for fulfillment is defined when instantiated.
 * The health goal cannot be less than or equal to 1.
 *
 * @author Carl G. Callum G.
 * @version 0.4 - 29.04.2023
 */
public class HealthGoal implements Goal<Integer> {

  private final int minimumHealth;

  /**
   * Creates a new health goal which checks the
   * player's health with the minimum health.
   * As players need at least one health point to survive,
   * the minimum health cannot be less than or equal to this.
   *
   * @param minimumHealth int - the amount of health for fulfillment
   * @throws IllegalArgumentException thrown if the minimum health is less than or equal to 1
   */
  public HealthGoal(final int minimumHealth) throws IllegalArgumentException {
    this.minimumHealth =
      Validation.requireLargerThan(
        minimumHealth,
        1,
        "The minimum health cannot be less than or equal to 1"
      );
  }

  /**
   * Checks if a player has fulfilled the health goal.
   * Calls the getHealth method on the player and checks if it is more than or equal to the minimum health.
   *
   * @param player Player - the player to check the goal with.
   * @throws NullPointerException if player is null
   * @see Goal#isFulfilled(Player)
   */
  @Override
  public boolean isFulfilled(final Player player) throws NullPointerException {
    return Objects.requireNonNull(player, "The player cannot be null").getHealth() >= minimumHealth;
  }

  /**
   * Get the goal's minimum health criteria.
   * @see Goal#getFulfillmentCriteria()
   * @return Integer - the minimum health criteria
   */
  @Override
  public Integer getFulfillmentCriteria() {
    return minimumHealth;
  }
}
