package edu.ntnu.idatt2001.paths.models.actions;

import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.Objects;

/**
 * GoldAction class.
 * Adds or subtracts gold from a player's gold when executed.
 * Amount of gold modified is defined when instantiated.
 *
 * @author Carl G. Callum G.
 * @version 0.5 - 29.04.2023
 */
public class GoldAction implements Action<Integer> {

  private final int gold;

  /**
   * Creates a new gold action which adds or removes
   * gold from a player's gold stash.
   *
   * @param gold int - the amount of gold to add or subtract
   */
  public GoldAction(final int gold) {
    this.gold = gold;
  }

  /**
   * Executes an action on a player.
   * Calls the addGold method on the player with a set amount of gold.
   *
   * @param player Player - the player to execute the action on
   * @throws NullPointerException if player is null
   * @see Action#execute(Player)
   */
  @Override
  public void execute(final Player player) throws NullPointerException {
    Objects.requireNonNull(player, "The player cannot be null").addGold(gold);
  }

  /**
   * Get method for the action's value.
   *
   * @return Integer - the gold of the action
   * @see Action#getValue()
   */
  @Override
  public Integer getValue() {
    return gold;
  }
}
