package edu.ntnu.idatt2001.paths.models.goals;

import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.Objects;

/**
 * MinimumScoreGoal class.
 * Checks if the player's score is less than or equal to a certain amount.
 * The amount of score for fulfillment is defined when instantiated.
 * Players can start with negative score and the maximum
 * score can therefore also be negative.
 *
 * @author Carl G. Callum G.
 * @version 0.4 - 29.04.2023
 */
public class MaximumScoreGoal implements Goal<Integer> {

  private final int maximumScore;

  /**
   * Creates a new score goal which checks the
   * player's score with the maximum score.
   *
   * @param maximumScore int - the amount of score for fulfillment
   */
  public MaximumScoreGoal(final int maximumScore) {
    this.maximumScore = maximumScore;
  }

  /**
   * Checks if a player has fulfilled the score goal.
   * Calls the getScore method and checks if it is
   * less than or equal to the maximum score
   *
   * @param player Player - the player to check the goal with.
   * @throws NullPointerException if player is null
   * @see Goal#isFulfilled(Player)
   */
  @Override
  public boolean isFulfilled(final Player player) throws NullPointerException {
    return Objects.requireNonNull(player, "The player cannot be null").getScore() <= maximumScore;
  }

  /**
   * Get the score goal's maximum score criteria.
   * @see Goal#getFulfillmentCriteria()
   * @return Integer - the maximum score criteria
   */
  @Override
  public Integer getFulfillmentCriteria() {
    return maximumScore;
  }
}
