package edu.ntnu.idatt2001.paths.models.goals;

import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.List;
import java.util.Objects;

/**
 * InventoryGoal class.
 * Checks if a player has all the inventory items
 * The items to have for fulfillment is defined when instantiated.
 *
 * @author Carl. G. Callum G.
 * @version 0.4 - 29.04.2023
 */
public class InventoryGoal implements Goal<List<String>> {

  private final List<String> mandatoryItems;

  /**
   * Creates a new InventoryGoal class checking if the player
   * has at least one instance of all the items in the list.
   * The list of mandatory items cannot be null, not be empty
   * and cannot contain null values.
   *
   * @param mandatoryItems List - the items to have for fulfillment
   * @throws NullPointerException thrown if the list is null or contains null values or blank values
   * @throws IllegalArgumentException thrown if there are no items in the list
   */
  public InventoryGoal(final List<String> mandatoryItems) {
    this.mandatoryItems =
      Validation.requireNonNullOrEmptyOrContainsNullOrBlank(
        mandatoryItems,
        "The list of mandatory items cannot be null",
        "The list of mandatory items cannot be empty",
        "The list of mandatory items cannot contain null values",
        "The list of mandatory items cannot contain blank values"
      );
  }

  /**
   * Checks if a player has fulfilled the InventoryGoal.
   * Uses the player's isInInventory method to check if the player has all the items.
   * Uses this method to ignore case and untrimmed strings.
   *
   * @param player Player - the player to check the goal with.
   * @throws NullPointerException if player is null
   * @see Goal#isFulfilled(Player)
   */
  @Override
  public boolean isFulfilled(final Player player) throws NullPointerException {
    Objects.requireNonNull(player, "The player cannot be null");
    return mandatoryItems.stream().allMatch(item -> player.isInInventory(item));
  }

  /**
   * Get the inventory goal's mandatory items
   *
   * @see Goal#getFulfillmentCriteria()
   * @return List - the mandatory items to have for fulfillment
   */
  @Override
  public List<String> getFulfillmentCriteria() {
    return mandatoryItems;
  }
}
