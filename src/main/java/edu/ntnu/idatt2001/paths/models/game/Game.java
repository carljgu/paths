package edu.ntnu.idatt2001.paths.models.game;

import edu.ntnu.idatt2001.paths.exceptions.UnplayableStoryException;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.goals.Goal;
import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.validation.Validation;
import java.util.List;
import java.util.Objects;

/**
 * The game class.
 * A game object represents what a user
 * will use to play through a story.
 * A game has several goals that a user has to achieve
 * If there are no goals, there is no game.
 *
 * @author Carl G. Callum G.
 * @version 0.4 - 29.04.2023
 */
public class Game {

  private final Player player;
  private final Story story;
  private final List<Goal<?>> goals;

  /**
   * Creates a new Game object. None of the
   * parameters can be null. For the game to have any meaning,
   * it has to have at least one goal.
   *
   * @param player Player - representation of the user and the user's possessions
   * @param story  Story - how a story and game is structured through links and passages.
   * @param goals  Goals - what the player has to achieve in order to win.
   * @throws NullPointerException thrown if any of the parameters are null or if the list of goals contains null values.
   * @throws IllegalArgumentException thrown if the list of goals or empty.
   */
  public Game(Player player, Story story, List<Goal<?>> goals)
    throws NullPointerException, IllegalArgumentException {
    this.player = Objects.requireNonNull(player, "Player cannot be null");
    this.story = Objects.requireNonNull(story, "Story cannot be null");
    this.goals =
      Validation.requireNonNullOrEmptyOrContainsNull(
        goals,
        "List of goals cannot be null",
        "List of goals cannot be empty",
        "List of goals cannot contain null values"
      );
  }

  /**
   * Retrieves the player
   *
   * @return Player - representation of the user and the user's possessions
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * Retrieves the story
   *
   * @return Story - how a story and game is structured through links and passages.
   */
  public Story getStory() {
    return story;
  }

  /**
   * Retrieves the list of goals
   *
   * @return Goals - what the player has to achieve in order to win.
   */
  public List<Goal<?>> getGoals() {
    return goals;
  }

  /**
   * Begins the game by retrieving the first passage in the story.
   * @see Story#getOpeningPassage()
   *
   * @return Passage - the first passage in the story
   * that the player starts with
   * @throws UnplayableStoryException thrown if the story is not playable
   */
  public Passage begin() throws UnplayableStoryException {
    if (!story.isPlayable()) throw new UnplayableStoryException("Can't begin an unplayable game");
    return story.getOpeningPassage();
  }

  /**
   * Moves the player from a chosen link to a new passage
   * Retrieves the passage that a link connects to.
   * @see Story#getPassage(Link)
   *
   * @param link Link - a connection to a passage with an associated action
   * @return Passage - the next passage in the story the link connects to
   */
  public Passage go(final Link link) throws NullPointerException {
    return story.getPassage(link);
  }
}
