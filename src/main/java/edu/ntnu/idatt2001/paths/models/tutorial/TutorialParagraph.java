package edu.ntnu.idatt2001.paths.models.tutorial;

import edu.ntnu.idatt2001.paths.validation.Validation;

/**
 * Class for a tutorial paragraph
 * A paragraph consists of a title, content and an image path.
 * The image path is optional and can be null.
 * @author Carl G.
 * @version 1.0 22.05.2023
 */
public class TutorialParagraph {

  private final String title;
  private final String content;
  private final String imagePath;

  /**
   * Creates a new tutorial paragraph with a title, content and an image path.
   * @param title String - the title of the paragraph
   * @param content String - the content of the paragraph
   * @param imagePath String - the optional image path of the paragraph
   * @throws IllegalArgumentException if the title or content is only whitespace
   * @throws NullPointerException if the title or content is null
   */
  public TutorialParagraph(String title, String content, String imagePath)
    throws IllegalArgumentException, NullPointerException {
    super();
    this.title =
      Validation.requireNonNullOrBlank(title, "Title cannot be null", "Title cannot be empty");
    this.content =
      Validation.requireNonNullOrBlank(content, "Text cannot be null", "Text cannot be empty");
    this.imagePath = imagePath;
  }

  /**
   * Creates a new tutorial paragraph with a title and content.
   * @param title String - the title of the paragraph
   * @param content String - the content of the paragraph
   * @throws IllegalArgumentException if the title or content is only whitespace
   * @throws NullPointerException if the title or content is null
   */
  public TutorialParagraph(String title, String content)
    throws IllegalArgumentException, NullPointerException {
    this(title, content, null);
  }

  /**
   * Gets the title of the paragraph.
   * @return String - the title of the paragraph
   */
  public String getTitle() {
    return title;
  }

  /**
   * Gets the content of the paragraph.
   * @return String - the content of the paragraph
   */
  public String getContent() {
    return content;
  }

  /**
   * Gets the image path of the paragraph.
   * @return String - the image path of the paragraph
   */
  public String getImagePath() {
    return imagePath;
  }
}
