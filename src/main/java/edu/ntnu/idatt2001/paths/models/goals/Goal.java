package edu.ntnu.idatt2001.paths.models.goals;

import edu.ntnu.idatt2001.paths.models.player.Player;

/**
 * Goal interface.
 * Implements a method for checking if a goal is fulfilled
 * in a game.
 * Goals check if the player has achieved a certain criteria.
 *
 * @author Carl G. Callum G.
 * @version 0.3 - 29.04.2023
 */
public interface Goal<T> {
  /**
   * Checks if a player has fulfilled a goal.
   * Does not modify the players attributes.
   *
   * @param player Player - the player to check the goal with.
   * @return boolean - true if the goal is fulfilled, false otherwise.
   * @throws NullPointerException thrown if player is null
   */
  boolean isFulfilled(final Player player) throws NullPointerException;

  /**
   * Get the goal's fulfillment criteria.
   * @return T - the goal's fulfillment criteria
   */
  T getFulfillmentCriteria();
}
