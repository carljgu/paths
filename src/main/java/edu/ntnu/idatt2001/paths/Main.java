package edu.ntnu.idatt2001.paths;

import edu.ntnu.idatt2001.paths.views.App;

/**
 * The entrypoint class of the application
 *
 * @version 0.1
 */
public class Main {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    App.main(args);
  }
}
