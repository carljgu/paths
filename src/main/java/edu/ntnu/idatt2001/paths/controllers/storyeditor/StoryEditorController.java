package edu.ntnu.idatt2001.paths.controllers.storyeditor;

import edu.ntnu.idatt2001.paths.controllers.BaseController;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.ActionTypeEnum;
import edu.ntnu.idatt2001.paths.views.storyeditor.DraggableDiagramPane;
import edu.ntnu.idatt2001.paths.views.storyeditor.StoryEditorPane;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

/**
 * Interface for the story editor controller.
 *
 * @author Callum G.
 * @version 1.1 23.05.2023
 */
public interface StoryEditorController extends BaseController {
  StoryEditorPane getView();

  DraggableDiagramPane getDiagramPane();

  void pressDiagramPane(MouseEvent event);

  void dragDiagramPane(MouseEvent event);

  void createNewPassage(ActionEvent event);

  void updateSelectedPassage(ActionEvent event);

  void linkPassageNodes(ActionEvent event);

  void updateLinkPositions(List<Node> links);

  void updateCanvasGraphic(DraggableDiagramPane diagramPane, double dx, double dy);

  void setAsOpeningPassage(ActionEvent event);

  Action<?> getActionObjectWithValue(ActionTypeEnum actionType, String actionValue);

  void deletePassageNode(ActionEvent event);

  void updateLink(ActionEvent event);

  void deleteLink(ActionEvent event);

  void addLinkAction(ActionEvent event);

  HBox createSingleActionForm(Class<? extends Action<?>> actionType, String value);

  void updateStoryTitle();

  void exportStory(ActionEvent event);

  void playStory(ActionEvent event);

  String getStoryTitle();
}
