package edu.ntnu.idatt2001.paths.controllers.storyeditor;

import edu.ntnu.idatt2001.paths.controllers.AbstractBaseController;
import edu.ntnu.idatt2001.paths.controllers.storystarter.StoryStarterControllerImpl;
import edu.ntnu.idatt2001.paths.file.StoryFileWriter;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.ActionTypeEnum;
import edu.ntnu.idatt2001.paths.models.actions.GoldAction;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryAddAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAllAction;
import edu.ntnu.idatt2001.paths.models.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.ChoiceBoxOption;
import edu.ntnu.idatt2001.paths.views.FeedbackDialogs;
import edu.ntnu.idatt2001.paths.views.storyeditor.DragResizePassageNode;
import edu.ntnu.idatt2001.paths.views.storyeditor.DraggableDiagramPane;
import edu.ntnu.idatt2001.paths.views.storyeditor.PassageNodeLink;
import edu.ntnu.idatt2001.paths.views.storyeditor.StoryEditorPane;
import edu.ntnu.idatt2001.paths.views.storystarter.StoryStarterPane;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javafx.event.ActionEvent;
import javafx.event.EventTarget;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;

/**
 * The story editor controller.
 * The story editor controller is responsible for handling the actions of the
 * story editor scene.
 * Actions include saving the story, adding a passage, adding a link between
 * passages, and editing the title and content of a passage.
 * The story editor controller is also responsible for updating the view when
 * changes are made to the model.
 *
 * @author Carl G.
 * @version 1.1 23.05.2023
 */
public class StoryEditorControllerImpl
  extends AbstractBaseController
  implements StoryEditorController {

  private static final String DIAGRAM_NOT_FOUND_ERROR_TITLE = "Error getting diagram pane";
  private static final String DIAGRAM_PANE_LOOKUP = "#diagramPane";
  private static final String DIAGRAM_NOT_FOUND_ERROR_CONTENT =
    "Could not load the editor. Please try again or restart the application.";

  private final Story story;
  private final StoryEditorPane storyEditorView;

  private DragResizePassageNode selectedPassageNode;
  private PassageNodeLink selectedLink;

  /**
   * Creates a new story editor controller with a new story.
   * The story will have the title "My new story" and no passages.
   */
  public StoryEditorControllerImpl() {
    this(new Story("My new story", null));
  }

  /**
   * Creates a new story editor controller with the given story.
   *
   * @param story the story to edit
   * @throws NullPointerException if story is null
   */
  public StoryEditorControllerImpl(Story story) throws NullPointerException {
    this.story = Objects.requireNonNull(story);
    this.storyEditorView = new StoryEditorPane(this);
    addStoryToView();
  }

  /**
   * Returns the pane of the controller
   * @return StoryEditorPane - the pane of the controller
   */
  @Override
  public StoryEditorPane getView() {
    return storyEditorView;
  }

  /**
   * Gets the diagram pane by lookup in the view
   * If the pane is not found, an error dialog is shown
   * @return DraggableDiagramPane - the diagram pane
   */
  @Override
  public DraggableDiagramPane getDiagramPane() {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return null;
    }
    return diagramPane;
  }

  /**
   * With a story, adds the passages to the view with the correct links
   */
  private void addStoryToView() {
    DraggableDiagramPane diagramPane = getDiagramPane();
    if (Objects.isNull(diagramPane)) {
      return;
    }
    diagramPane.getStoryBlockNodesContainer().getChildren().clear();
    double rowYStart = 50;
    int passageIndex = 0;
    for (Passage passage : story.getPassages()) {
      DragResizePassageNode passageNode = new DragResizePassageNode(passage);
      storyEditorView.addPassageNodeToDiagram(passageNode);
      passageNode.setIsOpeningPassage(Objects.equals(passage, story.getOpeningPassage()));
      // Set the layout x to increase for each node. When the node is at the end of the screen, set the next passage node to be on the next row
      passageNode.setLayoutX(
        passageNode.getLayoutX() + passageIndex * (passageNode.getPrefWidth() + 100)
      );
      passageIndex += 1;
      if (passageNode.getLayoutX() + passageNode.getPrefWidth() > App.getStage().getWidth()) {
        rowYStart += 50 + passageNode.getPrefHeight();
        passageIndex = 0;
      }
      passageNode.setLayoutY(rowYStart);
    }

    List<Node> passageNodes = diagramPane.getStoryBlockNodesContainer().getChildren();
    for (int i = 0; i < passageNodes.size(); i++) {
      DragResizePassageNode sourcePassageNode = (DragResizePassageNode) passageNodes.get(i);

      for (Link link : sourcePassageNode.getPassage().getLinks()) {
        Passage targetPassage = story.getPassage(link);
        if (Objects.nonNull(targetPassage)) {
          DragResizePassageNode targetPassageNode = (DragResizePassageNode) passageNodes.get(
            passageNodes.indexOf(new DragResizePassageNode(targetPassage))
          );
          PassageNodeLink passageNodeLink = new PassageNodeLink(
            sourcePassageNode,
            targetPassageNode
          );
          diagramPane.getLinkContainer().getChildren().add(passageNodeLink);
        }
      }
    }
  }

  /**
   * Sets the selected passage node in the controller
   * Sets first all to be without selection and then sets the selected passage node
   * Also updates the text fields with the passage title and content
   * @param selectedPassageNode DragResizePassageNodeController - the passage node to be selected
   */
  private void setSelectedPassageNode(DragResizePassageNode selectedPassageNode) {
    this.selectedPassageNode = selectedPassageNode;
    selectedPassageNode.select();
    TextField passageTitleField = (TextField) storyEditorView.lookup("#passageTitleField");
    TextArea passageContentField = (TextArea) storyEditorView.lookup("#passageContentField");
    if (Objects.isNull(passageTitleField) || Objects.isNull(passageContentField)) {
      FeedbackDialogs.showErrorDialog(
        "Error getting text fields",
        "Could not find the text fields for passage. Please try again or restart the application."
      );
      return;
    }
    passageTitleField.setText(selectedPassageNode.getPassage().getTitle());
    passageContentField.setText(selectedPassageNode.getPassage().getContent());
  }

  /**
   * Selects a node link
   * Sets first all to be without selection and then sets the selected node link
   * Also updates the text fields and actions with the link title and link actions
   * @param selectedLink PassageNodeLink - the link to be selected
   */
  private void setSelectedNodeLink(PassageNodeLink selectedLink) {
    this.selectedLink = selectedLink;
    selectedLink.select();
    TextField linkTitleField = (TextField) storyEditorView.lookup("#linkTextField");
    if (Objects.isNull(linkTitleField)) {
      FeedbackDialogs.showErrorDialog(
        "Error getting text fields",
        "Could not find the text fields for link. Please try again or restart the application."
      );
      return;
    }
    linkTitleField.setText(selectedLink.getLink().getText());

    Pane linkActionContainer = (Pane) storyEditorView.lookup("#linkActionFormContainer");
    if (Objects.isNull(linkActionContainer)) {
      FeedbackDialogs.showErrorDialog(
        "Error getting actions",
        "Could not find the actions for the selected link. Please try again or restart the application."
      );
      return;
    }
    linkActionContainer.getChildren().clear();
    selectedLink
      .getLink()
      .getActions()
      .forEach(a -> {
        @SuppressWarnings("unchecked")
        HBox actionForm = createSingleActionForm(
          ((Class<? extends Action<?>>) a.getClass()),
          a.getValue().toString()
        );
        linkActionContainer.getChildren().add(actionForm);
      });
  }

  /**
   * Controller method to handle presses to the diagram pane
   * If the click is a double click, the selected passage node and link is set to null
   * If passage or node link was clicked, they are selected
   * @param event MouseEvent - the event of the click
   */
  @Override
  public void pressDiagramPane(MouseEvent event) {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }
    diagramPane.setUserData(new double[] { event.getSceneX(), event.getSceneY() });
    if (event.getClickCount() >= 2) {
      selectedPassageNode = null;
      selectedLink = null;

      diagramPane
        .getStoryBlockNodesContainer()
        .getChildren()
        .forEach(n -> {
          DragResizePassageNode otherNode = (DragResizePassageNode) n;
          otherNode.deselect();
        });
      diagramPane
        .getLinkContainer()
        .getChildren()
        .forEach(n -> {
          PassageNodeLink otherLink = (PassageNodeLink) n;
          otherLink.deselect();
        });

      EventTarget target = event.getTarget();
      if (target instanceof Rectangle rectangle) {
        setSelectedPassageNode(getNodeFromParents(DragResizePassageNode.class, rectangle));
      } else if (target instanceof Pane) {
        diagramPane
          .getLinkContainer()
          .getChildren()
          .stream()
          .filter(l -> {
            PassageNodeLink linkNode = (PassageNodeLink) l;
            Line line = linkNode.getLine();
            Line lineBound = new Line(
              line.getStartX(),
              line.getStartY(),
              line.getEndX(),
              line.getEndY()
            );
            lineBound.setStrokeWidth(PassageNodeLink.SELECTION_MARGIN);
            return lineBound.contains(event.getX(), event.getY());
          })
          .findAny()
          .ifPresent(l -> setSelectedNodeLink((PassageNodeLink) l));
      }
    }
    updateLinkPositions(diagramPane.getLinkContainer().getChildren());
  }

  /**
   * Controller method to handle drags to the diagram pane
   * Moves the diagram pane and all the passage nodes and links if the background canvas is hit
   * Moves the selected passage node if it is hit
   * @param event MouseEvent - the event of the drag
   */
  @Override
  public void dragDiagramPane(MouseEvent event) {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }
    double[] userData = (double[]) diagramPane.getUserData();
    double offsetX = event.getSceneX() - userData[0];
    double offsetY = event.getSceneY() - userData[1];

    // Moving passage nodes and links
    updatePassageNodePositions(
      diagramPane.getStoryBlockNodesContainer().getChildren(),
      offsetX,
      offsetY
    );

    // Update link positions
    updateLinkPositions(diagramPane.getLinkContainer().getChildren());

    // Endless moving background grid
    updateCanvasGraphic(diagramPane, event.getSceneX() - offsetX, event.getSceneY() - offsetY);

    diagramPane.setUserData(new double[] { event.getSceneX(), event.getSceneY() });
  }

  /**
   * Create a new passage with the chosen title and content in the fields
   * Adds the passage to the story and the diagram
   * @param event ActionEvent - the event of the button press
   */
  @Override
  public void createNewPassage(ActionEvent event) {
    TextField passageTitleField = (TextField) storyEditorView.lookup("#passageTitleField");
    TextArea passageContentField = (TextArea) storyEditorView.lookup("#passageContentField");
    if (Objects.isNull(passageTitleField) || Objects.isNull(passageContentField)) {
      FeedbackDialogs.showErrorDialog(
        "Error getting text fields",
        "Could not find the text fields for passage. Please try again or restart the application."
      );
      return;
    }
    try {
      Passage newPassage = new Passage(passageTitleField.getText(), passageContentField.getText());
      story.addPassage(newPassage);
      storyEditorView.addPassageNodeToDiagram(new DragResizePassageNode(newPassage));
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error creating passage", e.getMessage());
    }
  }

  /**
   * Update the selected passage with the chosen title and content in the fields
   * @param event ActionEvent - the event of the button press
   */
  @Override
  public void updateSelectedPassage(ActionEvent event) {
    TextField passageTitleField = (TextField) storyEditorView.lookup("#passageTitleField");
    TextArea passageContentField = (TextArea) storyEditorView.lookup("#passageContentField");
    if (Objects.isNull(passageTitleField) || Objects.isNull(passageContentField)) {
      FeedbackDialogs.showErrorDialog(
        "Error getting text fields",
        "Could not find the text fields for passage. Please try again or restart the application."
      );
      return;
    }

    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }

    if (Objects.isNull(selectedPassageNode)) {
      FeedbackDialogs.showErrorDialog(
        "Error updating passage",
        "No passage selected. Please select a passage by double clicking on it and try again."
      );
      return;
    }

    Passage updatedPassage = new Passage(
      passageTitleField.getText(),
      passageContentField.getText(),
      selectedPassageNode.getPassage().getLinks()
    );

    try {
      story
        .getPassages()
        .stream()
        .filter(p -> p.equals(selectedPassageNode.getPassage()))
        .findFirst()
        .ifPresent(p -> {
          story.removePassageWithPassage(p);
          story.addPassage(updatedPassage);
        });
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error updating passage", e.getMessage());
      return;
    }

    diagramPane
      .getStoryBlockNodesContainer()
      .getChildren()
      .stream()
      .filter(pn -> pn.equals(selectedPassageNode))
      .findFirst()
      .ifPresent(pn -> ((DragResizePassageNode) pn).setPassage(updatedPassage));

    try {
      diagramPane
        .getLinkContainer()
        .getChildren()
        .forEach(n -> {
          PassageNodeLink linkLine = (PassageNodeLink) n;
          if (linkLine.getTarget().equals(selectedPassageNode)) {
            Link link = linkLine.getLink();
            linkLine.setLink(
              new Link(link.getText(), updatedPassage.getTitle(), link.getActions())
            );
          }
        });
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error updating links to and from", e.getMessage());
    }
  }

  @Override
  public void linkPassageNodes(ActionEvent event) {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }
    if (Objects.isNull(selectedPassageNode)) {
      FeedbackDialogs.showErrorDialog(
        "Error linking passages",
        "No passage selected. Please select a passage by double clicking on it and try again."
      );
      return;
    }

    Button linkButton = (Button) event.getSource();

    DragResizePassageNode passageNode = getNodeFromParents(DragResizePassageNode.class, linkButton);
    if (passageNode.equals(selectedPassageNode)) {
      FeedbackDialogs.showErrorDialog("Error linking passages", "Cannot link to itself.");
      return;
    }
    List<Node> links = diagramPane.getLinkContainer().getChildren();
    PassageNodeLink nodeLink = new PassageNodeLink(selectedPassageNode, passageNode);
    if (links.stream().anyMatch(n -> ((PassageNodeLink) n).getLink().equals(nodeLink.getLink()))) {
      return;
    }
    links.add(nodeLink);
    try {
      boolean added = selectedPassageNode.getPassage().addLink(nodeLink.getLink());
      if (!added) {
        FeedbackDialogs.showErrorDialog(
          "Error linking passages",
          "Cannot link to a passage that already exists."
        );
      }
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error linking passages", e.getMessage());
    }
  }

  /**
   * Updates the node positions of the passage nodes in the diagram pane
   * @param passageNodes List - the list of passage nodes to update
   * @param offsetX double - the offset to move the nodes in the x direction
   * @param offsetY double - the offset to move the nodes in the y direction
   */
  private void updatePassageNodePositions(List<Node> passageNodes, double offsetX, double offsetY) {
    for (var passageNode : passageNodes) {
      double layoutX = passageNode.getLayoutX() + offsetX;
      double layoutY = passageNode.getLayoutY() + offsetY;
      passageNode.setLayoutX(layoutX);
      passageNode.setLayoutY(layoutY);
    }
  }

  /**
   * Updates the link positions of the passage nodes in the diagram pane
   * @param links List - the list of links to update
   */
  @Override
  public void updateLinkPositions(List<Node> links) {
    links.forEach(link -> ((PassageNodeLink) link).updateLinePostion());
  }

  /**
   * Updates the canvas graphic of the diagram pane
   * @param diagramPane DraggableDiagramPane - the diagram pane to update the canvas graphic of
   * @param dx double - the offset to move the canvas graphic in the x direction
   * @param dy double - the offset to move the canvas graphic in the y direction
   */
  @Override
  public void updateCanvasGraphic(DraggableDiagramPane diagramPane, double dx, double dy) {
    Canvas background = diagramPane.getBackgroundCanvas();

    GraphicsContext gc = background.getGraphicsContext2D();
    gc.clearRect(0, 0, background.getWidth(), background.getHeight());

    double gridSize = diagramPane.getBackgroundGridSize();
    double gridOffsetX = (dx % gridSize + gridSize) % gridSize;
    double gridOffsetY = (dy % gridSize + gridSize) % gridSize;

    gc.save();
    gc.translate(gridOffsetX, gridOffsetY);
    diagramPane.drawBackgroundGrid();
    gc.restore();
  }

  /**
   * Updates the opening passage of the story
   * Updates the story to reflect this change
   * @param event ActionEvent - the mouse event that triggered the update
   */
  @Override
  public void setAsOpeningPassage(ActionEvent event) {
    Button button = (Button) event.getSource();
    DragResizePassageNode clickedPassageNode = getNodeFromParents(
      DragResizePassageNode.class,
      button
    );
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }

    diagramPane
      .getStoryBlockNodesContainer()
      .getChildren()
      .stream()
      .filter(pn -> !pn.equals(clickedPassageNode))
      .forEach(pn -> ((DragResizePassageNode) pn).setIsOpeningPassage(false));

    clickedPassageNode.setIsOpeningPassage(true);
    story.setOpeningPassage(clickedPassageNode.getPassage());
  }

  /**
   * Converts an action type enum and action value to an action object
   * @param actionType ActionTypeEnum - the action type enum to convert
   * @param actionValue String - the action value to convert
   */
  @Override
  public Action<?> getActionObjectWithValue(ActionTypeEnum actionType, String actionValue)
    throws NumberFormatException, IllegalArgumentException {
    return switch (actionType) {
      case GOLD -> new GoldAction(Integer.parseInt(actionValue));
      case HEALTH -> new HealthAction(Integer.parseInt(actionValue));
      case SCORE -> new ScoreAction(Integer.parseInt(actionValue));
      case INVENTORY_ADD -> new InventoryAddAction(actionValue);
      case INVENTORY_REMOVE_ALL -> new InventoryRemoveAllAction(actionValue);
      default -> null;
    };
  }

  /**
   * Deletes the passage node from the diagram pane
   * Removes it from the passage nodes container and the story
   * @param event ActionEvent - the event that triggered the delete
   */
  @Override
  public void deletePassageNode(ActionEvent event) {
    Button button = (Button) event.getSource();
    DragResizePassageNode clickedPassageNode = getNodeFromParents(
      DragResizePassageNode.class,
      button
    );
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }

    boolean passageDeleted = false;
    try {
      passageDeleted = story.removePassageWithPassage(clickedPassageNode.getPassage());
    } catch (Exception ignoredException) {}
    if (!passageDeleted) {
      FeedbackDialogs.showErrorDialog(
        "Error deleting passage",
        "Could not delete passage. Check that there are no links pointing to the passage"
      );
      return;
    }

    diagramPane
      .getLinkContainer()
      .getChildren()
      .removeIf(node -> {
        PassageNodeLink nodeLink = (PassageNodeLink) node;
        if (Objects.equals(nodeLink, selectedLink)) selectedLink = null;
        return (
          nodeLink.getSource().equals(clickedPassageNode) ||
          nodeLink.getTarget().equals(clickedPassageNode)
        );
      });

    for (Node node : diagramPane.getLinkContainer().getChildren()) {
      PassageNodeLink nodeLink = (PassageNodeLink) node;
      if (
        nodeLink.getSource().equals(clickedPassageNode) ||
        nodeLink.getTarget().equals(clickedPassageNode)
      ) {
        diagramPane.getLinkContainer().getChildren().remove(nodeLink);
        if (Objects.equals(selectedLink, nodeLink)) selectedLink = null;
      }
    }
    try {
      diagramPane.getStoryBlockNodesContainer().getChildren().remove(clickedPassageNode);
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error deleting passage", e.getMessage());
      return;
    }
    if (Objects.equals(selectedPassageNode, clickedPassageNode)) selectedPassageNode = null;
  }

  /**
   * Updates the link based on the text field values and actions chosen
   * Updates the story to reflect this change
   * @param event ActionEvent - the event that triggered the update
   */
  @Override
  public void updateLink(ActionEvent event) {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }

    if (Objects.isNull(selectedLink)) {
      FeedbackDialogs.showErrorDialog(
        "Error updating link",
        "No link selected. Please select a link by double clicking on it and try again."
      );
      return;
    }

    TextField linkTextField = (TextField) storyEditorView.lookup("#linkTextField");

    Pane linkActionContainer = (Pane) storyEditorView.lookup("#linkActionFormContainer");
    if (Objects.isNull(linkTextField) || Objects.isNull(linkActionContainer)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        "Could not load the information about this link. Please try again or restart the application."
      );
      return;
    }
    List<Action<?>> actions = new ArrayList<>();
    Set<Node> actionClassFields = linkActionContainer.lookupAll("#actionTypeChoiceBox");
    Set<Node> actionValueFields = linkActionContainer.lookupAll("#actionValueField");

    Iterator<Node> classIterator = actionClassFields.iterator();
    Iterator<Node> valueIterator = actionValueFields.iterator();
    while (classIterator.hasNext() && valueIterator.hasNext()) {
      TextField valueField = (TextField) valueIterator.next();
      @SuppressWarnings("unchecked")
      ChoiceBox<ChoiceBoxOption<Class<? extends Action<?>>>> classChoiceBox = (ChoiceBox<ChoiceBoxOption<Class<? extends Action<?>>>>) classIterator.next();

      if (Objects.isNull(classChoiceBox.getValue())) {
        continue;
      }
      ActionTypeEnum actionTypeEnum = ActionTypeEnum.getActionType(
        classChoiceBox.getValue().getValue().getSimpleName()
      );
      Action<?> action = getActionObjectWithValue(actionTypeEnum, valueField.getText());
      actions.add(action);
    }

    Link link;
    try {
      link = new Link(linkTextField.getText(), selectedLink.getLink().getReference(), actions);
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error updating link", e.getMessage());
      return;
    }
    List<Node> links = diagramPane.getLinkContainer().getChildren();
    links
      .stream()
      .filter(n -> n.equals(selectedLink))
      .findFirst()
      .ifPresent(n -> {
        ((PassageNodeLink) n).setLink(link);
      });

    try {
      story
        .getPassages()
        .stream()
        .filter(p -> p.equals(selectedLink.getSource().getPassage()))
        .findFirst()
        .ifPresent(p -> {
          if (p.removeLink(link)) p.addLink(link);
        });
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error updating link", e.getMessage());
    }
  }

  /**
   * Deletes the link from the diagram pane
   * Removes it from the link container and the story
   * @param event ActionEvent - the event that triggered the delete
   */
  @Override
  public void deleteLink(ActionEvent event) {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) storyEditorView.lookup(
      DIAGRAM_PANE_LOOKUP
    );
    if (Objects.isNull(diagramPane)) {
      FeedbackDialogs.showErrorDialog(
        DIAGRAM_NOT_FOUND_ERROR_TITLE,
        DIAGRAM_NOT_FOUND_ERROR_CONTENT
      );
      return;
    }
    if (Objects.isNull(selectedLink)) {
      FeedbackDialogs.showErrorDialog(
        "Error deleting link",
        "No link selected. Please select a link by double clicking on it and try again."
      );
      return;
    }

    diagramPane.getLinkContainer().getChildren().remove(selectedLink);
    try {
      story
        .getPassages()
        .stream()
        .filter(p -> p.equals(selectedLink.getSource().getPassage()))
        .findFirst()
        .ifPresent(p -> {
          p.removeLink(selectedLink.getLink());
        });
      selectedLink = null;
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error deleting link", e.getMessage());
    }
  }

  /**
   * A recursive method to get the desired node from a node's parent
   * @param clazz Class - the class of the node to get
   * @param node Node - the node to get the parent from
   * @return T - the parent of the node of type T or null
   */
  private <T extends Node> T getNodeFromParents(Class<T> clazz, Node node)
    throws NullPointerException {
    if (node == null) throw new NullPointerException(
      "No " + clazz.getSimpleName() + " found in parents"
    );
    if (clazz.isInstance(node)) {
      return clazz.cast(node);
    }
    return getNodeFromParents(clazz, node.getParent());
  }

  /**
   * Adds a new action form to the link action container which is empty
   * @param event ActionEvent - the button event that triggered the add
   */
  @Override
  public void addLinkAction(ActionEvent event) {
    Pane linkActionContainer = (Pane) storyEditorView.lookup("#linkActionFormContainer");
    if (Objects.isNull(linkActionContainer)) {
      FeedbackDialogs.showErrorDialog(
        "Error adding new action",
        "Could not load the list of actions. Please try again or restart the application."
      );
      return;
    }
    HBox singleActionForm = createSingleActionForm(null, "");
    linkActionContainer.getChildren().add(singleActionForm);
  }

  /**
   * Creates a single action form with a choice box of actions and a text field for the value
   * @param actionType Class - the type of action to have selected. Null means no selection
   * @param value String - the value of the action
   * @return HBox - the action form
   */
  @Override
  public HBox createSingleActionForm(Class<? extends Action<?>> actionType, String value) {
    HBox singleActionForm = new HBox();

    ChoiceBox<ChoiceBoxOption<Class<? extends Action<?>>>> actionTypeChoiceBox = new ChoiceBox<>();
    actionTypeChoiceBox.setId("actionTypeChoiceBox");
    actionTypeChoiceBox
      .getItems()
      .addAll(
        List.of(
          new ChoiceBoxOption<>("Modify gold", GoldAction.class),
          new ChoiceBoxOption<>("Modify score", ScoreAction.class),
          new ChoiceBoxOption<>("Modify health", HealthAction.class),
          new ChoiceBoxOption<>("Add to inventory", InventoryAddAction.class),
          new ChoiceBoxOption<>("Remove all from inventory", InventoryRemoveAllAction.class)
        )
      );
    actionTypeChoiceBox.setValue(
      actionTypeChoiceBox
        .getItems()
        .stream()
        .filter(c -> Objects.equals(c.getValue(), actionType))
        .findFirst()
        .orElse(null)
    );

    TextField actionValueField = new TextField(value);
    actionValueField.setId("actionValueField");
    singleActionForm.getChildren().addAll(actionTypeChoiceBox, actionValueField);
    return singleActionForm;
  }

  /**
   * Updates the story title.
   * Shows error dialogs if an error occurs.
   */
  @Override
  public void updateStoryTitle() {
    TextField storyTitleField = (TextField) storyEditorView.lookup("#storyTitleField");
    if (Objects.isNull(storyTitleField)) {
      FeedbackDialogs.showErrorDialog(
        "Error getting title",
        "Could not get the field for the story title"
      );
      return;
    }
    try {
      story.setTitle(storyTitleField.getText());
    } catch (IllegalArgumentException | NullPointerException e) {
      FeedbackDialogs.showErrorDialog("Invalid story title", e.getMessage());
      storyTitleField.undo();
    }
  }

  /**
   * Exports the story to a file
   * The file is a .paths file which is a custom file type for this application
   * @param event ActionEvent - the button event that triggered the export
   */
  @Override
  public void exportStory(ActionEvent event) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Export Story");
    fileChooser.setInitialFileName(story.getTitle() + ".paths");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Paths", "*.paths"));
    File file = fileChooser.showSaveDialog(storyEditorView.getScene().getWindow());
    if (file != null) {
      try {
        StoryFileWriter storyFileWriter = new StoryFileWriter(file);
        storyFileWriter.writeStory(story);
      } catch (Exception e) {
        FeedbackDialogs.showErrorDialog("Error exporting story", e.getMessage());
      }
      FeedbackDialogs.showInformationDialog(
        "Story exported",
        "You're story was exported to \n" + file.getPath()
      );
    }
  }

  /**
   * Plays the story. Changes the scene to the story starter pane
   * @param event ActionEvent - the button event that triggered the play
   */
  @Override
  public void playStory(ActionEvent event) {
    if (!story.isPlayable()) {
      FeedbackDialogs.showErrorDialog(
        "Story not playable",
        "The story is not playable. Make sure you have an opening passage."
      );
      return;
    }
    StoryStarterPane storyStarterPane = new StoryStarterPane(new StoryStarterControllerImpl(story));
    App.changeRootPane(storyStarterPane);
  }

  /**
   * Retrives the story title from the story
   * @return String - the story title
   */
  @Override
  public String getStoryTitle() {
    return story.getTitle();
  }
}
