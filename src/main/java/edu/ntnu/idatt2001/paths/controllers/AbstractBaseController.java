package edu.ntnu.idatt2001.paths.controllers;

import edu.ntnu.idatt2001.paths.controllers.startmenu.StartMenuControllerImpl;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.startmenu.StartMenuPane;
import javafx.event.ActionEvent;

/**
 * An abstract controller that has common methods for all controllers.
 *
 * @author Callum G
 * @version 1.0 30.04.2023
 */
public abstract class AbstractBaseController implements BaseController {

  /**
   * Goes back to the menu.
   * @param event ActionEvent - The event that triggered the method.
   */
  @Override
  public void goBackToMenu(ActionEvent event) {
    App.changeRootPane(new StartMenuPane(new StartMenuControllerImpl()));
  }
}
