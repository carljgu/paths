package edu.ntnu.idatt2001.paths.controllers.startmenu;

import javafx.event.ActionEvent;

/**
 * Interface for the start menu controller.
 *
 * @author Callum G.
 * @version 1.0 23.04.2023
 */
public interface StartMenuController {
  void showStoryImportPage(ActionEvent event);

  void showStoryEditor(ActionEvent event);

  void showTutorial(ActionEvent event);

  void exitStage(ActionEvent event);
}
