package edu.ntnu.idatt2001.paths.controllers.playgame;

import edu.ntnu.idatt2001.paths.controllers.AbstractBaseController;
import edu.ntnu.idatt2001.paths.controllers.storystarter.StoryStarterControllerImpl;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.game.Game;
import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.FeedbackDialogs;
import edu.ntnu.idatt2001.paths.views.storyplayer.ClickLinkEvent;
import edu.ntnu.idatt2001.paths.views.storyplayer.PlayPassagePane;
import edu.ntnu.idatt2001.paths.views.storystarter.StoryStarterPane;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;

/**
 * Controller for the story starter pane.
 * @author Carl G. Callum G.
 * @version 1.3 23.05.2023
 */
public class PlayGameControllerImpl extends AbstractBaseController implements PlayGameController {

  private final Game game;
  private final Map<Passage, PlayPassagePane> playablePassages;
  private final ObjectProperty<PlayPassagePane> currentPassagePane;
  private final StringProperty gameSubtitle;
  private final StringProperty gameTitle;
  private final BooleanProperty showLinks;

  /**
   * Creates a controller for the story starter pane.
   * @param game The game to play.
   */
  public PlayGameControllerImpl(Game game) {
    this.game = game;
    this.showLinks = new SimpleBooleanProperty(true);
    this.playablePassages = createPassagePanesToStory();
    this.currentPassagePane = new SimpleObjectProperty<PlayPassagePane>(getFirstPassageToDisplay());
    this.gameTitle = new SimpleStringProperty("Playing " + game.getStory().getTitle());
    this.gameSubtitle = new SimpleStringProperty("");
    game
      .getPlayer()
      .addPlayerChangeListener(p -> {
        showLinks.set(p.isAlive());
      });
    updateGameTitle(game, game.getStory().getOpeningPassage());
  }

  /**
   * Maps passages to their panes.
   * @see PlayPassagePane
   * @return Map - The map of passages to their panes.
   */
  private Map<Passage, PlayPassagePane> createPassagePanesToStory() {
    final Map<Passage, PlayPassagePane> passageToPane = new HashMap<>();
    game
      .getStory()
      .getPassages()
      .forEach(p -> {
        passageToPane.put(p, new PlayPassagePane(p, showLinks, this));
      });
    return passageToPane;
  }

  /**
   * Gets the first passage to display.
   * This is the pane for the opening passage
   * @return PlayPassagePane - The first passage to display
   */
  @Override
  public PlayPassagePane getFirstPassageToDisplay() {
    return playablePassages.get(game.getStory().getOpeningPassage());
  }

  /**
   * Gets the clicked link event and updates the game to be the next passage.
   * If the link is broken, an error dialog is shown.
   * Updates the player with actions.
   * Updates the game title and subtitle.
   * @param event ClickLinkEvent - The event that triggered the method. Contains the clicked link
   */
  @Override
  public void goThroughLink(ClickLinkEvent event) {
    Link link = event.getLink();
    Player player = game.getPlayer();
    Passage nextPassage = game.getStory().getPassage(link);
    if (nextPassage == null) {
      FeedbackDialogs.showErrorDialog(
        "Broken link found",
        "This link is broken, and does not lead to a passage."
      );
      return;
    }

    link.getActions().forEach(action -> action.execute(player));
    updateGameTitle(game, nextPassage);
    setCurrentPassagePane(nextPassage);
  }

  /**
   * Updates the game title and subtitle based on the game state.
   * @param game Game - The game to update the title and subtitle for.
   * @param nextPassage Passage - the next passage to go to
   */
  @Override
  public void updateGameTitle(Game game, Passage nextPassage) {
    Player player = game.getPlayer();
    if (game.getGoals().stream().allMatch(goal -> goal.isFulfilled(player))) {
      setGameTitle("You won!");
      if (player.isAlive()) {
        setGameSubtitle("You can still continue the story to explore more");
      } else {
        setGameSubtitle("You died, but you still achieved all your goals!");
      }
    } else if (!player.isAlive()) {
      setGameTitle("You died");
    } else if (nextPassage.getLinks().isEmpty()) {
      setGameTitle("You lost");
      setGameSubtitle("You did not fulfill your goals before the end of the story");
    }
  }

  /**
   * Restarts the game.
   * Shows a confirmation dialog before restarting.
   * @param event ActionEvent - The event that triggered the method.
   */
  @Override
  public void restartGame(ActionEvent event) {
    boolean clickedOk = FeedbackDialogs.showConfirmationDialog(
      "Restarting game",
      "Are you sure you want to restart the game. This game will be lost."
    );
    if (!clickedOk) return;
    StoryStarterPane storyStarterPane = new StoryStarterPane(
      new StoryStarterControllerImpl(game.getStory())
    );
    App.changeRootPane(storyStarterPane);
  }

  /**
   * Switches to the main menu view.
   * Shows a confirmation dialog before changing.
   * @param event ActionEvent - The event that triggered the method.
   */
  @Override
  public void goToMainMenu(ActionEvent event) {
    boolean clickedOk = FeedbackDialogs.showConfirmationDialog(
      "Returing to main menu",
      "Are you sure you want to go to the main menu. This game will be lost."
    );
    if (!clickedOk) return;
    this.goBackToMenu(event);
  }

  /**
   * Gets the game that is being played.
   * @return Game - The game that is being played.
   */
  @Override
  public Game getGame() {
    return game;
  }

  /**
   * Gets the current passage pane.
   * Updates the pane and returns it.
   * @see PlayPassagePane
   */
  @Override
  public void setCurrentPassagePane(Passage passage) {
    currentPassagePane.set(playablePassages.get(passage));
  }

  /**
   * Gets the current passage pane.
   * @return ObjectProperty - The current passage pane.
   */
  @Override
  public ObjectProperty<PlayPassagePane> getCurrentPassagePane() {
    return currentPassagePane;
  }

  /**
   * Gets the current game title string
   * @return StringProperty - The current game title string
   */
  @Override
  public StringProperty getGameTitle() {
    return gameTitle;
  }

  /**
   * Sets the current game title strings
   * @param newTitle String - The new game title string
   */
  @Override
  public void setGameTitle(String newTitle) throws NullPointerException {
    gameTitle.set(Objects.requireNonNull(newTitle, "The game title cannot be null"));
  }

  /**
   * Gets the current game subtitle string
   * @return StringProperty - The current game subtitle string
   */
  @Override
  public StringProperty getGameSubtitle() {
    return gameSubtitle;
  }

  /**
   * Sets the current game subtitle string
   * @param newSubtitle String - The new game subtitle string
   */
  @Override
  public void setGameSubtitle(String newSubtitle) throws NullPointerException {
    gameSubtitle.set(Objects.requireNonNull(newSubtitle, "The game subtitle cannot be null"));
  }
}
