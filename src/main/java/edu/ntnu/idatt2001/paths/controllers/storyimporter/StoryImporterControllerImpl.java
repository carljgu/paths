package edu.ntnu.idatt2001.paths.controllers.storyimporter;

import edu.ntnu.idatt2001.paths.controllers.AbstractBaseController;
import edu.ntnu.idatt2001.paths.controllers.storyeditor.StoryEditorControllerImpl;
import edu.ntnu.idatt2001.paths.controllers.storystarter.StoryStarterControllerImpl;
import edu.ntnu.idatt2001.paths.file.StoryFileReader;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.FeedbackDialogs;
import edu.ntnu.idatt2001.paths.views.storystarter.StoryStarterPane;
import java.io.File;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableStringValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Controller for the story importer pane.
 *
 * @author Callum G.
 * @version 1.0 30.04.2023
 */
public class StoryImporterControllerImpl
  extends AbstractBaseController
  implements StoryImporterController {

  private Story story;

  private StoryFileReader storyFileReader;

  private StringProperty fileName;

  private BooleanProperty storyImportedCorrectly;

  private ObservableList<String> readErrors;

  /**
   * Creates a controller for the story importer pane.
   */
  public StoryImporterControllerImpl() {
    this.fileName = new SimpleStringProperty("File name: ");
    storyImportedCorrectly = new SimpleBooleanProperty(false);
    readErrors = FXCollections.observableArrayList();
  }

  /**
   * Method to import a story from a file.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void importStory(ActionEvent event) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Story File");
    fileChooser
      .getExtensionFilters()
      .add(new FileChooser.ExtensionFilter("Story Files (.paths)", "*.paths"));
    File file = fileChooser.showOpenDialog(new Stage());

    if (file != null) {
      try {
        storyFileReader = new StoryFileReader(file);
        story = storyFileReader.readStory();
        storyImportedCorrectly.set(true);
        ObservableList<String> errors = FXCollections.observableArrayList();
        errors.addAll(storyFileReader.getPassagesSkipped());
        errors.addAll(storyFileReader.getLinksSkipped());
        errors.addAll(storyFileReader.getActionsSkipped());
        readErrors.setAll(
          errors.stream().map(error -> error.replace(System.lineSeparator(), " ")).toList()
        );
        fileName.set("File name: " + file.getName());
      } catch (Exception e) {
        storyImportedCorrectly.set(false);
        FeedbackDialogs.showErrorDialog("Error reading file", e.getMessage());
        readErrors.removeAll();
        fileName.set("File name: ");
        story = null;
      }
    }
  }

  /**
   * Method to play the story.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void playStory(ActionEvent event) {
    if (story != null) {
      App.changeRootPane(new StoryStarterPane(new StoryStarterControllerImpl(story)));
    } else {
      FeedbackDialogs.showErrorDialog("Error playing story", "No valid story has been imported.");
    }
  }

  /**
   * Method to edit the story.
   * Changes the view to the editor.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void editStory(ActionEvent event) {
    StoryEditorControllerImpl editor = new StoryEditorControllerImpl(story);
    App.changeRootPane(editor.getView());
  }

  /**
   * Method to get the file name.
   * @return ObservableStringValue - The file name.
   */
  @Override
  public ObservableStringValue getFileName() {
    return fileName;
  }

  /**
   * Method to get the read errors.
   * @return ObservableList {@literal <}String> - The read errors.
   */
  @Override
  public ObservableList<String> getReadErrors() {
    return readErrors;
  }

  /**
   * Method to get the story imported correctly.
   * @return BooleanProperty - The story imported correctly.
   */
  @Override
  public BooleanProperty getStoryImportedCorrectly() {
    return storyImportedCorrectly;
  }
}
