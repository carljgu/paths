package edu.ntnu.idatt2001.paths.controllers.storyimporter;

import edu.ntnu.idatt2001.paths.controllers.BaseController;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableStringValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

/**
 * Interface for the story importer controller.
 *
 * @author Callum G.
 * @version 0.1 23.05.2023
 */
public interface StoryImporterController extends BaseController {
  void importStory(ActionEvent event);

  void playStory(ActionEvent event);

  void editStory(ActionEvent event);

  ObservableStringValue getFileName();

  ObservableList<String> getReadErrors();

  BooleanProperty getStoryImportedCorrectly();
}
