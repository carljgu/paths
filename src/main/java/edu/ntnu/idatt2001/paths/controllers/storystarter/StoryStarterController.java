package edu.ntnu.idatt2001.paths.controllers.storystarter;

import edu.ntnu.idatt2001.paths.controllers.BaseController;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.game.GameDifficultyEnum;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.scene.layout.VBox;

/**
 * Interface for the story starter controller.
 *
 * @author Callum G.
 * @version 0.1 23.05.2023
 */
public interface StoryStarterController extends BaseController {
  void addGoalField(ActionEvent event);

  void startStory(ActionEvent event);

  Story getStory();

  VBox getGoalsList();

  StringProperty getPlayerName();

  ObjectProperty<GameDifficultyEnum> getDifficulty();
}
