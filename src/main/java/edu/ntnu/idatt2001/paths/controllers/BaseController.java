package edu.ntnu.idatt2001.paths.controllers;

import javafx.event.ActionEvent;

/**
 * An interface that has common methods for all controllers.
 * This is used to make sure that all controllers have the same methods.
 *
 * This is an example of the Interface Segregation Principle.
 *
 * @author Callum G.
 * @version 0.1 23.05.2023
 */
public interface BaseController {
  void goBackToMenu(ActionEvent event);
}
