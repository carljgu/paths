package edu.ntnu.idatt2001.paths.controllers.storystarter;

import edu.ntnu.idatt2001.paths.controllers.AbstractBaseController;
import edu.ntnu.idatt2001.paths.controllers.playgame.PlayGameControllerImpl;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.game.Game;
import edu.ntnu.idatt2001.paths.models.game.GameDifficultyEnum;
import edu.ntnu.idatt2001.paths.models.goals.Goal;
import edu.ntnu.idatt2001.paths.models.goals.GoalTypeEnum;
import edu.ntnu.idatt2001.paths.models.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.models.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.models.goals.InventoryGoal;
import edu.ntnu.idatt2001.paths.models.goals.MaximumScoreGoal;
import edu.ntnu.idatt2001.paths.models.goals.MinimumScoreGoal;
import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.models.player.PlayerBuilder;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.ChoiceBoxOption;
import edu.ntnu.idatt2001.paths.views.FeedbackDialogs;
import edu.ntnu.idatt2001.paths.views.storyplayer.PlayGamePane;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

/**
 * Controller for the story starter pane.
 * Adds functionality for adding new goal fields and starting the story.
 * @author Carl G., Callum G.
 * @version 1.2 23.05.2023
 */
public class StoryStarterControllerImpl
  extends AbstractBaseController
  implements StoryStarterController {

  private final Story story;
  private final VBox goalsList;
  private StringProperty playerName;
  private ObjectProperty<GameDifficultyEnum> difficulty;
  private ListProperty<Pair<GoalTypeEnum, String>> goals;

  /**
   * Creates a controller for the story starter pane.
   * @param story Story - The story to be played.
   */
  public StoryStarterControllerImpl(Story story) {
    this.story = story;
    this.goalsList = new VBox();
    this.playerName = new SimpleStringProperty("");
    this.goals = new SimpleListProperty<>(FXCollections.observableArrayList());
    this.difficulty = new SimpleObjectProperty<>(GameDifficultyEnum.EASY);
  }

  /**
   * Adds new field inputs for a new goal to the pane.
   * @param event ActionEvent - The event that triggered the method.
   */
  @Override
  public void addGoalField(ActionEvent event) {
    HBox goalFields = new HBox();
    goalFields.setSpacing(10);
    goalFields.setPadding(new Insets(10));

    ChoiceBox<ChoiceBoxOption<Class<? extends Goal<?>>>> goalType = new ChoiceBox<>();
    goalType
      .getItems()
      .addAll(
        List.of(
          new ChoiceBoxOption<>("Minimum gold", GoldGoal.class),
          new ChoiceBoxOption<>("Minimum health", HealthGoal.class),
          new ChoiceBoxOption<>("Needs in inventory", InventoryGoal.class),
          new ChoiceBoxOption<>("Maximum score", MaximumScoreGoal.class),
          new ChoiceBoxOption<>("Minimum score", MinimumScoreGoal.class)
        )
      );
    goalType.getSelectionModel().selectFirst();

    TextField goalValue = new TextField();

    goalFields.getChildren().addAll(goalType, goalValue);
    goalsList.getChildren().add(goalFields);

    goals.add(
      new Pair<GoalTypeEnum, String>(
        GoalTypeEnum.getGoalType(goalType.getValue().getValue().getSimpleName()),
        goalValue.textProperty().getValue()
      )
    );

    goalValue
      .textProperty()
      .addListener((observable, oldValue, newValue) -> {
        goals.remove(
          new Pair<GoalTypeEnum, String>(
            GoalTypeEnum.getGoalType(goalType.getValue().getValue().getSimpleName()),
            oldValue
          )
        );
        goals.add(
          new Pair<GoalTypeEnum, String>(
            GoalTypeEnum.getGoalType(goalType.getValue().getValue().getSimpleName()),
            newValue
          )
        );
      });

    goalType
      .valueProperty()
      .addListener((observable, oldValue, newValue) -> {
        goals.remove(
          new Pair<GoalTypeEnum, String>(
            GoalTypeEnum.getGoalType(oldValue.getValue().getSimpleName()),
            goalValue.textProperty().getValue()
          )
        );
        goals.add(
          new Pair<GoalTypeEnum, String>(
            GoalTypeEnum.getGoalType(newValue.getValue().getSimpleName()),
            goalValue.textProperty().getValue()
          )
        );
      });
  }

  /**
   * Get the goals as a list.
   * @return List - The goals as a list.
   */
  private List<Goal<?>> getGoals() {
    List<Goal<?>> goals = new ArrayList<>();

    try {
      for (Pair<GoalTypeEnum, String> goal : this.goals) {
        goals.add(getGoalFromClassAndValue(goal.getKey(), goal.getValue()));
      }
      return goals;
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error loading goals", e.getMessage());
      return null;
    }
  }

  /**
   * Starts the story.
   * Checks all the goals and the difficulty, creates the game and switches scenes.
   * If input fields in the goals pane are empty, they will be ignored.
   * If the story is not playable an error message is sent.
   * @param event ActionEvent - The event that triggered the method.
   */
  @Override
  public void startStory(ActionEvent event) {
    List<Goal<?>> localGoals = getGoals();
    if (localGoals == null) {
      return;
    }
    if (!story.isPlayable()) {
      FeedbackDialogs.showErrorDialog(
        "Error creating game",
        "Story is not playable. This might be because there is no opening passage"
      );
      return;
    }
    if (!story.getBrokenLinks().isEmpty()) {
      boolean confirmed = FeedbackDialogs.showConfirmationDialog(
        "Broken links found",
        "There are passages that have links which do lead anywhere. Do you want to play it anyway?"
      );
      if (!confirmed) {
        return;
      }
    }
    Game game;
    try {
      Player player = new PlayerBuilder(difficulty.getValue())
        .setName(playerName.getValue())
        .build();
      game = new Game(player, story, localGoals);
    } catch (Exception e) {
      FeedbackDialogs.showErrorDialog("Error creating game", e.getMessage());
      return;
    }
    PlayGamePane playStoryPane = new PlayGamePane(new PlayGameControllerImpl(game));
    App.changeRootPane(playStoryPane);
  }

  /**
   * Retrieves a goal from a class and a value.
   * @param goalType Class - The class of the goal. Needs to implement the goal interface.
   * @param value String - The value of the goal. Needs to be converted to the correct type.
   * @return Goal - The goal from the class and value.
   * @throws NumberFormatException if the value cannot be parsed to an integer.
   * @throws NullPointerException if the value is null.
   * @throws IllegalArgumentException if the goal does not support the value.
   */
  private Goal<?> getGoalFromClassAndValue(GoalTypeEnum goalType, String value)
    throws NumberFormatException, NullPointerException, IllegalArgumentException {
    switch (goalType) {
      case GOLD:
        return new GoldGoal(Integer.parseInt(value));
      case HEALTH:
        return new HealthGoal(Integer.parseInt(value));
      case INVENTORY:
        List<String> items = Arrays.stream(value.split(",")).map(String::trim).toList();
        return new InventoryGoal(items);
      case MAXIUMUM_SCORE:
        return new MaximumScoreGoal(Integer.parseInt(value));
      case MINIMUM_SCORE:
        return new MinimumScoreGoal(Integer.parseInt(value));
      default:
        throw new IllegalArgumentException("Goal not supported");
    }
  }

  /**
   * Method to get the story.
   * @return Story - The story.
   */
  @Override
  public Story getStory() {
    return this.story;
  }

  /**
   * Method to get the goals list.
   * @return VBox - The goals list.
   */
  @Override
  public VBox getGoalsList() {
    return this.goalsList;
  }

  /**
   * Method to get the player name value.
   * @return StringProperty - The player name value.
   */
  public StringProperty getPlayerName() {
    return this.playerName;
  }

  /**
   * Method to get the difficulty value.
   * @return ObjectProperty - The difficulty value.
   */
  public ObjectProperty<GameDifficultyEnum> getDifficulty() {
    return this.difficulty;
  }
}
