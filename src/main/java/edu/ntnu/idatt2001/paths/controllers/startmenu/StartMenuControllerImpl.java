package edu.ntnu.idatt2001.paths.controllers.startmenu;

import edu.ntnu.idatt2001.paths.controllers.storyeditor.StoryEditorControllerImpl;
import edu.ntnu.idatt2001.paths.controllers.storyimporter.StoryImporterControllerImpl;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.FeedbackDialogs;
import edu.ntnu.idatt2001.paths.views.storyimporter.StoryImporterPane;
import edu.ntnu.idatt2001.paths.views.tutorial.TutorialPane;
import javafx.event.ActionEvent;

/**
 * The start menu controller.
 * The start menu controller is responsible for handling the actions of the
 * start menu scene.
 * Actions include importing a story from a file, creating a new story,
 * showing the tutorial, and exiting the program.
 * @author Carl G.
 * @version 1.0 22.04.2023
 */
public class StartMenuControllerImpl implements StartMenuController {

  /**
   * Changes the scene of the application to the story import page.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void showStoryImportPage(ActionEvent event) {
    App.changeRootPane(new StoryImporterPane(new StoryImporterControllerImpl()));
  }

  /**
   * Changes the scene of the application to the story editor page.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void showStoryEditor(ActionEvent event) {
    StoryEditorControllerImpl editor = new StoryEditorControllerImpl();
    App.changeRootPane(editor.getView());
  }

  /**
   * Changes the scene of the application to the tutorial page.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void showTutorial(ActionEvent event) {
    App.changeRootPane(new TutorialPane());
  }

  /**
   * Exits the program.
   * Shows a confirmation dialog to the user before exiting the program.
   * @param event ActionEvent - The event that triggered the action.
   */
  @Override
  public void exitStage(ActionEvent event) {
    boolean exitConfirmation = FeedbackDialogs.showConfirmationDialog(
      "Exiting program",
      "Are you sure you want to exit?"
    );
    if (exitConfirmation) {
      System.exit(0);
    }
  }
}
