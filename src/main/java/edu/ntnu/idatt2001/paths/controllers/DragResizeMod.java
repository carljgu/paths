package edu.ntnu.idatt2001.paths.controllers;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 * A class that makes a node resizable and draggable.
 * Inspiration from https://github.com/varren/JavaFX-Resizable-Draggable-Node
 * <pre></pre>
 *
 * ************* How to use ************************
 * <pre>
 *
 * Rectangle rectangle = new Rectangle(50, 50);
 * rectangle.setFill(Color.BLACK);
 * DragResizeMod.makeResizable(rectangle, null);
 *
 * Pane root = new Pane();
 * root.getChildren().add(rectangle);
 *
 * primaryStage.setScene(new Scene(root, 300, 275));
 * primaryStage.show();
 * </pre>
 *
 * ************* OnDragResizeEventListener **********
 *<pre></pre>
 * You need to override OnDragResizeEventListener and
 * 1) preform out of main field bounds check
 * 2) make changes to the node
 * (this class will not change anything in node coordinates)
 *
 * There is defaultListener and it works only with Canvas nad Rectangle
 *
 * @author Carl G.
 * @version 0.1 30.04.2023
 */
public class DragResizeMod {

  /**
   * Interface to listen for node drag and resize events.
   * Calls onDrag when the node is dragged by mouse event
   * Calls onResize when the node is resized by mouse event on the corners or edges
   */
  public interface OnDragResizeEventListener {
    void onDrag(Node node, double x, double y, double h, double w);

    void onResize(Node node, double x, double y, double h, double w);
  }

  /**
   * The different states the node can be in
   */
  public enum State {
    DEFAULT,
    DRAG,
    NW_RESIZE,
    SW_RESIZE,
    NE_RESIZE,
    SE_RESIZE,
    E_RESIZE,
    W_RESIZE,
    N_RESIZE,
    S_RESIZE,
  }

  private double clickX, clickY, nodeX, nodeY, nodeH, nodeW;

  private State state = State.DEFAULT;

  private final Node node;
  private OnDragResizeEventListener listener;

  private static final int MARGIN = 8;
  private static final double MIN_W = 30;
  private static final double MIN_H = 20;

  /**
   * Constructs a modifcation for a node to make it resizable and draggable
   * Private to force use of static method makeResizable
   * @param node Node - the node to make resizable and draggable
   * @param listener OnDragResizeEventListener - the listener to call when the node is dragged or resized
   */
  private DragResizeMod(Node node, OnDragResizeEventListener listener) {
    this.node = node;
    if (listener != null) this.listener = listener;
  }

  /**
   * Checks if the mouse event is in the drag zone
   * Creates a resizer which is called on mouse events.
   * @param node Node - the node that is being dragged or resized
   * @param listener OnDragResizeEventListener - the listener to call when the node is dragged or resized
   */
  public static void makeResizable(Node node, OnDragResizeEventListener listener) {
    final DragResizeMod resizer = new DragResizeMod(node, listener);
    node.setOnMousePressed(resizer::mousePressed);
    node.setOnMouseDragged(resizer::mouseDragged);
    node.setOnMouseMoved(resizer::mouseOver);
    node.setOnMouseReleased(resizer::mouseReleased);
  }

  /**
   * Sets the cursor to be in default state
   * @param event MouseEvent - the mouse event to check on release
   */
  protected void mouseReleased(MouseEvent event) {
    node.setCursor(Cursor.DEFAULT);
    state = State.DEFAULT;
  }

  /**
   * Sets the state of the cursor based on the event
   * @param event MouseEvent - the mouse event to check
   */
  protected void mouseOver(MouseEvent event) {
    State state = currentMouseState(event);
    Cursor cursor = getCursorForState(state);
    node.setCursor(cursor);
  }

  /**
   * Gets the state of the mouse event.
   * @param event MouseEvent - the mouse event to check
   * @return State - the state of the mouse event
   */
  private State currentMouseState(MouseEvent event) {
    State state = State.DEFAULT;
    boolean left = isLeftResizeZone(event);
    boolean right = isRightResizeZone(event);
    boolean top = isTopResizeZone(event);
    boolean bottom = isBottomResizeZone(event);

    if (left && top) state = State.NW_RESIZE; else if (left && bottom) state =
      State.SW_RESIZE; else if (right && top) state = State.NE_RESIZE; else if (
      right && bottom
    ) state = State.SE_RESIZE; else if (right) state = State.E_RESIZE; else if (left) state =
      State.W_RESIZE; else if (top) state = State.N_RESIZE; else if (bottom) state =
      State.S_RESIZE; else if (isInDragZone(event)) state = State.DRAG;

    return state;
  }

  /**
   * Turns a state into a cursor
   * If a state is not found, the default cursor is returned
   * @param state State - the state to turn into a cursor
   * @return Cursor - the cursor for the state
   */
  private static Cursor getCursorForState(State state) {
    return switch (state) {
      case NW_RESIZE -> Cursor.NW_RESIZE;
      case SW_RESIZE -> Cursor.SW_RESIZE;
      case NE_RESIZE -> Cursor.NE_RESIZE;
      case SE_RESIZE -> Cursor.SE_RESIZE;
      case E_RESIZE -> Cursor.E_RESIZE;
      case W_RESIZE -> Cursor.W_RESIZE;
      case N_RESIZE -> Cursor.N_RESIZE;
      case S_RESIZE -> Cursor.S_RESIZE;
      default -> Cursor.DEFAULT;
    };
  }

  /**
   * If the state is drag, the node is moved around according to the mouse drag event
   * If the state is not drag or default, the node is resized according to the mouse drag event
   * @param event MouseEvent - the mouse event to check
   */
  protected void mouseDragged(MouseEvent event) {
    if (listener != null) {
      double mouseX = parentX(event.getX());
      double mouseY = parentY(event.getY());
      if (state == State.DRAG) {
        listener.onDrag(node, mouseX - clickX, mouseY - clickY, nodeH, nodeW);
      } else if (state != State.DEFAULT) {
        //resizing
        double newX = nodeX;
        double newY = nodeY;
        double newH = nodeH;
        double newW = nodeW;

        // Right Resize
        if (state == State.E_RESIZE || state == State.NE_RESIZE || state == State.SE_RESIZE) {
          newW = mouseX - nodeX;
        }
        // Left Resize
        if (state == State.W_RESIZE || state == State.NW_RESIZE || state == State.SW_RESIZE) {
          newX = mouseX;
          newW = nodeW + nodeX - newX;
        }

        // Bottom Resize
        if (state == State.S_RESIZE || state == State.SE_RESIZE || state == State.SW_RESIZE) {
          newH = mouseY - nodeY;
        }
        // Top Resize
        if (state == State.N_RESIZE || state == State.NW_RESIZE || state == State.NE_RESIZE) {
          newY = mouseY;
          newH = nodeH + nodeY - newY;
        }

        //min valid rect Size Check
        if (newW < MIN_W) {
          if (
            state == State.W_RESIZE || state == State.NW_RESIZE || state == State.SW_RESIZE
          ) newX = newX - MIN_W + newW;
          newW = MIN_W;
        }

        if (newH < MIN_H) {
          if (
            state == State.N_RESIZE || state == State.NW_RESIZE || state == State.NE_RESIZE
          ) newY = newY + newH - MIN_H;
          newH = MIN_H;
        }

        listener.onResize(node, newX, newY, newH, newW);
      }
    }
    event.consume();
  }

  /**
   * Sets the correct state based on the mouse position
   * @param event MouseEvent - the mouse event to check
   */
  protected void mousePressed(MouseEvent event) {
    if (isInResizeZone(event)) {
      setNewInitialEventCoordinates(event);
      state = currentMouseState(event);
    } else if (isInDragZone(event)) {
      setNewInitialEventCoordinates(event);
      state = State.DRAG;
    } else {
      state = State.DEFAULT;
    }
  }

  /**
   * Sets the node coordinates to default values
   * @param event MouseEvent - the mouse event
   */
  private void setNewInitialEventCoordinates(MouseEvent event) {
    nodeX = nodeX();
    nodeY = nodeY();
    nodeH = nodeH();
    nodeW = nodeW();
    clickX = event.getX();
    clickY = event.getY();
  }

  /**
   * Checks if the mouse is in the resize zone
   * @param event MouseEvent - the mouse event to check
   * @return boolean - true if the mouse is in the resize zone, false otherwise
   */
  private boolean isInResizeZone(MouseEvent event) {
    return (
      isLeftResizeZone(event) ||
      isRightResizeZone(event) ||
      isBottomResizeZone(event) ||
      isTopResizeZone(event)
    );
  }

  /**
   * Checks if the mouse is in the drag zone
   * @param event MouseEvent - the mouse event to check
   * @return boolean - true if the mouse is in the drag zone, false otherwise
   */
  private boolean isInDragZone(MouseEvent event) {
    double xPos = parentX(event.getX());
    double yPos = parentY(event.getY());
    double nodeX = nodeX() + MARGIN;
    double nodeY = nodeY() + MARGIN;
    double nodeX0 = nodeX() + nodeW() - MARGIN;
    double nodeY0 = nodeY() + nodeH() - MARGIN;

    return (xPos > nodeX && xPos < nodeX0) && (yPos > nodeY && yPos < nodeY0);
  }

  /**
   * Checks if the event click is in the left resize zone
   * @param event MouseEvent - the mouse event to check
   * @return boolean - if the mouse is in the left resize zone
   */
  private boolean isLeftResizeZone(MouseEvent event) {
    return intersect(0, event.getX());
  }

  /**
   * Checks if the event click is in the right resize zone
   * @param event MouseEvent - the mouse event to check
   * @return boolean - if the mouse is in the right resize zone
   */
  private boolean isRightResizeZone(MouseEvent event) {
    return intersect(nodeW(), event.getX());
  }

  /**
   * Checks if the event click is in the top resize zone
   * @param event MouseEvent - the mouse event to check
   * @return boolean - if the mouse is in the top resize zone
   */
  private boolean isTopResizeZone(MouseEvent event) {
    return intersect(0, event.getY());
  }

  /**
   * Checks if the event click is in the bottom resize zone
   * @param event MouseEvent - the mouse event to check
   * @return boolean - if the mouse is in the bottom resize zone
   */
  private boolean isBottomResizeZone(MouseEvent event) {
    return intersect(nodeH(), event.getY());
  }

  /**
   * Checks if there is an intersection
   * @param side double - the side to check
   * @param point double - the point to check
   * @return boolean - if there is an intersection
   */
  private boolean intersect(double side, double point) {
    return side + MARGIN > point && side - MARGIN < point;
  }

  /**
   * Gets the parents x coordinate
   * @param event MouseEvent - the mouse event to check
   * @return double - the parents x coordinate
   */
  private double parentX(double localX) {
    return nodeX() + localX;
  }

  /**
   * Gets the parents y coordinate
   * @param event MouseEvent - the mouse event to check
   * @return double - the parents y coordinate
   */
  private double parentY(double localY) {
    return nodeY() + localY;
  }

  /**
   * Gets the node's x position
   * @return double - the node's x position
   */
  private double nodeX() {
    return node.getBoundsInParent().getMinX();
  }

  /**
   * Gets the node's y position
   * @return double - the node's y position
   */
  private double nodeY() {
    return node.getBoundsInParent().getMinY();
  }

  /**
   * Gets the node's width
   * @return double - the node's width
   */
  private double nodeW() {
    return node.getBoundsInParent().getWidth();
  }

  /**
   * Gets the node's height
   * @return double - the node's height
   */
  private double nodeH() {
    return node.getBoundsInParent().getHeight();
  }
}
