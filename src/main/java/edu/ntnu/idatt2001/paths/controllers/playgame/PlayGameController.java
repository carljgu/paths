package edu.ntnu.idatt2001.paths.controllers.playgame;

import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.game.Game;
import edu.ntnu.idatt2001.paths.views.storyplayer.ClickLinkEvent;
import edu.ntnu.idatt2001.paths.views.storyplayer.PlayPassagePane;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;

/**
 * Interface for the play game controller.
 *
 * @author Callum G.
 * @version 1.1 23.05.2023
 */
public interface PlayGameController {
  PlayPassagePane getFirstPassageToDisplay();

  void goThroughLink(ClickLinkEvent event);

  void updateGameTitle(Game game, Passage nextPassage);

  void restartGame(ActionEvent event);

  void goToMainMenu(ActionEvent event);

  Game getGame();

  void setCurrentPassagePane(Passage passage);

  ObjectProperty<PlayPassagePane> getCurrentPassagePane();

  StringProperty getGameTitle();

  void setGameTitle(String newTitle) throws NullPointerException;

  StringProperty getGameSubtitle();

  void setGameSubtitle(String newSubtitle) throws NullPointerException;
}
