package edu.ntnu.idatt2001.paths.controllers;

/**
 * A controller to go back to the menu.
 * @author Carl G. Callum G.
 * @version 0.2 30.04.2023
 */
public class TutorialPageController extends AbstractBaseController {}
