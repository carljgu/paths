package edu.ntnu.idatt2001.paths.controllers.storyeditor;

import edu.ntnu.idatt2001.paths.controllers.DragResizeMod;
import edu.ntnu.idatt2001.paths.views.storyeditor.DragResizePassageNode;
import javafx.scene.Node;

/**
 * A drag resize event listener for the drag resize mod.
 * This listener is responsible for handling the drag and resize events
 * @author Carl G.
 * @version 0.1 30.04.2023
 */
public class PassageNodeDragResizeListener implements DragResizeMod.OnDragResizeEventListener {

  /**
   * Overrides the on drag method from the drag resize event listener.
   * Sets the node size to the given parameters.
   * @param node Node - The node to resize.
   * @param x double - The x position of the node.
   * @param y double - The y position of the node.
   * @param h double - The height of the node.
   * @param w double - The width of the node.
   * @see DragResizeMod.OnDragResizeEventListener#onDrag(Node, double, double, double, double)
   */
  @Override
  public void onDrag(Node node, double x, double y, double h, double w) {
    setNodeLayoutAndSize(node, x, y, h, w);
  }

  /**
   * Overrides the on resize method from the drag resize event listener.
   * Sets the node size to the given parameters.
   * @param node Node - The node to resize.
   * @param x double - The x position of the node.
   * @param y double - The y position of the node.
   * @param h double - The height of the node.
   * @param w double - The width of the node.
   * @see DragResizeMod.OnDragResizeEventListener#onResize(Node, double, double, double, double)
   */
  @Override
  public void onResize(Node node, double x, double y, double h, double w) {
    setNodeLayoutAndSize(node, x, y, h, w);
  }

  /**
   * Sets the node size to the given parameters.
   * @param node Node - The node to resize.
   * @param x double - The x position of the node.
   * @param y double - The y position of the node.
   * @param h double - The height of the node.
   * @param w double - The width of the node.
   */
  private void setNodeLayoutAndSize(Node node, double x, double y, double h, double w) {
    if (!(node instanceof DragResizePassageNode passageNode)) {
      return;
    }
    passageNode.setLayoutX(x);
    passageNode.setLayoutY(y);
    passageNode.setMaxSize(w, h);
    passageNode.setMinSize(w, h);
  }
}
