package edu.ntnu.idatt2001.paths.validation;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import edu.ntnu.idatt2001.paths.file.FileExtensionEnum;
import edu.ntnu.idatt2001.paths.file.FileUtils;
import java.io.File;
import java.util.Objects;

/**
 * Class for story file validation.
 *
 * @author Callum G.
 * @version 0.1 23.04.2023
 */
public class StoryFileValidation {

  /**
   * Method to check if the file extension is valid for the story file.
   * @param <T> the file class that extends the File class
   * @param file T - the file to check
   * @return boolean - true if the extension is valid, false if not
   * @throws NullPointerException if the file name is null
   * @throws BadFileNameException if the file name does not contain an extension
   */
  public static <T extends File> boolean isValidFileType(final T file)
    throws NullPointerException, BadFileNameException {
    return FileUtils.getFileExtension(file).equals(FileExtensionEnum.PATHS.getExtension());
  }

  /**
   * Method to check if a line is a valid passage start.
   * @param line String - the line to check
   * @return boolean - true if the file line is valid, false if not
   * @throws NullPointerException if the line is null
   */
  public static boolean isValidPassageStart(final String line) throws NullPointerException {
    return RegexEnum.matches(line, RegexEnum.PASSAGE_TITLE);
  }

  /**
   * Method to check if a line has a valid link.
   * @param line String - the line to check
   * @return boolean - true if the file line is valid, false if not
   * @throws NullPointerException if the line is null
   */
  public static boolean isValidLink(final String line) throws NullPointerException {
    Objects.requireNonNull(line, "The line cannot be null");
    if (!line.contains(")")) return false;
    if (!RegexEnum.matches(line, RegexEnum.LINK_PARENTHESES)) return false;
    final String linkString = line.substring(0, line.indexOf(")") + 1);
    return RegexEnum.matches(linkString, RegexEnum.LINK);
  }

  /**
   * Method to check if an action is in a valid format.
   * @param actionString String - the action to check
   * @return boolean - true if the file action is valid, false if not
   * @throws NullPointerException if the action is null
   */
  public static boolean isValidAction(final String actionString) throws NullPointerException {
    return RegexEnum.matches(actionString, RegexEnum.ACTION);
  }

  /**
   * Method to check if a link contains actions.
   * @param link String - the link to check
   * @return boolean - true if the link contains actions, false if not
   * @throws NullPointerException if the link is null
   */
  public static boolean containsActions(final String link) throws NullPointerException {
    return RegexEnum.matches(link, RegexEnum.CONTAINS_ACTIONS);
  }
}
