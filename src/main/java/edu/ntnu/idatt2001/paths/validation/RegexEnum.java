package edu.ntnu.idatt2001.paths.validation;

import java.util.Objects;

/**
 * Enum for the different regular expressions used in the program.
 *
 * @author Callum G.
 * @version 0.1 23.04.2023
 */
public enum RegexEnum {
  /**
   * Passage title.
   * Matches the following:
   * <pre>::Passage title</pre>
   */
  PASSAGE_TITLE("^:{2}.{1,}$"),
  /**
   * Link.
   * Matches the following:
   * <pre>[link text](passage title)</pre>
   */
  LINK("^\\[{1}.*(?=\\]).(?=\\().*(?=\\)).$"),
  /**
   * Link with parentheses.
   * Matches the following:
   * <pre>...) or ...){@literal <}{action type}(value){@literal >}</pre>
   */
  LINK_PARENTHESES("^.*(?=\\)).(?=$|(?=\\<).(?=\\{)).*$"),
  /**
   * Link contains actions.
   * Matches the following:
   * <pre>...){@literal <}{...}{@literal >}</pre>
   */
  CONTAINS_ACTIONS("^.*(?=\\)).(?=\\<).(?=\\{).*(?=\\)).(?=\\>).$"),
  /**
   * Action.
   * Matches the following:
   * <pre>{Action type}(value)</pre>
   */
  ACTION("^\\{.*(?=\\}).(?=\\().*(?=\\)).$");

  private final String pattern;

  /**
   * Constructor for RegexEnum.
   * @param pattern The pattern to be used.
   */
  RegexEnum(String pattern) {
    this.pattern = pattern;
  }

  /**
   * Get the pattern.
   * @return The pattern.
   */
  public String getPattern() {
    return pattern;
  }

  /**
   * Check if a string matches the pattern.
   *
   * @param string The string to check.
   * @param pattern The pattern to check against.
   * @return Predicate of whether the string matches the pattern.
   * @throws NullPointerException If the string is null.
   */
  public static boolean matches(final String string, final RegexEnum pattern) {
    return Objects
      .requireNonNull(string, "The string cannot be null.")
      .matches(pattern.getPattern());
  }
}
