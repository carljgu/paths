package edu.ntnu.idatt2001.paths.validation;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import edu.ntnu.idatt2001.paths.file.FileExtensionEnum;
import edu.ntnu.idatt2001.paths.file.FileUtils;
import java.io.File;

/**
 * Class for tutorial file validation.
 *
 * @author Carl G.
 * @version 1.0 22.05.2023
 */
public class TutorialFileValidation {

  /**
   * Method to check if the file extension is valid for the tutorial file.
   * @param <T> the file class that extends the File class
   * @param file T - the file to check
   * @return boolean - true if the extension is valid, false if not
   * @throws NullPointerException if the file name is null
   * @throws BadFileNameException if the file name does not contain an extension
   */
  public static <T extends File> boolean isValidFileType(final T file)
    throws NullPointerException, BadFileNameException {
    return FileUtils.getFileExtension(file).equals(FileExtensionEnum.TUTORIAL.getExtension());
  }
}
