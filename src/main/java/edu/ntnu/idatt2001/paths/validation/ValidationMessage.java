package edu.ntnu.idatt2001.paths.validation;

/**
 * Enum for validation messages.
 * Contains default messages to be thrown by the validation class.
 * @author Callum G.
 * @version 0.2 - 29.04.2023
 */
public enum ValidationMessage {
  /**
   * The default message to be thrown if a collection is null
   */
  COLLECTION_IS_NULL("The collection cannot be null"),
  /**
   * The default message to be thrown if a collection is empty
   */
  COLLECTION_IS_EMPTY("The collection cannot be empty"),
  /**
   * The default message to be thrown if a collection contains null values
   */
  COLLECTION_CONTAINS_NULL("The collection cannot contain null"),
  /**
   * The default message to be thrown if a collection of strings contains a blank string.
   */
  COLLECTION_CONTAINS_BLANK("The collection cannot contain blank strings"),
  /**
   * The default message to be thrown if a map is null.
   */
  MAP_IS_NULL("The map cannot be null"),
  /**
   * The default message to be thrown if a map is empty.
   */
  MAP_IS_EMPTY("The map cannot be empty"),
  /**
   * The default message to be thrown if a map contains null values.
   */
  MAP_CONTAINS_NULL("The map cannot contain null values"),
  /**
   * The default message to be thrown if a string is null.
   */
  STRING_IS_NULL("The string cannot be null."),
  /**
   * The default message to be thrown if a string is blank.
   */
  STRING_IS_BLANK("The string cannot be blank."),
  /**
   * The default message to be thrown if a string is empty.
   */
  NUMBER_IS_SMALLER("The number cannot be smaller than the minimum value."),
  /**
   * The default message to be thrown if a number is larger than the maximum value.
   */
  NUMBER_IS_LARGER("The number cannot be larger than the maximum value."),
  /**
   * The default message to be thrown if a number is not positive.
   */
  NUMBER_IS_NOT_POSITIVE("The number cannot be negative.");

  private final String message;

  /**
   * Constructor for the Validation messages
   * @param message The message to be given
   */
  private ValidationMessage(String message) {
    this.message = message;
  }

  /**
   * Getter method for messages in the enum
   * @return String - the current enum value
   */
  public String getMessage() {
    return this.message;
  }
}
