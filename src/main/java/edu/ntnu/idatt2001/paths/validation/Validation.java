package edu.ntnu.idatt2001.paths.validation;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Class for validating objects.
 * Base class for validation of different classes in models.
 * All methods return the object if it is valid.
 * @author Callum G.
 * @version 0.1 - 29.04.2023
 */
public class Validation {

  /**
   * Check if a collection of a given type is null or contains null values.
   * @param <T> A subclass of the class collection.
   * @param <V> The type in the collection.
   * @param collection The collection to check.
   * @param nullMessage The message to be given if the collection is null
   * @param containsNullMessage The message to be given if the collection contains null
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection is empty
   */
  public static <T extends Collection<V>, V> T requireNonNullOrContainsNull(
    T collection,
    String nullMessage,
    String containsNullMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (
      Objects.requireNonNull(collection, nullMessage).stream().anyMatch(Objects::isNull)
    ) throw new NullPointerException(containsNullMessage);
    return collection;
  }

  /**
   * Check if a collection of a given type is null or contains null values.
   * Uses default messages.
   * @param <T> A subclass of the class collection.
   * @param <V> The type in the collection.
   * @param collection The collection to check.
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection is empty
   */
  public static <T extends Collection<V>, V> T requireNonNullOrContainsNull(T collection)
    throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrContainsNull(
      collection,
      ValidationMessage.COLLECTION_IS_NULL.getMessage(),
      ValidationMessage.COLLECTION_CONTAINS_NULL.getMessage()
    );
  }

  /**
   * Check if a collection of a given type is null or empty.
   * @param <T> A subclass of the class collection.
   * @param <V> The type in the collection.
   * @param collection The collection to check.
   * @param nullMessage The message to be given if the collection is null
   * @param emptyMessage The message to be given if the collection is empty
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null
   * @throws IllegalArgumentException if the collection is empty
   */
  public static <T extends Collection<V>, V> T requireNonNullOrEmpty(
    T collection,
    String nullMessage,
    String emptyMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (
      Objects.requireNonNull(collection, nullMessage).isEmpty()
    ) throw new IllegalArgumentException(emptyMessage);
    return collection;
  }

  /**
   * Check if a collection of a given type is null or empty.
   * Uses default messages.
   * @param <T> A subclass of the class collection.
   * @param <V> The type in the collection.
   * @param collection The collection to check.
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null
   * @throws IllegalArgumentException if the collection is empty
   */
  public static <T extends Collection<V>, V> T requireNonNullOrEmpty(T collection)
    throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrEmpty(
      collection,
      ValidationMessage.COLLECTION_IS_NULL.getMessage(),
      ValidationMessage.COLLECTION_IS_EMPTY.getMessage()
    );
  }

  /**
   * Check if a collection of a given type is null or empty or contains null values.
   * @param <T> A subclass of the class collection.
   * @param <V> The type in the collection.
   * @param collection The collection to check.
   * @param nullMessage The message to be given if the collection is null
   * @param emptyMessage The message to be given if the collection is empty
   * @param containsNullMessage The message to be given if the collection contains null
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection is empty
   */
  public static <T extends Collection<V>, V> T requireNonNullOrEmptyOrContainsNull(
    T collection,
    String nullMessage,
    String emptyMessage,
    String containsNullMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (
      requireNonNullOrEmpty(collection, nullMessage, emptyMessage)
        .stream()
        .anyMatch(Objects::isNull)
    ) throw new NullPointerException(containsNullMessage);
    return collection;
  }

  /**
   * Check if a collection of a given type is null or empty or contains null values.
   * Uses default messages.
   * @param <T> A subclass of the class collection.
   * @param <V> The type in the collection.
   * @param collection The collection to check.
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection is empty
   */
  public static <T extends Collection<V>, V> T requireNonNullOrEmptyOrContainsNull(T collection)
    throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrEmptyOrContainsNull(
      collection,
      ValidationMessage.COLLECTION_IS_NULL.getMessage(),
      ValidationMessage.COLLECTION_IS_EMPTY.getMessage(),
      ValidationMessage.COLLECTION_CONTAINS_NULL.getMessage()
    );
  }

  /**
   * Check if a collection of a strings is null or contains null values or blank strings.
   * @param <T> A subclass of the class collection that contains strings.
   * @param collection The collection of strings to check.
   * @param nullMessage The message to be given if the collection is null
   * @param containsNullMessage The message to be given if the collection contains null
   * @param containsBlankMessage The message to be given if the collection contains a blank string
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection contains blank strings
   */
  public static <T extends Collection<String>> T requireNonNullOrContainsNullOrBlank(
    T collection,
    String nullMessage,
    String containsNullMessage,
    String containsBlankMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (!Objects.requireNonNull(collection, nullMessage).isEmpty()) {
      if (collection.stream().anyMatch(Objects::isNull)) throw new NullPointerException(
        containsNullMessage
      );
      if (collection.stream().anyMatch(String::isBlank)) throw new IllegalArgumentException(
        containsBlankMessage
      );
    }
    return collection;
  }

  /**
   * Check if a collection of a strings is null or contains null values or blank strings.
   * Uses default messages.
   * @param <T> A subclass of the class collection that contains strings.
   * @param collection The collection of strings to check.
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection contains blank strings
   */
  public static <T extends Collection<String>> T requireNonNullOrContainsNullOrBlank(T collection)
    throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrContainsNullOrBlank(
      collection,
      ValidationMessage.COLLECTION_IS_NULL.getMessage(),
      ValidationMessage.COLLECTION_CONTAINS_NULL.getMessage(),
      ValidationMessage.COLLECTION_CONTAINS_BLANK.getMessage()
    );
  }

  /**
   * Check if a collection of a strings is null or empty or contains null values or blank strings.
   * @param <T> A subclass of the class collection that contains strings.
   * @param collection The collection of strings to check.
   * @param nullMessage The message to be given if the collection is null
   * @param emptyMessage The message to be given if the collection is empty
   * @param containsNullMessage The message to be given if the collection contains null
   * @param containsBlankMessage The message to be given if the collection contains a blank string
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection is empty or contains blank strings
   */
  public static <T extends Collection<String>> T requireNonNullOrEmptyOrContainsNullOrBlank(
    T collection,
    String nullMessage,
    String emptyMessage,
    String containsNullMessage,
    String containsBlankMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (
      requireNonNullOrEmptyOrContainsNull(
        collection,
        nullMessage,
        emptyMessage,
        containsNullMessage
      )
        .stream()
        .anyMatch(String::isBlank)
    ) throw new IllegalArgumentException(containsBlankMessage);
    return collection;
  }

  /**
   * Check if a collection of a strings is null or empty or contains null values or blank strings.
   * Uses default messages.
   * @param <T> A subclass of the class collection that contains strings.
   * @param collection The collection of strings to check.
   * @return Collection - the checked collection
   * @throws NullPointerException if the collection is null or contains null
   * @throws IllegalArgumentException if the collection is empty or contains blank strings
   */
  public static <T extends Collection<String>> T requireNonNullOrEmptyOrContainsNullOrBlank(
    T collection
  ) throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrEmptyOrContainsNullOrBlank(
      collection,
      ValidationMessage.COLLECTION_IS_NULL.getMessage(),
      ValidationMessage.COLLECTION_IS_EMPTY.getMessage(),
      ValidationMessage.COLLECTION_CONTAINS_NULL.getMessage(),
      ValidationMessage.COLLECTION_CONTAINS_BLANK.getMessage()
    );
  }

  /**
   * Check if a map of a given type is null or empty.
   * @param <T> A subclass of the class map.
   * @param map The map to check.
   * @param nullMessage The message to be given if the map is null
   * @param emptyMessage The message to be given if the map is empty
   * @return Map - the checked map
   * @throws NullPointerException if the map is null
   * @throws IllegalArgumentException if the map is empty
   */
  public static <T extends Map<?, ?>> T requireNonNullOrEmpty(
    T map,
    String nullMessage,
    String emptyMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (Objects.requireNonNull(map, nullMessage).isEmpty()) throw new IllegalArgumentException(
      emptyMessage
    );
    return map;
  }

  /**
   * Check if a map of a given type is null or empty.
   * Uses default messages.
   * @param <T> A subclass of the class map.
   * @param map The map to check.
   * @return Map - the checked map
   * @throws NullPointerException if the map is null
   * @throws IllegalArgumentException if the map is empty
   */
  public static <T extends Map<?, ?>> T requireNonNullOrEmpty(T map)
    throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrEmpty(
      map,
      ValidationMessage.MAP_IS_NULL.getMessage(),
      ValidationMessage.MAP_IS_EMPTY.getMessage()
    );
  }

  /**
   * Check if a map of a given type is null or contains null values.
   * @param <T> A subclass of the class map.
   * @param map The map to check.
   * @param nullMessage The message to be given if the map is null
   * @param containsNullMessage The message to be given if the map contains null
   * @return Map - the checked map
   * @throws NullPointerException if the map is null or contains null
   */
  public static <T extends Map<?, ?>> T requireNonNullOrContainsNull(
    T map,
    String nullMessage,
    String containsNullMessage
  ) throws NullPointerException {
    if (
      Objects.requireNonNull(map, nullMessage).values().stream().anyMatch(Objects::isNull)
    ) throw new NullPointerException(containsNullMessage);
    return map;
  }

  /**
   * Check if a map of a given type is null or contains null values.
   * Uses default messages.
   * @param <T> A subclass of the class map.
   * @param map The map to check.
   * @return Map - the checked map
   * @throws NullPointerException if the map is null or contains null
   */
  public static <T extends Map<?, ?>> T requireNonNullOrContainsNull(T map)
    throws NullPointerException {
    return requireNonNullOrContainsNull(
      map,
      ValidationMessage.MAP_IS_NULL.getMessage(),
      ValidationMessage.MAP_CONTAINS_NULL.getMessage()
    );
  }

  /**
   * Check if a map of a given type is null or empty or contains null values.
   * @param <T> A subclass of the class map.
   * @param map The map to check.
   * @param nullMessage The message to be given if the map is null
   * @param emptyMessage The message to be given if the map is empty
   * @param containsNullMessage The message to be given if the map contains null
   * @return Map - the checked map
   * @throws NullPointerException if the map is null or contains null
   * @throws IllegalArgumentException if the map is empty
   */
  public static <T extends Map<?, ?>> T requireNonNullOrEmptyOrContainsNull(
    T map,
    String nullMessage,
    String emptyMessage,
    String containsNullMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (
      requireNonNullOrEmpty(map, nullMessage, emptyMessage)
        .values()
        .stream()
        .anyMatch(Objects::isNull)
    ) throw new NullPointerException(containsNullMessage);
    return map;
  }

  /**
   * Check if a map of a given type is null or empty or contains null values.
   * Uses default messages.
   * @param <T> A subclass of the class map.
   * @param map The map to check.
   * @return Map - the checked map
   * @throws NullPointerException if the map is null or contains null
   * @throws IllegalArgumentException if the map is empty
   */
  public static <T extends Map<?, ?>> T requireNonNullOrEmptyOrContainsNull(T map)
    throws NullPointerException, IllegalArgumentException {
    return requireNonNullOrEmptyOrContainsNull(
      map,
      ValidationMessage.MAP_IS_NULL.getMessage(),
      ValidationMessage.MAP_IS_EMPTY.getMessage(),
      ValidationMessage.MAP_CONTAINS_NULL.getMessage()
    );
  }

  /**
   * Check if a string is null or blank.
   * @param checkString The string to check.
   * @param nullMessage The message to be given if the string is null
   * @param blankMessage The message to be given if the string is blank
   * @return String - the checked string
   * @throws NullPointerException if the string is null
   * @throws IllegalArgumentException if the string is blank
   */
  public static String requireNonNullOrBlank(
    String checkString,
    String nullMessage,
    String blankMessage
  ) throws NullPointerException, IllegalArgumentException {
    if (
      Objects.requireNonNull(checkString, nullMessage).isBlank()
    ) throw new IllegalArgumentException(blankMessage);
    return checkString;
  }

  /**
   * Check if a string is null or blank.
   * Uses default messages.
   * @param checkString The string to check.
   * @return String - the checked string
   * @throws NullPointerException if the string is null
   * @throws IllegalArgumentException if the string is blank
   */
  public static String requireNonNullOrBlank(String checkString) {
    requireNonNullOrBlank(
      checkString,
      ValidationMessage.STRING_IS_NULL.getMessage(),
      ValidationMessage.STRING_IS_BLANK.getMessage()
    );
    return checkString;
  }

  /**
   * Check if a number is larger than a given number.
   * @param <T> A subclass of the class number.
   * @param number The number to check.
   * @param checkNumber The number to check against.
   * @param message The message to be given if the number is smaller than the given number
   * @return Number - the checked number
   * @throws NullPointerException if the number is null
   * @throws IllegalArgumentException if the number is smaller than the given number
   */
  public static <T extends Number> T requireLargerThan(T number, T checkNumber, String message)
    throws NullPointerException, IllegalArgumentException {
    if (
      Objects.requireNonNull(number, message).doubleValue() <= checkNumber.doubleValue()
    ) throw new IllegalArgumentException(message);
    return number;
  }

  /**
   * Check if a number is larger than a given number.
   * Uses default messages.
   * @param <T> A subclass of the class number.
   * @param number The number to check.
   * @param checkNumber The number to check against.
   * @return Number - the checked number
   * @throws NullPointerException if the number is null
   * @throws IllegalArgumentException if the number is smaller than the given number
   */
  public static <T extends Number> T requireLargerThan(T number, T checkNumber) {
    requireLargerThan(number, checkNumber, ValidationMessage.NUMBER_IS_SMALLER.getMessage());
    return number;
  }

  /**
   * Check if a number is smaller than a given number.
   * @param <T> A subclass of the class number.
   * @param number The number to check.
   * @param checkNumber The number to check against.
   * @param message The message to be given if the number is larger than the given number
   * @return Number - the checked number
   * @throws NullPointerException if the number is null
   * @throws IllegalArgumentException if the number is larger than the given number
   */
  public static <T extends Number> T requireSmallerThan(T number, T checkNumber, String message)
    throws NullPointerException, IllegalArgumentException {
    if (
      Objects.requireNonNull(number, message).doubleValue() >= checkNumber.doubleValue()
    ) throw new IllegalArgumentException(message);
    return number;
  }

  /**
   * Check if a number is smaller than a given number.
   * Uses default messages.
   * @param <T> A subclass of the class number.
   * @param number The number to check.
   * @param checkNumber The number to check against.
   * @return Number - the checked number
   * @throws NullPointerException if the number is null
   * @throws IllegalArgumentException if the number is larger than the given number
   */
  public static <T extends Number> T requireSmallerThan(T number, T checkNumber) {
    requireSmallerThan(number, checkNumber, ValidationMessage.NUMBER_IS_LARGER.getMessage());
    return number;
  }

  /**
   * Check if a number is positive.
   * For the purpose of this method, 0 is positive.
   * @param <T> A subclass of the class number.
   * @param number The number to check.
   * @param message The message to be given if the number is not positive
   * @return Number - the checked number
   * @throws NullPointerException if the number is null
   * @throws IllegalArgumentException if the number is not positive
   */
  public static <T extends Number> T requirePositive(T number, String message)
    throws NullPointerException, IllegalArgumentException {
    if (
      Objects.requireNonNull(number, message).doubleValue() < 0
    ) throw new IllegalArgumentException(message);
    return number;
  }

  /**
   * Check if a number is positive.
   * For the purpose of this method, 0 is positive.
   * Uses default messages.
   * @param <T> A subclass of the class number.
   * @param number The number to check.
   * @return Number - the checked number
   * @throws NullPointerException if the number is null
   * @throws IllegalArgumentException if the number is not positive
   */
  public static <T extends Number> T requirePositive(T number) {
    requirePositive(number, ValidationMessage.NUMBER_IS_NOT_POSITIVE.getMessage());
    return number;
  }
}
