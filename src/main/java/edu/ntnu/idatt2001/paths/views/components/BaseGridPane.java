package edu.ntnu.idatt2001.paths.views.components;

import edu.ntnu.idatt2001.paths.controllers.BaseController;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Text;

/**
 * Abstract class for creating a grid pane with the ability to add a back button and a title.
 * The grid pane is divided into 3 columns and 3 rows.
 * The first column is 20% of the width, the second is 60% and the third is 20%.
 * The first row is 20% of the height, the second is 40% and the third is 40%.
 * @author Callum G.
 * @version 1.0 30.04.2023
 */
public abstract class BaseGridPane extends GridPane {

  /**
   * Creates a new BaseGridPane.
   * Sets the horizontal and vertical gap to 50.
   * Sets the column and row constraints.
   */
  public BaseGridPane() {
    setHgap(50);
    setVgap(50);
    ColumnConstraints column1 = new ColumnConstraints();
    column1.setPercentWidth(20);
    ColumnConstraints column2 = new ColumnConstraints();
    column2.setPercentWidth(60);
    ColumnConstraints column3 = new ColumnConstraints();
    column3.setPercentWidth(20);
    getColumnConstraints().addAll(column1, column2, column3);

    RowConstraints row1 = new RowConstraints();
    row1.setPercentHeight(20);
    RowConstraints row2 = new RowConstraints();
    row2.setPercentHeight(40);
    RowConstraints row3 = new RowConstraints();
    row3.setPercentHeight(40);

    getRowConstraints().addAll(row1, row2, row3);
  }

  /**
   * Adds a back button to the grid pane.
   * @param controller BaseController - the controller to be used for the button
   */
  public void addBackToMainButton(BaseController controller) {
    Button backButton = ButtonComponent.createButton("Back to menu", controller::goBackToMenu);
    add(backButton, 0, 0, 1, 1);
  }

  /**
   * Adds a title to the grid pane.
   * @param title String - the title to be added
   */
  public void setTitle(String title) {
    Text titleText = TitleComponent.createTitle(title);
    setHalignment(titleText, javafx.geometry.HPos.CENTER);
    add(titleText, 1, 0, 1, 1);
  }
}
