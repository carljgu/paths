package edu.ntnu.idatt2001.paths.views.storyeditor;

import edu.ntnu.idatt2001.paths.controllers.storyeditor.StoryEditorController;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import edu.ntnu.idatt2001.paths.views.components.ButtonComponent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 * Pane for the story editor. Contains the diagram pane and a side menu with forms for editing the story.
 * The side menu is scrollable.
 *
 * @author Carl G.
 * @version 1.1 23.05.2023
 */
public class StoryEditorPane extends BorderPane {

  private final StoryEditorController controller;

  /**
   * Creates a pane for the story editor.
   *
   * @param controller StoryEditorController - The controller for the pane.
   */
  public StoryEditorPane(StoryEditorController controller) {
    super();
    this.controller = controller;
    DraggableDiagramPane diagramPane = new DraggableDiagramPane();
    diagramPane.setId("diagramPane");
    diagramPane.setOnMousePressed(controller::pressDiagramPane);
    diagramPane.setOnMouseDragged(controller::dragDiagramPane);
    diagramPane.setOnMouseReleased(event -> {
      controller.updateLinkPositions(diagramPane.getLinkContainer().getChildren());
    });
    setCenter(diagramPane);
    VBox sideMenu = new VBox();
    sideMenu.paddingProperty().set(new Insets(50, 10, 0, 10));
    sideMenu.setBackground(new Background(new BackgroundFill(Color.FLORALWHITE, null, null)));
    sideMenu
      .getChildren()
      .addAll(
        getEditorTitle(),
        getEditorTutorialText(),
        getEditStoryTitleForm(),
        getNewPassageForm(),
        getUpdateLinkForm(),
        ButtonComponent.createButton("Export story", controller::exportStory),
        ButtonComponent.createButton("Play story", controller::playStory),
        ButtonComponent.createButton("Main menu", controller::goBackToMenu)
      );
    ScrollPane scrollPane = new ScrollPane(sideMenu);
    scrollPane.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
    scrollPane.setFitToHeight(true);
    scrollPane.setFitToWidth(true);
    setLeft(scrollPane);
    App
      .getStage()
      .widthProperty()
      .addListener((observable, oldValue, newValue) -> {
        diagramPane.drawBackgroundGrid();
      });
    App
      .getStage()
      .heightProperty()
      .addListener((observable, oldValue, newValue) -> {
        controller.updateCanvasGraphic(diagramPane, getWidth(), getHeight());
      });
  }

  /**
   * Creates a new form for editing the story title
   * The story title updates the story when onfocused
   *
   * @return Pane - The form for editing the story title
   */
  private Pane getEditStoryTitleForm() {
    VBox form = new VBox();

    Label storyTitleLabel = new Label("Story title");
    TextField storyTitleField = new TextField(controller.getStoryTitle());
    storyTitleField.setId("storyTitleField");

    storyTitleField
      .focusedProperty()
      .addListener((observable, oldValue, newValue) -> {
        if (!newValue) controller.updateStoryTitle();
      });

    form.getChildren().addAll(storyTitleLabel, storyTitleField);
    return form;
  }

  /**
   * Creates a new form for creating a passage
   *
   * @return Pane - The form for creating a passage
   */
  private Pane getNewPassageForm() {
    Pane form = new VBox();
    Label passageTitleLabel = new Label("Passage title");
    TextField passageTitleField = new TextField();
    passageTitleField.setId("passageTitleField");
    Label passageDescriptionLabel = new Label("Passage description");
    TextArea passageContentField = new TextArea();
    passageContentField.setId("passageContentField");

    Button newPassageButton = new Button("Create passage");
    newPassageButton.setOnAction(controller::createNewPassage);
    Button updatePassageButton = new Button("Update passage");
    updatePassageButton.setOnAction(controller::updateSelectedPassage);

    form
      .getChildren()
      .addAll(
        passageTitleLabel,
        passageTitleField,
        passageDescriptionLabel,
        passageContentField,
        newPassageButton,
        updatePassageButton
      );
    return form;
  }

  /**
   * Creates a new form for editing a link between two passages
   *
   * @return VBox - the form for changing a link
   */
  private VBox getUpdateLinkForm() {
    VBox form = new VBox();
    form.setSpacing(10);

    Label linkTextLabel = new Label("Link text");
    TextField linkTextField = new TextField();
    linkTextField.setId("linkTextField");

    Button updateLinkButton = new Button("Update link");
    updateLinkButton.setOnAction(controller::updateLink);

    Button deleteLinkButton = new Button("Delete link");
    deleteLinkButton.setOnAction(controller::deleteLink);

    Button addActionButton = new Button("Add action");
    addActionButton.setOnAction(controller::addLinkAction);

    VBox actionFormContainer = new VBox();
    actionFormContainer.setId("linkActionFormContainer");

    form
      .getChildren()
      .addAll(
        linkTextLabel,
        linkTextField,
        actionFormContainer,
        addActionButton,
        updateLinkButton,
        deleteLinkButton
      );
    return form;
  }

  /**
   * Creates a new text for the editor title
   * @return Text - The text for the editor title
   */
  private Text getEditorTitle() {
    Text editorTitle = new Text("Welcome to the story editor!");
    editorTitle.setFont(FontEnum.BODY.getFont());
    return editorTitle;
  }

  /**
   * Creates a new text for the editor tutorial
   * @return Text - The text for the editor tutorial
   */
  private Text getEditorTutorialText() {
    String tutorialString =
      """
        To create a new passage, fill out the form below and click `Create passage`.
        To edit a passage, double click on it to select it. It will then have a darker shade.
        In the form below you can write the desired passage information and click `Update passage`.
        To delete a passage, select it and click `Delete passage`.
        To create a link between two passages, select the source passage and click `Link from selected` on the target.
        You can update a link by selecting it. It will then turn red. In the form below, you can change the link text and add actions.
        To remove actions from a link, keep the action form empty.
        To delete a link, select it and click `Delete link`.
        To set a passage as the opening passage, select it and click `Set as opening passage`. It will then have a red border.
        The passage nodes are resizable and movable.
        Double click anywhere else to deselect selections.
        Click the export button to export the story to a selected .paths file.
        """;
    Text tutorialText = new Text(tutorialString);
    return tutorialText;
  }

  /**
   * Creates a new button for creating a new passage
   * @param passageNode - The passage node to add to the diagram
   * @return Button - The button for creating a new passage
   */
  public boolean addPassageNodeToDiagram(DragResizePassageNode passageNode) {
    DraggableDiagramPane diagramPane = (DraggableDiagramPane) getCenter();
    passageNode.getLinkingButton().setOnAction(controller::linkPassageNodes);
    passageNode.getOpeningPassageButton().setOnAction(controller::setAsOpeningPassage);
    passageNode.getDeleteButton().setOnAction(controller::deletePassageNode);
    passageNode.setLayoutX(diagramPane.getWidth() / 2);
    passageNode.setLayoutY(diagramPane.getHeight() / 2);
    return diagramPane.getStoryBlockNodesContainer().getChildren().add(passageNode);
  }
}
