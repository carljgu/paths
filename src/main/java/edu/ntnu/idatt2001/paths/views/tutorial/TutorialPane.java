package edu.ntnu.idatt2001.paths.views.tutorial;

import edu.ntnu.idatt2001.paths.controllers.BaseController;
import edu.ntnu.idatt2001.paths.controllers.TutorialPageController;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.file.TutorialFileReader;
import edu.ntnu.idatt2001.paths.models.tutorial.TutorialParagraph;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import edu.ntnu.idatt2001.paths.views.components.BaseGridPane;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * A pane to display the tutorial.
 * Reads the tutorial from a delimited text file.
 * Converts the text file into tutorial paragraphs.
 * The file should be formatted as follows: Title;Text;Image path
 *
 * @author Carl G. Callum G.
 * @version 0.2 30.04.2023
 */
public class TutorialPane extends BaseGridPane {

  private final BaseController controller;
  private static final String TUTORIAL_FILE = "/tutorialText.csv";

  /**
   * Creates a new TutorialPane.
   */
  public TutorialPane() {
    super();
    controller = new TutorialPageController();
    addBackToMainButton(controller);
    setAlignment(Pos.CENTER);
    setPadding(new Insets(50));

    Text tutorialTitle = new Text("Game tutorial");
    tutorialTitle.setFont(FontEnum.LARGE_TITLE.getFont());
    tutorialTitle.setTextAlignment(TextAlignment.CENTER);

    VBox tutorialContentBox = new VBox();
    tutorialContentBox.setAlignment(Pos.CENTER);
    tutorialContentBox.setPadding(new Insets(50));

    tutorialContentBox.getChildren().add(tutorialTitle);
    Collection<TutorialParagraphPane> paragraphs = getParagraphs();
    paragraphs
      .stream()
      .forEach(paragraph -> {
        paragraph.setAlignment(Pos.CENTER);
        paragraph.setSpacing(30);
      });
    tutorialContentBox.getChildren().addAll(paragraphs);
    tutorialContentBox.setSpacing(100);

    ScrollPane scrollPane = new ScrollPane(tutorialContentBox);
    scrollPane.setFitToWidth(true);
    scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
    scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

    VBox rootBox = new VBox();
    rootBox.getChildren().addAll(scrollPane);
    rootBox.setAlignment(Pos.CENTER);

    add(rootBox, 1, 0, 2, 3);
  }

  /**
   * Retrieves the tutorial from the tutorial file if it exists.
   * If there are not tutorials to list or error reading the file,
   * a message about no tutorial content is shown instead.
   * @return A list of tutorial paragraph panes.
   */
  private List<TutorialParagraphPane> getParagraphs() {
    boolean giveEmptyTutorialMessage = false;
    List<TutorialParagraphPane> paragraphs = new ArrayList<>();
    TutorialParagraphPane emptyTutorialMessage = new TutorialParagraphPane(
      new TutorialParagraph("No content", "There is no tutorial to show.")
    );
    try {
      TutorialFileReader reader = TutorialFileReader.getInstance();
      paragraphs.addAll(
        reader
          .readParagraphsFromFile(TUTORIAL_FILE)
          .stream()
          .map(p -> new TutorialParagraphPane(p))
          .toList()
      );
    } catch (IOException | WrongFileExtensionException e) {
      giveEmptyTutorialMessage = true;
    }
    if (paragraphs.isEmpty() || giveEmptyTutorialMessage) {
      paragraphs.add(emptyTutorialMessage);
    }
    return paragraphs;
  }
}
