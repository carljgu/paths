package edu.ntnu.idatt2001.paths.views.storyplayer;

import edu.ntnu.idatt2001.paths.controllers.playgame.PlayGameController;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.views.App;
import edu.ntnu.idatt2001.paths.views.ColorEnum;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * A pane displaying a passage
 * Contains the passage title, content and links.
 *
 * @see Passage
 * @author Carl G.
 * @version 1.1 22.05.2023
 */
public class PlayPassagePane extends VBox {

  private final double PADDING_RATIO = 0.7;

  /**
   * Creates a new play passage pane.
   * Uses VBox to display the passage title, content and links under each other
   * @param passage Passage - the passage to display information about
   * @param showLinks BooleanProperty - whether or not to show the links
   * @param controller PlayGameController - the controller for the game
   */
  public PlayPassagePane(
    Passage passage,
    BooleanProperty showLinks,
    PlayGameController controller
  ) {
    Text passageTitle = createPassageTitle(passage.getTitle());
    Text passageContent = createPassageContent(passage.getContent());
    VBox passageLinks = createPassageLinks(controller, passage.getLinks());

    passageLinks.visibleProperty().bind(showLinks);

    VBox passageInfoBox = new VBox(passageTitle, passageContent);
    passageInfoBox.getChildren().addAll(passageLinks);
    passageInfoBox.setAlignment(Pos.CENTER);
    passageInfoBox.setSpacing(100);
    passageInfoBox.setPadding(new Insets(30));
    passageInfoBox.setMaxWidth(App.getStage().getWidth() * PADDING_RATIO);
    passageContent.setWrappingWidth(passageInfoBox.getMaxWidth() * PADDING_RATIO);

    setBackground(
      new Background(
        new BackgroundFill(ColorEnum.LIGHT_GREEN.getColor(), new CornerRadii(10), new Insets(10))
      )
    );
    getChildren().add(passageInfoBox);
    setAlignment(Pos.CENTER);
    setMaxWidth(App.getStage().getScene().getWidth() * PADDING_RATIO);
    setMaxHeight(App.getStage().getScene().getHeight() * PADDING_RATIO);
  }

  /**
   * Creates a text object with the passage title
   * @param titleText String - the title of the passage
   * @return Text - text with the passage title
   */
  private Text createPassageTitle(String titleText) {
    Text title = new Text(titleText);
    title.setFont(FontEnum.TITLE.getFont());
    title.setTextAlignment(TextAlignment.CENTER);
    return title;
  }

  /**
   * Creates a text object with the passage content
   * @param content String - the content of the passage
   * @return Text - text with the passage content
   */
  private Text createPassageContent(String content) {
    Text text = new Text(content);
    text.setFont(FontEnum.BODY.getFont());
    text.setTextAlignment(TextAlignment.CENTER);
    return text;
  }

  /**
   * Creates a list of buttons with the passage links
   * Uses the controller to give an action to the buttons
   * @param controller PlayGameController - the controller to use
   * @param links List - the links to display
   * @return List - list of buttons with the passage links
   */
  private VBox createPassageLinks(PlayGameController controller, List<Link> links) {
    VBox passageLinkBox = new VBox();
    passageLinkBox.setAlignment(Pos.CENTER);
    passageLinkBox
      .getChildren()
      .addAll(
        links
          .stream()
          .map(l -> {
            Button linkButton = new Button(l.getText());
            linkButton.setOnAction(e -> {
              ClickLinkEvent linkEvent = new ClickLinkEvent(l);
              controller.goThroughLink(linkEvent);
            });
            return linkButton;
          })
          .toList()
      );
    return passageLinkBox;
  }
}
