package edu.ntnu.idatt2001.paths.views.startmenu;

import edu.ntnu.idatt2001.paths.controllers.startmenu.StartMenuController;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import java.util.ArrayList;
import java.util.Collection;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 * The start menu scene.
 * The start menu scene is the first scene that is shown when the program is
 * started. It contains buttons for the user to choose between creating a new
 * story, importing a story from a file, and exiting the program.
 *
 * @author Carl G.
 * @version 1.0 22.04.2023
 */

public class StartMenuPane extends StackPane {

  private final StartMenuController controller;
  private final Font buttonFont;

  /**
   * Creates a new start menu scene.f
   * Constructor of the start menu schene. Creates a title and a collection of
   * buttons for the user to choose between.
   * @param controller - The controller for the start menu.
   */
  public StartMenuPane(StartMenuController controller) {
    super();
    this.controller = controller;
    this.buttonFont = FontEnum.BUTTON.getFont();

    VBox titleAndButtons = new VBox(20);
    titleAndButtons.setAlignment(Pos.CENTER);
    titleAndButtons.setSpacing(50);
    titleAndButtons.setPadding(new javafx.geometry.Insets(50));

    titleAndButtons.getChildren().add(getTitleLabel());

    titleAndButtons.getChildren().addAll(getStartMenuButtons());

    getChildren().addAll(titleAndButtons);
  }

  /**
   * Creates a title label with the text `Paths`.
   * @return Label - The title label.
   */
  private Label getTitleLabel() {
    Label titleLabel = new Label("Paths");
    titleLabel.setFont(FontEnum.LARGE_TITLE.getFont());
    titleLabel.setPadding(new javafx.geometry.Insets(50));
    return titleLabel;
  }

  /**
   * Creates a button with the given text and action.
   * @param text - The text to be displayed on the button.
   * @param action - The action to be performed when the button is clicked.
   * @return Button - The created button.
   */
  private Button createButton(String text, EventHandler<ActionEvent> action) {
    Button button = new Button(text);
    button.setWrapText(true);
    button.setAlignment(Pos.CENTER);
    button.setFont(buttonFont);
    button.setOnAction(action);
    return button;
  }

  /**
   * Creates a collection of buttons for the start menu.
   * @return Collection - The collection of buttons.
   */
  private Collection<Button> getStartMenuButtons() {
    Collection<Button> buttons = new ArrayList<>();
    buttons.add(createButton("Import story and play", controller::showStoryImportPage));
    buttons.add(createButton("Create story", controller::showStoryEditor));
    buttons.add(createButton("Tutorial", controller::showTutorial));
    Button exitButton = createButton("Exit", controller::exitStage);
    exitButton.getStyleClass().addAll("dangerous", "button");
    buttons.add(exitButton);

    return buttons;
  }
}
