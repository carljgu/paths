package edu.ntnu.idatt2001.paths.views.storystarter;

import edu.ntnu.idatt2001.paths.controllers.storystarter.StoryStarterController;
import edu.ntnu.idatt2001.paths.models.game.GameDifficultyEnum;
import edu.ntnu.idatt2001.paths.models.player.PlayerStatEnum;
import edu.ntnu.idatt2001.paths.views.ChoiceBoxOption;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import edu.ntnu.idatt2001.paths.views.InformationTooltip;
import edu.ntnu.idatt2001.paths.views.components.BaseGridPane;
import edu.ntnu.idatt2001.paths.views.components.ButtonComponent;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectBinding;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * A pane for starting a story.
 * The pane contains a title, a pane for selecting goals for the game, a pane for selecting the
 * difficulty of the game, and a button for starting the story.
 * @author Carl G., Callum G.
 * @version 1.1 22.05.2023
 */
public class StoryStarterPane extends BaseGridPane {

  private final StoryStarterController controller;

  /**
   * Creates a pane for starting a story.
   * @param controller StoryStarterController - The controller for the pane.
   */
  public StoryStarterPane(StoryStarterController controller) {
    this.controller = controller;
    setTitle(controller.getStory().getTitle());
    addBackToMainButton(controller);
    setAlignment(Pos.CENTER);
    setPadding(new Insets(50));

    VBox contentBox = new VBox();
    contentBox.setAlignment(Pos.CENTER);

    VBox difficultyPlayerNameBox = new VBox();
    difficultyPlayerNameBox.setSpacing(50);
    difficultyPlayerNameBox.getChildren().addAll(createDifficultyBox(), createPlayerNameBox());

    HBox gameOptions = new HBox();
    gameOptions.setSpacing(50);
    Button newGoalButton = ButtonComponent.createButton("Add new goal", controller::addGoalField);
    gameOptions.getChildren().addAll(createAddGoalsBox(newGoalButton), difficultyPlayerNameBox);
    gameOptions.setAlignment(Pos.CENTER);

    Button startStoryButton = ButtonComponent.createButton("Start story", controller::startStory);

    contentBox.getChildren().addAll(gameOptions, startStoryButton);
    add(contentBox, 0, 1, 3, 3);
    Platform.runLater(() -> controller.addGoalField(new ActionEvent(newGoalButton, newGoalButton)));
  }

  /**
   * Creates a pane for selecting goals for the game.
   * The pane contains a scrollable pane with a list of input fields for goals.
   * The pane also contains a button for adding new input fields.
   * @return Pane - A pane for selecting goals for the game.
   */
  private Pane createAddGoalsBox(Button newGoalButton) {
    VBox goalsBox = new VBox();
    goalsBox.setAlignment(Pos.CENTER);

    Text goalsTitle = new Text("Add goals");
    goalsTitle.setFont(FontEnum.BODY.getFont());

    VBox scrollingBox = new VBox();

    VBox goalsList = controller.getGoalsList();

    if (goalsList == null) {
      goalsList = new VBox();
    }

    goalsList.setSpacing(10);

    scrollingBox.getChildren().addAll(goalsList, newGoalButton);

    ScrollPane scrollPane = new ScrollPane(scrollingBox);
    scrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
    scrollPane.setMinHeight(200);
    scrollPane.setMaxHeight(1000);

    String tooltipText =
      "Choose a goal from the choice box and enter a value for the goal.\nInventory goals require you to write a comma separated list of values";
    goalsBox.getChildren().addAll(goalsTitle, new InformationTooltip(tooltipText), scrollPane);

    return goalsBox;
  }

  /**
   * Creates a pane with a choice box for selecting difficulty.
   * Also contains information about the difficulty.
   * @return Pane - A pane with a choice box for selecting difficulty.
   */
  private Pane createDifficultyBox() {
    VBox difficultyChooserPane = new VBox();
    difficultyChooserPane.setAlignment(Pos.CENTER);
    Text difficultyTitle = new Text("Choose difficulty");
    difficultyTitle.setFont(FontEnum.BODY.getFont());

    HBox difficultyChooserHbox = new HBox();
    difficultyChooserHbox.setAlignment(Pos.CENTER);
    difficultyChooserHbox.setSpacing(10);

    ChoiceBox<ChoiceBoxOption<GameDifficultyEnum>> difficultyChooser = new ChoiceBox<>();
    difficultyChooser
      .getItems()
      .addAll(
        List.of(
          new ChoiceBoxOption<>("Default", GameDifficultyEnum.DEFAULT),
          new ChoiceBoxOption<>("Easy", GameDifficultyEnum.EASY),
          new ChoiceBoxOption<>("Medium", GameDifficultyEnum.MEDIUM),
          new ChoiceBoxOption<>("Hard", GameDifficultyEnum.HARD)
        )
      );
    difficultyChooser.getSelectionModel().selectFirst();

    StringBuilder tooltipText = new StringBuilder()
      .append("Starting attributes:\n")
      .append("Default: ")
      .append(PlayerStatEnum.DEFAULT_GOLD.getValue())
      .append(" gold, ")
      .append(PlayerStatEnum.DEFAULT_HEALTH.getValue())
      .append(" health, and ")
      .append(PlayerStatEnum.DEFAULT_SCORE.getValue())
      .append(" score.\n")
      .append("Easy: ")
      .append(PlayerStatEnum.EASY_GOLD.getValue())
      .append(" gold, ")
      .append(PlayerStatEnum.EASY_HEALTH.getValue())
      .append(" health, and ")
      .append(PlayerStatEnum.EASY_SCORE.getValue())
      .append(" score.\n")
      .append("Medium: ")
      .append(PlayerStatEnum.MEDIUM_GOLD.getValue())
      .append(" gold, ")
      .append(PlayerStatEnum.MEDIUM_HEALTH.getValue())
      .append(" health, and ")
      .append(PlayerStatEnum.MEDIUM_SCORE.getValue())
      .append(" score.\n")
      .append("Hard: ")
      .append(PlayerStatEnum.HARD_GOLD.getValue())
      .append(" gold, ")
      .append(PlayerStatEnum.HARD_HEALTH.getValue())
      .append(" health, and ")
      .append(PlayerStatEnum.HARD_SCORE.getValue())
      .append(" score.\n");

    difficultyChooserHbox
      .getChildren()
      .addAll(difficultyChooser, new InformationTooltip(tooltipText.toString()));
    difficultyChooserPane.getChildren().addAll(difficultyTitle, difficultyChooserHbox);
    difficultyChooser.setId("difficultyChoiceBox");
    ObjectBinding<GameDifficultyEnum> difficultyBinding = Bindings.createObjectBinding(
      () -> difficultyChooser.valueProperty().getValue().getValue(),
      difficultyChooser.valueProperty()
    );
    controller.getDifficulty().bind(difficultyBinding);

    return difficultyChooserPane;
  }

  private Pane createPlayerNameBox() {
    VBox playerNameBox = new VBox();
    playerNameBox.setAlignment(Pos.CENTER);
    playerNameBox.setSpacing(10);

    Label playerNameTitle = new Label("Enter player name");
    playerNameTitle.setFont(FontEnum.BODY.getFont());

    TextField playerNameField = new TextField();
    playerNameField.setId("playerNameField");
    controller.getPlayerName().bind(playerNameField.textProperty());

    playerNameBox.getChildren().addAll(playerNameTitle, playerNameField);

    return playerNameBox;
  }
}
