package edu.ntnu.idatt2001.paths.views.tutorial;

import edu.ntnu.idatt2001.paths.models.tutorial.TutorialParagraph;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import java.util.Objects;
import javafx.beans.binding.Bindings;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * A block of text and image to be displayed in the tutorial.
 * @author Carl G.
 * @version 1.0 23.04.2023
 */
public class TutorialParagraphPane extends VBox {

  private final int MIN_WIDTH = 300;

  /**
   * Creates a new TutorialParagraph with a title, text and image.
   * If the image path is null, no image will be displayed.
   * @param tutorialParagraph TutorialParagraph - A paragraph with a title,
   *  text and a possible image path which can be null
   */
  public TutorialParagraphPane(TutorialParagraph tutorialParagraph)
    throws IllegalArgumentException, NullPointerException {
    setMinWidth(MIN_WIDTH);

    Text titleText = new Text(tutorialParagraph.getTitle());
    titleText.setFont(FontEnum.TITLE.getFont());
    titleText.setWrappingWidth(MIN_WIDTH);
    titleText.setTextAlignment(TextAlignment.CENTER);
    titleText.wrappingWidthProperty().bind(Bindings.multiply(this.widthProperty(), 0.7));

    Text contentText = new Text(tutorialParagraph.getContent());
    contentText.setFont(FontEnum.BODY.getFont());
    contentText.wrappingWidthProperty().bind(Bindings.multiply(this.widthProperty(), 0.7));
    contentText.setTextAlignment(TextAlignment.CENTER);

    String imagePath = tutorialParagraph.getImagePath();
    getChildren().addAll(titleText, contentText);
    if (Objects.nonNull(imagePath)) {
      Image image = new Image(imagePath);
      ImageView imageView = new ImageView(image);
      imageView.fitWidthProperty().bind(Bindings.divide(this.widthProperty(), 2));
      imageView.setPreserveRatio(true);
      getChildren().add(imageView);
    }
  }
}
