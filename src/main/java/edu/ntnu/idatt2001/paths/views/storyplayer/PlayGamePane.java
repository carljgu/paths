package edu.ntnu.idatt2001.paths.views.storyplayer;

import edu.ntnu.idatt2001.paths.controllers.playgame.PlayGameController;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * A pane displayed when playing a story.
 * Displays the current passage, player stats and game goals.
 *
 * @author Carl G. Callum G.
 * @version 1.2 23.05.2023
 */
public class PlayGamePane extends BorderPane {

  private final PlayGameController controller;

  /**
   * Creates a new play story pane.
   * @param controller PlayGameController - The controller for the pane.
   */
  public PlayGamePane(PlayGameController controller) {
    this.controller = controller;

    setCenter(controller.getCurrentPassagePane().get());
    controller
      .getCurrentPassagePane()
      .addListener((observable, oldValue, newValue) -> {
        setCenter(newValue);
      });

    setTop(createTitlePane());

    final VBox playerStatsPane = createPlayerStatsDisplay();
    setLeft(playerStatsPane);
    final VBox gameGoalsPane = createGameGoalsDisplay();
    setRight(gameGoalsPane);
    final HBox bottomButtonsDisplay = createBottomButtonsDisplay();
    setBottom(bottomButtonsDisplay);
  }

  /**
   * Creates a vbox with a title and a subtitle
   * @return VBox - The pane with title and subtitle.
   */
  private VBox createTitlePane() {
    Text gameTitleText = new Text();
    gameTitleText.setFont(FontEnum.LARGE_TITLE.getFont());
    gameTitleText.textProperty().bind(controller.getGameTitle());
    gameTitleText.setTextAlignment(TextAlignment.CENTER);
    Text gameSubtitleText = new Text();
    gameSubtitleText.setFont(FontEnum.TITLE.getFont());
    gameSubtitleText.textProperty().bind(controller.getGameSubtitle());
    gameSubtitleText.setTextAlignment(TextAlignment.CENTER);
    VBox titlePane = new VBox();
    titlePane.setAlignment(Pos.CENTER);
    titlePane.getChildren().addAll(gameTitleText, gameSubtitleText);
    return titlePane;
  }

  /**
   * Creates the bottom buttons display.
   * Contains a button for restarting the game and a button for going back to the main menu.
   * @return HBox - The bottom buttons display.
   */
  public HBox createBottomButtonsDisplay() {
    HBox bottomButtonsDisplay = new HBox();
    bottomButtonsDisplay.setSpacing(10);
    Button restartGameButton = new Button("Restart game");
    restartGameButton.getStyleClass().addAll("dangerous", "button", "button-large");
    restartGameButton.setOnAction(controller::restartGame);

    Button toMainMenuButton = new Button("To main menu");
    toMainMenuButton.getStyleClass().addAll("dangerous", "button", "button-large");
    toMainMenuButton.setOnAction(controller::goToMainMenu);

    bottomButtonsDisplay.getChildren().addAll(restartGameButton, toMainMenuButton);
    bottomButtonsDisplay.setAlignment(Pos.TOP_CENTER);
    bottomButtonsDisplay.setPadding(new Insets(30));

    return bottomButtonsDisplay;
  }

  /**
   * Creates the player stats display.
   * @see PlayerInfoPane
   * @return VBox - The player stats display.
   */
  public VBox createPlayerStatsDisplay() {
    return new PlayerInfoPane(controller);
  }

  /**
   * Creates the game goals display.
   * @see GameGoalsPane
   * @return VBox - The game goals display.
   */
  public VBox createGameGoalsDisplay() {
    return new GameGoalsPane(controller);
  }
}
