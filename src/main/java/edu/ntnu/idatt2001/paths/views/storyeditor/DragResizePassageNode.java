package edu.ntnu.idatt2001.paths.views.storyeditor;

import edu.ntnu.idatt2001.paths.controllers.DragResizeMod;
import edu.ntnu.idatt2001.paths.controllers.storyeditor.PassageNodeDragResizeListener;
import edu.ntnu.idatt2001.paths.models.Passage;
import java.util.Objects;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * A passage node that can be dragged and resized.
 * It has a title and a button bar with buttons for linking, opening passage and deleting the passage.
 * The border of the passage node is red if it is an opening passage
 *
 * @author Carl G.
 * @version 0.1 30.04.2023
 */
public class DragResizePassageNode extends StackPane {

  private final Rectangle box;
  private final Button linkingButton;
  private final Button openingPassageButton;
  private final Button deleteButton;

  private BooleanProperty isOpeningPassage;

  private SimpleObjectProperty<Passage> passageProperty;

  public static final Paint DEFAULT_FILL = LinearGradient.valueOf(
    "linear-gradient(to bottom, #ffffff, #e6e6e6)"
  );
  public static final Paint SELECTED_FILL = Color.GRAY;

  private final double STARTING_WIDTH = 250;
  private final double STARTING_HEIGHT = 250;

  public static final double CENTER_CORNER_OFFSET = 0.3;

  /**
   * Creates a passage node with a passage
   * @param passage Passage - The passage to be displayed on the passage node
   * @throws NullPointerException if passage is null
   */
  public DragResizePassageNode(Passage passage) throws NullPointerException {
    Objects.requireNonNull(passage, "Passage cannot be null");
    passageProperty = new SimpleObjectProperty<>(passage);
    isOpeningPassage = new SimpleBooleanProperty(false);

    DragResizeMod.makeResizable(this, new PassageNodeDragResizeListener());
    this.setMaxSize(STARTING_WIDTH, STARTING_HEIGHT);
    setPrefSize(STARTING_WIDTH, STARTING_HEIGHT);
    this.box = initBackgroundBox();
    Text title = initTitle();
    this.linkingButton = initLinkingButton();
    this.openingPassageButton = initOpeningPassageButton();
    this.deleteButton = initDeleteButton();

    VBox buttonBox = new VBox();
    buttonBox
      .getChildren()
      .addAll(this.linkingButton, this.openingPassageButton, this.deleteButton);
    buttonBox.setMaxSize(buttonBox.getPrefWidth(), BASELINE_OFFSET_SAME_AS_HEIGHT);
    buttonBox.setAlignment(Pos.BOTTOM_CENTER);
    buttonBox.setSpacing(5);
    setAlignment(buttonBox, Pos.BOTTOM_CENTER);

    getChildren().addAll(this.box, title, buttonBox);
  }

  /**
   * Creates a background box for the passage node
   * @return Rectangle - the background box of the passage node
   */
  private Rectangle initBackgroundBox() {
    Rectangle rect = new Rectangle(STARTING_WIDTH, STARTING_HEIGHT);

    rect.setStrokeType(StrokeType.INSIDE);
    rect.setStrokeWidth(2);

    rect
      .strokeProperty()
      .bind(Bindings.when(isOpeningPassage).then(Color.RED).otherwise(Color.BLACK));

    rect.setFill(DEFAULT_FILL);
    rect.widthProperty().bind(widthProperty());
    rect.heightProperty().bind(heightProperty());
    return rect;
  }

  /**
   * Creates a title for the passage node which is the title of the passage
   * @return Text - the title of the passage node
   */
  private Text initTitle() {
    Text title = new Text();
    title
      .textProperty()
      .bind(Bindings.createStringBinding(() -> passageProperty.get().getTitle(), passageProperty));
    title.setFont(Font.font("Arial", FontWeight.BOLD, 30));
    title.wrappingWidthProperty().bind(this.widthProperty());
    title.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
    setAlignment(title, javafx.geometry.Pos.TOP_CENTER);
    return title;
  }

  /**
   * Creates a button for linking passages
   * @return Button - the linking button
   */
  private Button initLinkingButton() {
    Button button = new Button("Link from selected");
    setAlignment(button, Pos.BOTTOM_CENTER);
    return button;
  }

  /**
   * Creates a button for setting the passage as opening passage
   * @return Button - the opening passage button
   */
  private Button initOpeningPassageButton() {
    Button button = new Button("Set as opening passage");
    setAlignment(button, Pos.BOTTOM_LEFT);
    return button;
  }

  /**
   * Creates a button for deleting the passage
   * @return Button - the delete button
   */
  private Button initDeleteButton() {
    Button button = new Button("Delete");
    button.setTextFill(Color.WHITE);
    button.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
    setAlignment(button, Pos.BOTTOM_RIGHT);
    return button;
  }

  /**
   * Returns the box of the passage node
   * @return Rectangle - the box of the passage node
   */
  public Rectangle getBox() {
    return this.box;
  }

  /**
   * Returns the passage of the passage node
   * @return Passage - the passage of the passage node
   */
  public Passage getPassage() {
    return this.passageProperty.get();
  }

  /**
   * Sets the passage of the passage node
   * @param passage Passage - the passage to be set
   */
  public void setPassage(Passage passage) {
    this.passageProperty.set(passage);
  }

  /**
   * Returns the button for linking passages
   * @return Button - the linking button
   */
  public Button getLinkingButton() {
    return linkingButton;
  }

  /**
   * Returns the button for setting the passage as opening passage
   * @return Button - the opening passage button
   */
  public Button getOpeningPassageButton() {
    return openingPassageButton;
  }

  /**
   * Returns the button for deleting the passage
   * @return Button - the delete button
   */
  public Button getDeleteButton() {
    return deleteButton;
  }

  /**
   * sets the node to be the opening passage
   * @param isOpeningPassage isOpeningPassage - the passage to be set
   */
  public void setIsOpeningPassage(boolean isOpeningPassage) {
    this.isOpeningPassage.set(isOpeningPassage);
  }

  /**
   * Returns the x coordinate of a point between the center
   * of the node and the corner of the node
   *
   * @param betweenPercent double - the percentage of the distance between the center and the corner.
   * A number between -1 and 1.
   * @return double - the x coordinate of the point
   */
  public double getBetweenCenterAndCornerX(double betweenPercent) {
    return getLayoutX() + (1 + betweenPercent) * getWidth() / 2;
  }

  /**
   * Returns the y coordinate of a point between the center
   * of the node and the corner of the node
   *
   * @param betweenPercent double - the percentage of the distance between the center and the corner
   * A number between -1 and 1.
   * @return double - the y coordinate of the point
   */
  public double getBetweenCenterAndCornerY(double betweenPercent) {
    return getLayoutY() + (1 + betweenPercent) * getHeight() / 2;
  }

  /**
   * selects the node by chaning its fill to the selection fill
   */
  public void select() {
    this.box.setFill(SELECTED_FILL);
  }

  /**
   * deselects the node by chaning its fill to the default fill
   */
  public void deselect() {
    this.box.setFill(DEFAULT_FILL);
  }

  /**
   * Equals method for passage nodes
   * Two drag resize passage nodes are equal if they have the same passage
   * @param o Object - the object to be compared
   * @return boolean - true if the objects are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    if (o instanceof DragResizePassageNode) {
      DragResizePassageNode other = (DragResizePassageNode) o;
      return this.passageProperty.get().equals(other.passageProperty.get());
    }
    return false;
  }

  /**
   * Hashcode method for passage nodes
   * @return int - the hashcode of the passage node
   */
  @Override
  public int hashCode() {
    return this.passageProperty.get().hashCode();
  }
}
