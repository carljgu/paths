package edu.ntnu.idatt2001.paths.views;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;

/**
 * A class that represents an option in a {@link ChoiceBox}.
 * Also possible to use with {@link ComboBox}.
 * @param <T> The type of the value that the choice box should contain
 * @author Carl G.
 * @version 1.0 23.04.2023
 */
public class ChoiceBoxOption<T> {

  private final String display;
  private final T value;

  /**
   * Creates a new ChoiceBoxOption with the given display string and value.
   * @param display String - The display of the option
   * @param value T - The value of the option
   */
  public ChoiceBoxOption(String display, T value) {
    this.display = display;
    this.value = value;
  }

  /**
   * Returns the display string of the option.
   * Shown in the choice box gui.
   * @return String - The display string
   */
  public String getDisplay() {
    return display;
  }

  /**
   * Returns the value of the option.
   * @return T - The value
   */
  public T getValue() {
    return value;
  }

  /**
   * Returns the display string of the option.
   * @return String - The display string
   */
  @Override
  public String toString() {
    return display;
  }
}
