package edu.ntnu.idatt2001.paths.views;

import edu.ntnu.idatt2001.paths.controllers.startmenu.StartMenuControllerImpl;
import edu.ntnu.idatt2001.paths.views.startmenu.StartMenuPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * The class App which starts the application window.
 *
 * @author Carl G.
 * @version 0.2 - 22.04.2021
 */
public class App extends Application {

  private static final int DEFAULT_WIDTH = 1280;
  private static final int DEFAULT_HEIGHT = 720;

  private static Stage stage;

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    launch();
  }

  /**
   * Show the first stage of the application
   *
   * @param startStage stage to add the first scene to
   * @throws Exception thrown if application window could not be opened.
   */
  @Override
  public void start(Stage startStage) throws Exception {
    stage = startStage;
    stage.setTitle("Paths");

    Image icon = new Image("logo.png");
    stage.getIcons().add(icon);

    StartMenuPane startMenuPane = new StartMenuPane(new StartMenuControllerImpl());
    Scene scene = new Scene(startMenuPane, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    scene.getStylesheets().add("stylesheet.css");
    stage.setScene(scene);

    stage.show();
  }

  /**
   * Get the stage of the application
   * @return Stage - the application stage
   */
  public static Stage getStage() {
    return stage;
  }

  /**
   * Set the scene of the application
   * @param pane Pane - the pane to set as the scene
   */
  public static void changeRootPane(Pane pane) {
    stage.getScene().setRoot(pane);
  }
}
