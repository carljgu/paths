package edu.ntnu.idatt2001.paths.views;

import javafx.scene.Group;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;

/**
 * Tooltip that displays information when hovered over.
 * Shows a circle with a question mark in the center.
 * When hovered over, a tooltip with the specified text is shown.
 *
 * @author Carl G.
 * @version 1.0 23.04.2023
 */
public class InformationTooltip extends Group {

  private final int RADIUS = 15;

  /**
   * Creates a new InformationTooltip with the specified text.
   * @param tooltipText String - The text to display in the tooltip.
   */
  public InformationTooltip(String tooltipText) {
    // Create the circle with the specified radius
    Circle circle = new Circle(RADIUS);
    // Set the circle fill color
    circle.setFill(Color.LIGHTBLUE);

    // Create the question mark text
    Text questionMark = new Text("?");
    // Set the text font size
    questionMark.setFont(Font.font(RADIUS + 5));
    questionMark.setFill(Color.WHITE);
    questionMark.setX(circle.getCenterX() - questionMark.getBoundsInLocal().getWidth() / 2);
    questionMark.setY(
      circle.getCenterY() + questionMark.getBoundsInLocal().getHeight() / 2 - RADIUS / 2
    );

    // Center the text within the circle
    questionMark.setTextAlignment(TextAlignment.CENTER);

    // Create a tooltip with the text
    Tooltip tooltip = new Tooltip(tooltipText);
    tooltip.setFont(Font.font(RADIUS));
    tooltip.setWrapText(true);
    tooltip.setMaxWidth(400);
    tooltip.setShowDelay(Duration.millis(0));

    Tooltip.install(circle, tooltip);
    Tooltip.install(questionMark, tooltip);

    // Add the circle and question mark text to the group
    getChildren().addAll(circle, questionMark);
  }
}
