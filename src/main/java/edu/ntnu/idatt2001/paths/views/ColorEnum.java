package edu.ntnu.idatt2001.paths.views;

import javafx.scene.paint.Color;

/**
 * An enum with different standard colors to use throughout the application.
 * @author Carl G.
 * @version 1.0 - 29.04.2023
 */
public enum ColorEnum {
  /**
   * Color for lighter green.
   */
  LIGHT_GREEN("#B7E4C7"),

  /**
   * Color for a lighter blue.
   */
  LIGHT_BLUE("#D5E1F3"),

  /**
   * Color for blue.
   * A bit darker than the light blue
   */
  BLUE("#748FD6"),

  /**
   * A light red for displaying
   * something that should be noticed.
   */
  WARNING_RED("#DD0000");

  private final Color color;

  /**
   * Creates a new color
   * @param hex String - The hex color code
   */
  ColorEnum(String hex) {
    this.color = Color.web(hex);
  }

  /**
   * Returns the color created from the hex code.
   * @return Color - The color.
   */
  public Color getColor() {
    return color;
  }
}
