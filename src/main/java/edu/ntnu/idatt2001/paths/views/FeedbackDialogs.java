package edu.ntnu.idatt2001.paths.views;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

/**
 * The feedback dialogs.
 * A class with static methods to show dialog
 * boxes with different types of feedback to the user.
 *
 * @author Carl G.
 * @version 1.0 22.04.2023
 */
public class FeedbackDialogs {

  /**
   * Creates a new alert dialog with an alert type, title and content.
   * @param alertType - The type of alert.
   * @param title - The title of the alert box.
   * @param content - The content of the dialog.
   * @return Alert - The alert dialog.
   */
  private static Alert initAlertDialog(AlertType alertType, String title, String content) {
    Alert alert = new Alert(alertType);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(content);

    return alert;
  }

  /**
   * Shows an information dialog with a title and content.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   */
  public static void showInformationDialog(String title, String content) {
    initAlertDialog(AlertType.INFORMATION, title, content).showAndWait();
  }

  /**
   * Shows an error dialog with a title and content.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   */
  public static void showErrorDialog(String title, String content) {
    initAlertDialog(AlertType.ERROR, title, content).showAndWait();
  }

  /**
   * Shows a confirmation dialog with a title and content.
   * Checks if the user clicked the OK button.
   * @param title - The title of the dialog.
   * @param content - The content of the dialog.
   * @return boolean - True if the user clicked OK, false otherwise.
   */
  public static boolean showConfirmationDialog(String title, String content) {
    Alert confirmationDialog = initAlertDialog(AlertType.CONFIRMATION, title, content);
    confirmationDialog.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
    Optional<ButtonType> result = confirmationDialog.showAndWait();
    return result.get() == ButtonType.OK;
  }
}
