package edu.ntnu.idatt2001.paths.views.storyplayer;

import edu.ntnu.idatt2001.paths.controllers.playgame.PlayGameController;
import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * This class is a part of the view of the game.
 * It is a pane that displays the player's health, score, gold and inventory.
 * Updates when the player's health, score, gold or inventory changes.
 *
 * @author Carl G.
 * @version 1.0 29.04.2023
 */
public class PlayerInfoPane extends VBox {

  /**
   * Creates a new player info pane
   * Adds text nodes for health, score, gold and inventory.
   * @param controller PlayGameController - The controller for the game.
   */
  public PlayerInfoPane(PlayGameController controller) {
    updatePlayerInfo(controller.getGame().getPlayer());
    controller.getGame().getPlayer().addPlayerChangeListener(this::updatePlayerInfo);

    setPadding(new Insets(20));
    setSpacing(15);
  }

  private void updatePlayerInfo(Player player) {
    getChildren().clear();
    Text healthText = createPlayerInfoText("Health", String.valueOf(player.getHealth()));
    Text scoreText = createPlayerInfoText("Score", String.valueOf(player.getScore()));
    Text goldText = createPlayerInfoText("Gold", String.valueOf(player.getGold()));

    Text inventoryText = createPlayerInventoryView(player);

    getChildren().addAll(healthText, scoreText, goldText, inventoryText);
  }

  /**
   * Creates a text node with the given label and player info.
   * @param label String - The label for the player info.
   * @param playerInfo String - The player info value.
   * @return Text - The text node with the player info.
   */
  private Text createPlayerInfoText(String label, String playerInfo) {
    Text text = new Text(label + ": " + playerInfo);
    text.setFont(FontEnum.BODY.getFont());
    return text;
  }

  /**
   * Creates a text node with the player's inventory.
   * @return Text - The text node with the player's inventory.
   */
  private Text createPlayerInventoryView(Player player) {
    StringBuilder sb = new StringBuilder();
    sb.append("Inventory:").append(System.lineSeparator());
    player
      .getInventory()
      .forEach(i -> sb.append("\u2022 ").append(i).append(System.lineSeparator()));
    Text text = new Text(sb.toString());
    text.setFont(FontEnum.BODY.getFont());
    return text;
  }
}
