package edu.ntnu.idatt2001.paths.views.components;

import edu.ntnu.idatt2001.paths.views.FontEnum;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;

/**
 * A button component for creating buttons with the same style throughout the application.
 * @author Callum G.
 * @version 1.0 30.04.2023
 */
public class ButtonComponent {

  /**
   * Creates a button with the given text and action.
   * @param text - The text to be displayed on the button.
   * @param action - The action to be performed when the button is clicked.
   * @return Button - The created button.
   */
  public static Button createButton(String text, EventHandler<ActionEvent> action) {
    Button button = new Button(text);
    button.setWrapText(true);
    button.setAlignment(Pos.CENTER);
    button.setFont(FontEnum.BUTTON.getFont());
    button.setOnAction(action);
    return button;
  }
}
