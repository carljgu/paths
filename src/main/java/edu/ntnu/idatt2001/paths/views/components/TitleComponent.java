package edu.ntnu.idatt2001.paths.views.components;

import edu.ntnu.idatt2001.paths.views.FontEnum;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * A component for creating titles with the same style throughout the application.
 * @author Callum G.
 * @version 1.0 30.04.2023
 */
public class TitleComponent {

  /**
   * Creates a title.
   * @param title the title.
   * @return the title.
   */
  public static Text createTitle(String title) {
    Text titleText = new Text(title);
    titleText.setFont(FontEnum.TITLE.getFont());
    titleText.setTextAlignment(TextAlignment.CENTER);
    return titleText;
  }
}
