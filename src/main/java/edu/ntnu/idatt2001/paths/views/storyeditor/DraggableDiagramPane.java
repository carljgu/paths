package edu.ntnu.idatt2001.paths.views.storyeditor;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * A pane that contains the story block nodes and the links between them.
 * Contains a canvas background grid which updates with the size of the pane.
 *
 * @author Carl G.
 * @version 0.1 30.04.2023
 */
public class DraggableDiagramPane extends Pane {

  private final Pane storyBlockNodesContainer;
  private final Pane linkContainer;
  private final Canvas background;

  private final double BACKGROUND_GRID_SIZE = 20;

  /**
   * Creates a new draggable diagram pane
   * sets listeners for the width and height properties to update the background grid
   */
  public DraggableDiagramPane() {
    super();
    this.storyBlockNodesContainer = new Pane();
    this.linkContainer = new Pane();
    this.background = new Canvas();
    this.widthProperty()
      .addListener((observable, oldValue, newValue) -> {
        this.background.setWidth(newValue.doubleValue());
        drawBackgroundGrid();
      });
    this.heightProperty()
      .addListener((observable, oldValue, newValue) -> {
        this.background.setHeight(newValue.doubleValue());
        drawBackgroundGrid();
      });
    this.getChildren().addAll(this.background, linkContainer, storyBlockNodesContainer);
  }

  /**
   * Returns the container for the story block nodes
   * @return Pane - the container for the story block nodes
   */
  public Pane getStoryBlockNodesContainer() {
    return storyBlockNodesContainer;
  }

  /**
   * Returns the container for the links
   * @return Pane - the container for the links
   */
  public Pane getLinkContainer() {
    return linkContainer;
  }

  /**
   * Returns the canvas for the background grid
   * @return Canvas - the canvas for the background grid
   */
  public Canvas getBackgroundCanvas() {
    return background;
  }

  /**
   * Returns the size of the background grid
   * @return double - the size of the background grid
   */
  public double getBackgroundGridSize() {
    return BACKGROUND_GRID_SIZE;
  }

  /**
   * Draws the background grid
   * Draws lines on the x axis and the y axis based on the background grid size
   */
  public void drawBackgroundGrid() {
    double startX = this.background.getLayoutX() - getLayoutX();
    double startY = this.background.getLayoutY() - getLayoutX();

    GraphicsContext gc = this.background.getGraphicsContext2D();
    gc.setStroke(Color.LIGHTGRAY);
    gc.setLineWidth(1);
    for (double y = startY; y < getHeight(); y += BACKGROUND_GRID_SIZE) {
      gc.strokeLine(startX, y, getWidth(), y);
    }
    for (double x = startX; x < getWidth(); x += BACKGROUND_GRID_SIZE) {
      gc.strokeLine(x, startY, x, getHeight());
    }
  }
}
