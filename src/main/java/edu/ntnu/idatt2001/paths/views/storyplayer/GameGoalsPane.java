package edu.ntnu.idatt2001.paths.views.storyplayer;

import edu.ntnu.idatt2001.paths.controllers.playgame.PlayGameController;
import edu.ntnu.idatt2001.paths.models.goals.Goal;
import edu.ntnu.idatt2001.paths.models.goals.GoalTypeEnum;
import edu.ntnu.idatt2001.paths.models.goals.InventoryGoal;
import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import java.util.List;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * This class is a part of the view of the game.
 * It is a pane that displays the goals of the game and whether they are fulfilled or not.
 * Updates when the player fulfills a goal.
 *
 * @author Carl G. Callum G.
 * @version 1.1 23.05.2023
 */
public class GameGoalsPane extends VBox {

  /**
   * Creates a new game goals pane
   * Adds text nodes for each goal.
   * @param controller PlayGameController - The controller for the pane.
   */
  public GameGoalsPane(PlayGameController controller) {
    Text title = new Text("Goals");
    title.setFont(FontEnum.TITLE.getFont());
    title.setId("goalListTitle");
    List<Goal<?>> goals = controller.getGame().getGoals();
    Player player = controller.getGame().getPlayer();

    getChildren().add(title);
    goals
      .stream()
      .forEach(goal -> {
        getChildren().add(createGoalText(goal, player));
      });
    player.addPlayerChangeListener(p -> {
      getChildren().removeIf(n -> !Objects.equals(n.getId(), "goalListTitle"));
      goals
        .stream()
        .forEach(goal -> {
          getChildren().add(createGoalText(goal, player));
        });
    });
    setPadding(new Insets(20));
    setSpacing(15);
  }

  /**
   * Creates a text node with the given goal.
   * @param goal Goal - The goal to display.
   * @param player Player - The player to check if the goal is fulfilled.
   * @return Text - The text node with the goal.
   */
  private Text createGoalText(Goal<?> goal, Player player) {
    GoalTypeEnum goalType = GoalTypeEnum.getGoalType(goal.getClass().getSimpleName());
    String goalTitle = goal.isFulfilled(player) ? "☑" : "☐";
    String goalCriteria = goal.getFulfillmentCriteria().toString();
    switch (goalType) {
      case GOLD:
        goalTitle += "collect " + goalCriteria + " gold";
        break;
      case HEALTH:
        goalTitle += "reach " + goalCriteria + " health";
        break;
      case MAXIUMUM_SCORE:
        goalTitle += "have at most " + goalCriteria + " score";
        break;
      case MINIMUM_SCORE:
        goalTitle += "get at least " + goalCriteria + " score";
        break;
      case INVENTORY:
        return createInventoryGoalText(((InventoryGoal) goal), goalTitle);
      case UNKNOWN:
        throw new IllegalArgumentException("Unknown goal type");
    }
    Text goalText = new Text(goalTitle);
    goalText.setFont(FontEnum.BODY.getFont());
    return goalText;
  }

  /**
   * Creates a text node with the given inventory goal.
   * @param goal InventoryGoal - The goal with items the player must have
   * @param goalTitle String - The temporary title of the goal
   */
  private Text createInventoryGoalText(InventoryGoal goal, String goalTitle) {
    StringBuilder sb = new StringBuilder();
    sb.append(goalTitle).append("Needs to collect:").append(System.lineSeparator());
    goal
      .getFulfillmentCriteria()
      .forEach(i -> sb.append("\u2022 ").append(i).append(System.lineSeparator()));
    Text text = new Text(sb.toString());
    text.setFont(FontEnum.BODY.getFont());
    return text;
  }
}
