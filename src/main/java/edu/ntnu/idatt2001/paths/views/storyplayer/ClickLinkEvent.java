package edu.ntnu.idatt2001.paths.views.storyplayer;

import edu.ntnu.idatt2001.paths.models.Link;
import javafx.event.ActionEvent;

/**
 * This class is an event that is fired when a link is clicked.
 * It is used to send a link with the action event.
 *
 * @author Carl G.
 * @version 1.0 29.04.2023
 */
public class ClickLinkEvent extends ActionEvent {

  private final Link link;

  /**
   * Creates a new click link event.
   * @param link Link - The link that was clicked.
   */
  public ClickLinkEvent(Link link) {
    super();
    this.link = link;
  }

  /**
   * Gets the link that was clicked.
   * @return Link - The link that was clicked.
   */
  public Link getLink() {
    return link;
  }
}
