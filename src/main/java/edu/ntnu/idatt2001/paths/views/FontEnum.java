package edu.ntnu.idatt2001.paths.views;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * An enum with different standard fonts to use throughout the application.
 * @author Carl G.
 * @version 0.1 - 23.04.2021
 */
public enum FontEnum {
  /**
   * A font for large titles.
   */
  LARGE_TITLE(Font.font("Arial", FontWeight.BOLD, 50)),

  /**
   * A font smaller titles
   */
  TITLE(Font.font("Arial", FontWeight.BOLD, 30)),

  /**
   * A font for body text.
   */
  BODY(Font.font("Arial", 24)),

  /**
   * A font for button text.
   */
  BUTTON(Font.font("Arial", FontWeight.BOLD, 20));

  private final Font font;

  FontEnum(Font font) {
    this.font = font;
  }

  /**
   * Returns the font.
   * @return the font.
   */
  public Font getFont() {
    return font;
  }
}
