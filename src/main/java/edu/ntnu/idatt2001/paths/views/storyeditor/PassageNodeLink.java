package edu.ntnu.idatt2001.paths.views.storyeditor;

import edu.ntnu.idatt2001.paths.models.Link;
import java.util.Objects;
import javafx.animation.RotateTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 * A link between two passage nodes. A source and a target.
 * The link is a line with an arrow at the end. The arrow is rotated to point towards the target
 * @author Carl G.
 * @version 0.1 30.04.2023
 */
public class PassageNodeLink extends Pane {

  private DragResizePassageNode source;
  private DragResizePassageNode target;
  private Line line;
  private Polygon arrow;
  private Text linkText;

  private ObjectProperty<Link> link;

  public static final Paint DEFAULT_COLOR = Color.BLACK;
  public static final double SELECTION_MARGIN = 50;

  /**
   * Creates a link between two passage nodes
   * Creates the line between them and an arrow in the middle of the line
   * @param source The source passage node
   * @param target The target passage node
   * @throws IllegalArgumentException if source or target is null
   */
  public PassageNodeLink(DragResizePassageNode source, DragResizePassageNode target)
    throws IllegalArgumentException {
    Objects.requireNonNull(source, "Source cannot be null");
    Objects.requireNonNull(target, "Target cannot be null");

    this.source = source;
    this.target = target;
    this.link =
      new SimpleObjectProperty<>(
        new Link(target.getPassage().getTitle(), target.getPassage().getTitle())
      );
    this.line = new Line();
    updateLinePostion();

    linkText = new Text();
    linkText.textProperty().bind(Bindings.createStringBinding(() -> link.get().getText(), link));
    linkText.setFont(new Font("Arial", 20));

    arrow = new Polygon();
    arrow.getPoints().addAll(0.0, 0.0, 10.0, 5.0, 0.0, 10.0);
    arrow.setFill(DEFAULT_COLOR);
    arrow.setStroke(DEFAULT_COLOR);
    arrow.setStrokeWidth(1);
    arrow.setLayoutX((line.getEndX() + line.getStartX()) / 2);
    arrow.setLayoutY((line.getEndY() + line.getStartY()) / 2);

    intializeLineChangeRunner();

    this.getChildren().addAll(line, arrow, linkText);
  }

  /**
   * Initializes the line change runner. This is a listener that updates the line and arrow position
   */
  private void intializeLineChangeRunner() {
    RotateTransition rotate = new RotateTransition(Duration.millis(1), arrow);
    rotate.setAxis(Rotate.Z_AXIS);
    rotate.setAutoReverse(true);
    rotate.setFromAngle(0);

    ChangeListener<Number> changeRunner = (observableValue, number, t1) -> {
      linkText.setLayoutX((line.getEndX() + line.getStartX()) / 2 + 30);
      linkText.setLayoutY((line.getEndY() + line.getStartY()) / 2 + 30);
      arrow.setLayoutX((line.getEndX() + line.getStartX()) / 2);
      arrow.setLayoutY((line.getEndY() + line.getStartY()) / 2);
      rotate.setByAngle(
        Math.atan2(line.getEndY() - line.getStartY(), line.getEndX() - line.getStartX()) *
        180 /
        Math.PI
      );
      rotate.play();
    };
    changeRunner.changed(null, null, null);
    line.startXProperty().addListener(changeRunner);
    line.startYProperty().addListener(changeRunner);
    line.endXProperty().addListener(changeRunner);
    line.endYProperty().addListener(changeRunner);
  }

  /**
   * Gets the source passage node
   * @return DragResizePassageNode - The source passage node
   */
  public DragResizePassageNode getSource() {
    return source;
  }

  /**
   * Gets the target passage node
   * @return DragResizePassageNode - The target passage node
   */
  public DragResizePassageNode getTarget() {
    return target;
  }

  /**
   * Gets the link
   * @return Link - The link of the passage node link
   */
  public Link getLink() {
    return link.get();
  }

  /**
   * Sets the link
   * @param link Link - The link of the passage nod link
   */
  public void setLink(Link link) {
    this.link.set(link);
  }

  /**
   * Sets the source passage node
   * @param source DragResizePassageNode - The source passage node
   * @throws IllegalArgumentException if source is null
   */
  public void setSource(DragResizePassageNode source) throws IllegalArgumentException {
    if (source == null) throw new IllegalArgumentException("Source cannot be null");
    this.source = source;
  }

  /**
   * Sets the target passage node
   * @param target DragResizePassageNode - The target passage node
   * @throws IllegalArgumentException if target is null
   */
  public void setTarget(DragResizePassageNode target) throws IllegalArgumentException {
    if (target == null) throw new IllegalArgumentException("Target cannot be null");
    this.target = target;
  }

  /**
   * Gets the line of the passage node link
   * @return Line - The line of the passage node link
   */
  public Line getLine() {
    return line;
  }

  /**
   * Updates the line position of the passage node link
   */
  public void updateLinePostion() {
    DragResizePassageNode source = getSource();
    DragResizePassageNode target = getTarget();

    boolean isSourceLeft = source.getLayoutX() < target.getLayoutX();
    double offsetPercent = DragResizePassageNode.CENTER_CORNER_OFFSET * (isSourceLeft ? 1 : -1);

    getLine().setStartX(source.getBetweenCenterAndCornerX(offsetPercent));
    getLine().setStartY(source.getBetweenCenterAndCornerY(offsetPercent));
    getLine().setEndX(target.getBetweenCenterAndCornerX(offsetPercent));
    getLine().setEndY(target.getBetweenCenterAndCornerY(offsetPercent));
  }

  /**
   * Selects the passage node link
   * Changes the color of the line and arrow
   */
  public void select() {
    line.setStroke(Color.RED);
    line.setStrokeWidth(5);
    arrow.setStroke(Color.RED);
  }

  /**
   * Deselects the passage node link
   * Changes the color of the line and arrow
   */
  public void deselect() {
    line.setStroke(DEFAULT_COLOR);
    line.setStrokeWidth(1);
    arrow.setStroke(DEFAULT_COLOR);
  }
}
