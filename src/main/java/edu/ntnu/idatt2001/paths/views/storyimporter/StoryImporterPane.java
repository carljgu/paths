package edu.ntnu.idatt2001.paths.views.storyimporter;

import edu.ntnu.idatt2001.paths.controllers.storyimporter.StoryImporterController;
import edu.ntnu.idatt2001.paths.views.FontEnum;
import edu.ntnu.idatt2001.paths.views.components.BaseGridPane;
import edu.ntnu.idatt2001.paths.views.components.ButtonComponent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * A pane for importing stories.
 * Extends the abstract class BaseGridPane.
 * @author Callum G.
 * @version 1.0 30.04.2023
 * @see BaseGridPane
 */
public class StoryImporterPane extends BaseGridPane {

  /**
   * Creates a new story importer pane.
   *
   * Adds a button for importing a story, a button for playing the story and a button for editing the story.
   * Also adds a list of errors that were generated when reading the file.
   * @param controller StoryImporterController - The controller for the pane.
   */
  public StoryImporterPane(StoryImporterController controller) {
    super();
    addBackToMainButton(controller);
    setTitle("Import a story");
    setAlignment(Pos.CENTER);
    setPadding(new Insets(50));

    VBox importStoryButtonBox = new VBox();
    importStoryButtonBox.setAlignment(Pos.CENTER);
    importStoryButtonBox.setSpacing(20);
    Text importedFileName = new Text();
    importedFileName.textProperty().bind(controller.getFileName());
    importStoryButtonBox
      .getChildren()
      .addAll(
        ButtonComponent.createButton("Import story", controller::importStory),
        importedFileName
      );

    VBox playEditStoryButtonBox = new VBox();
    playEditStoryButtonBox.setAlignment(Pos.CENTER);
    playEditStoryButtonBox.setSpacing(20);

    Button playStoryButton = ButtonComponent.createButton("Play story", controller::playStory);
    playStoryButton.disableProperty().bind(controller.getStoryImportedCorrectly().not());
    Button editStoryButton = ButtonComponent.createButton("Edit story", controller::editStory);
    editStoryButton.disableProperty().bind(controller.getStoryImportedCorrectly().not());
    playEditStoryButtonBox.getChildren().addAll(playStoryButton, editStoryButton);

    VBox buttonBox = new VBox();
    buttonBox.setAlignment(Pos.CENTER);
    buttonBox.setSpacing(20);
    buttonBox.getChildren().addAll(importStoryButtonBox, playEditStoryButtonBox);

    VBox errorVBox = new VBox();
    errorVBox.setAlignment(Pos.CENTER);
    errorVBox.setSpacing(5);
    Text errorText = new Text("Errors that were generated when reading the file:");
    errorText.setFont(FontEnum.TITLE.getFont());
    ListView<String> readErrors = new ListView<>();
    readErrors.setPlaceholder(new Text("No errors"));
    readErrors.setItems(controller.getReadErrors());
    readErrors.getStyleClass().add("read-errors-list");
    errorVBox.getChildren().addAll(errorText, readErrors);

    add(buttonBox, 1, 1, 1, 1);
    add(errorVBox, 0, 2, 3, 1);
  }
}
