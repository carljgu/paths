package edu.ntnu.idatt2001.paths.exceptions;

/**
 * Exception thrown when a story is unplayable
 *
 * @author Carl G.
 * @version 0.1 - 22.04.2023
 */
public class UnplayableStoryException extends RuntimeException {

  /**
   * Creates a new UnplayableStoryException with a message
   * @param message String - message to be displayed
   */
  public UnplayableStoryException(String message) {
    super(message);
  }
}
