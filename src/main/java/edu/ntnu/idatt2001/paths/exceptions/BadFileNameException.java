package edu.ntnu.idatt2001.paths.exceptions;

/**
 * Thrown if a file name is invalid.
 *
 * @author Callum G.
 * @version 0.1 - 23.04.2023
 */
public class BadFileNameException extends RuntimeException {

  /**
   * Constructor for the a wrong file extension exception.
   *
   * @param message String - the detail message to display
   */
  public BadFileNameException(String message) {
    super(message);
  }

  /**
   * Constructor for the a wrong file extension exception.
   *
   * Has a default message: "The file name is invalid".
   */
  public BadFileNameException() {
    super("The file name is invalid");
  }
}
