package edu.ntnu.idatt2001.paths.exceptions;

/**
 * Thrown if an extension of a file is invalid.
 *
 * @author Carl G. Callum G.
 * @version 0.2 23.04.2023
 */
public class WrongFileExtensionException extends RuntimeException {

  /**
   * Constructor for the a wrong file extension exception.
   *
   * @param message String - the detail message to display
   */
  public WrongFileExtensionException(String message) {
    super(message);
  }

  /**
   * Constructor for the a wrong file extension exception.
   *
   * Has a default message: "The file extension is invalid".
   */
  public WrongFileExtensionException() {
    super("The file extension is invalid");
  }
}
