package edu.ntnu.idatt2001.paths.exceptions;

/**
 * A runtime exception that should be thrown when a
 * story could not be read from a file becuase
 * the file was formatted incorrectly.
 *
 * @author Carl G. Callum G.
 * @version 0.2 23.04.2023
 */
public class StoryFormatException extends RuntimeException {

  /**
   * Constructor for the a story format exception.
   * @param message String - message to be displayed
   */
  public StoryFormatException(String message) {
    super(message);
  }

  /**
   * Constructor for the a story format exception.
   * Has a default message: "The story file is formatted incorrectly".
   */
  public StoryFormatException() {
    super("The story file is formatted incorrectly");
  }
}
