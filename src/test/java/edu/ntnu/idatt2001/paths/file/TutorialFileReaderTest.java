package edu.ntnu.idatt2001.paths.file;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2001.paths.models.tutorial.TutorialParagraph;
import java.util.List;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for TutorialFileReader.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TutorialFileReaderTest {

  @Nested
  class TutorialFileReaderReadTest {

    private static final String VALID_FILE_NAME = "/tutorialText.csv";

    @Test
    public void read_paragraphs_from_file_valid_and_empty_paragraphs() throws Exception {
      List<TutorialParagraph> paragraphs = assertDoesNotThrow(() -> {
        return TutorialFileReader.getInstance().readParagraphsFromFile(VALID_FILE_NAME);
      });

      assertEquals(5, paragraphs.size());
      assertEquals("Welcome", paragraphs.get(0).getTitle());
    }
  }
}
