package edu.ntnu.idatt2001.paths.file;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import java.io.File;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/**
 * Tests for FileUtils.
 * This class tests the methods in FileUtils. Currently, it only tests the
 * getFileExtension method, but it could be extended to test other methods as
 * well if more methods are added to FileUtils.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class FileUtilsTest {

  /**
   * Tests for getFileExtension.
   * Mainly tests that the method returns the correct file extension for
   * different file names.
   * It also tests that the method throws the correct exceptions when it is
   * given invalid arguments.
   */
  @Nested
  public class GetFileExtensionTest {

    @TempDir
    private Path tempDir;

    private File jpgFile;
    private File ymlFile;
    private File badFile;

    private final String JPG = "jpg";
    private final String JPG_FILE = "hello.jpg";

    private final String YML = "yml";
    private final String YML_FILE = ".gitlab-ci.yml";

    private final String BAD_FILE_NAME = "robot";

    @BeforeEach
    public void setUp() {
      jpgFile = tempDir.resolve(JPG_FILE).toFile();
      ymlFile = tempDir.resolve(YML_FILE).toFile();
      badFile = tempDir.resolve(BAD_FILE_NAME).toFile();
    }

    @Test
    public void get_file_extension_returns_file_extension() {
      assertEquals(FileUtils.getFileExtension(jpgFile), JPG);
    }

    @Test
    public void get_file_extension_returns_file_extension_of_last_dot() {
      assertEquals(FileUtils.getFileExtension(ymlFile), YML);
    }

    @Test
    public void get_file_extension_throws_exception_when_file_is_null() {
      assertThrows(NullPointerException.class, () -> FileUtils.getFileExtension(null));
    }

    @Test
    public void get_file_extension_throws_bad_filename_exception_when_file_name_has_no_dot() {
      assertThrows(BadFileNameException.class, () -> FileUtils.getFileExtension(badFile));
    }
  }
}
