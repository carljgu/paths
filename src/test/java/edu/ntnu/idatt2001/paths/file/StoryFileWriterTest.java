package edu.ntnu.idatt2001.paths.file;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mockConstruction;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.GoldAction;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.MockedConstruction;

/**
 * Tests for StoryFileWriter.
 * These tests make sure that the story file is written correctly and that the
 * correct exceptions are thrown when the file is invalid.
 * We also test that the file is written correctly by checking that the file
 * contains the correct information after it is written.
 * TempDir is used to create a temporary directory for the tests.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class StoryFileWriterTest {

  public static final String VALID_FILE_NAME = "writing_story.paths";

  /**
   * Tests for StoryFileWriter constructor.
   * Tests that the constructor does not throw exceptions when given valid file
   * path, and that it throws the correct exceptions when given invalid file
   * paths or null.
   */
  @Nested
  public class StoryFileWriterConstructorTest {

    @Test
    public void constructor_does_not_throw_exception_when_given_valid_file_path() {
      final File validFile = new File(VALID_FILE_NAME);
      StoryFileWriter storyFileWriter = new StoryFileWriter(validFile);

      assertEquals(storyFileWriter.getFile(), validFile);
    }

    @Test
    public void constructor_throws_bad_file_name_exception_when_given_blank_file_path() {
      assertThrows(BadFileNameException.class, () -> new StoryFileWriter(new File("")));
    }

    @Test
    public void constructor_throws_bad_file_name_exception_when_given_file_path_with_invalid_extension() {
      assertThrows(
        WrongFileExtensionException.class,
        () -> new StoryFileWriter(new File("invalid_file_name.txt"))
      );
    }

    @Test
    public void constructor_throws_null_pointer_exception_when_given_null_file_path() {
      assertThrows(NullPointerException.class, () -> new StoryFileWriter(null));
    }
  }

  /**
   * Tests for StoryFileWriter writeAction method.
   * Tests that the method does not throw exceptions when given a valid action.
   * Tests that the file is written correctly by checking that the
   * file contains the correct information after it is written.
   * The correct format of the action string after being written is:
   * <pre> {ActionType}{ActionValue} </pre>
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileWriterWriteActionTest#setUp} method and
   * {@link StoryFileWriterWriteActionTest#write_action_writes_correctly_to_file}
   * method for more information.
   *
   * @see {@link StoryFileWriter#writeAction(Action, BufferedWriter)}
   */
  @Nested
  public class StoryFileWriterWriteActionTest {

    private Action<?> action;

    // We need to use reflection to test the writeAction method as it is private.
    private Method writeActionMethod;

    private StoryFileWriter storyFileWriter;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileWriter = new StoryFileWriter(path.toFile());
      action = new GoldAction(10);

      // We get the writeAction method and make it accessible so that we can test it.
      writeActionMethod =
        StoryFileWriter.class.getDeclaredMethod("writeAction", Action.class, BufferedWriter.class);
      writeActionMethod.setAccessible(true);
    }

    @Test
    public void write_action_writes_correctly_to_file() throws Exception {
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()))) {
        // We invoke the writeAction method with the action string as an argument.
        writeActionMethod.invoke(storyFileWriter, action, writer);
      } catch (Exception e) {
        e.printStackTrace();
        fail();
      }
      String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
      assertEquals("{" + action.getClass().getSimpleName() + "}(" + action.getValue() + ")", line);
    }
  }

  /**
   * Tests for StoryFileWriter writeLink method.
   * Tests that the method does not throw exceptions when given a valid link.
   * Tests that the file is written correctly by checking that the
   * file contains the correct information after it is written.
   * The correct format of the link string after being written is:
   * <pre>
   * [LinkText](PassageName)
   * </pre>
   * It can also have an indefinite amount of action strings in the form:
   * <pre>
   * [LinkText](PassageName)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|...>
   * </pre>
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileWriterWriteActionTest#setUp} method and
   * {@link StoryFileWriterWriteActionTest#write_action_writes_correctly_to_file}
   * method for more information.
   *
   * @see {@link StoryFileWriter#writeLink(Link, BufferedWriter)}
   */
  @Nested
  public class StoryFileWriterWriteLinkTest {

    private Link link;

    private Method writeLinkMethod;

    private StoryFileWriter storyFileWriter;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileWriter = new StoryFileWriter(path.toFile());
      link = new Link("New Link", "Passage");
      writeLinkMethod =
        StoryFileWriter.class.getDeclaredMethod("writeLink", Link.class, BufferedWriter.class);
      writeLinkMethod.setAccessible(true);
    }

    @Test
    public void write_link_without_actions_writes_correctly_to_file() throws Exception {
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()))) {
        writeLinkMethod.invoke(storyFileWriter, link, writer);
      } catch (Exception e) {
        e.printStackTrace();
        fail();
      }
      String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
      assertEquals(
        "[" + link.getText() + "](" + link.getReference() + ")" + System.lineSeparator(),
        line
      );
    }

    @Test
    public void write_link_with_actions_writes_correctly_to_file() throws Exception {
      link.addAction(new GoldAction(10));
      link.addAction(new HealthAction(20));
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()))) {
        writeLinkMethod.invoke(storyFileWriter, link, writer);
      } catch (Exception e) {
        e.printStackTrace();
        fail();
      }
      String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
      final String expected =
        "[" +
        link.getText() +
        "](" +
        link.getReference() +
        ")<" +
        "{" +
        link.getActions().get(0).getClass().getSimpleName() +
        "}(" +
        link.getActions().get(0).getValue() +
        ")" +
        "|{" +
        link.getActions().get(1).getClass().getSimpleName() +
        "}(" +
        link.getActions().get(1).getValue() +
        ")>" +
        System.lineSeparator();
      assertEquals(expected, line);
    }
  }

  /**
   * Tests for StoryFileWriter writePassage method.
   * Tests that the method does not throw exceptions when given a valid passage.
   * Tests that the file is written correctly by checking that the
   * file contains the correct information after it is written.
   * The correct format of the passage string after being written is:
   * <pre>
   * ::Title
   *
   *
   * </pre>
   * The title must be on a new line and start with "::" and directly followed by the title.
   * But it can also have a content as follows:
   * <pre>
   * ::Title
   * Content
   *
   *
   * </pre>
   * The body must be on a new line and directly follow the title.
   * It can also have indefinite amount of links after the content as follows:
   * <pre>
   * ::Title
   * Content
   * [Link](Passage)<{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   *
   * </pre>
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileWriterWriteActionTest#setUp} method and
   * {@link StoryFileWriterWriteActionTest#write_action_writes_correctly_to_file}
   * method for more information.
   *
   * @see {@link StoryFileWriter#writePassage(Passage, BufferedWriter)}
   */
  @Nested
  public class StoryFileWriterWritePassageTest {

    private Passage passage;

    private Method writePassageMethod;

    private StoryFileWriter storyFileWriter;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileWriter = new StoryFileWriter(path.toFile());
      passage = new Passage("New Passage", "Content");
      writePassageMethod =
        StoryFileWriter.class.getDeclaredMethod(
            "writePassage",
            Passage.class,
            BufferedWriter.class
          );
      writePassageMethod.setAccessible(true);
    }

    @Test
    public void write_link_without_actions_writes_correctly_to_file() throws Exception {
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()))) {
        writePassageMethod.invoke(storyFileWriter, passage, writer);
      } catch (Exception e) {
        e.printStackTrace();
        fail();
      }
      String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
      assertEquals(
        "::" +
        passage.getTitle() +
        System.lineSeparator() +
        passage.getContent() +
        System.lineSeparator() +
        System.lineSeparator(),
        line
      );
    }

    @Test
    public void write_link_with_actions_writes_correctly_to_file() throws Exception {
      passage.addLink(new Link("New Link", "Passage"));
      passage.addLink(new Link("New Link 2", "Passage 2"));
      try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toString()))) {
        writePassageMethod.invoke(storyFileWriter, passage, writer);
      } catch (Exception e) {
        e.printStackTrace();
        fail();
      }
      String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
      final String expected =
        "::" +
        passage.getTitle() +
        System.lineSeparator() +
        passage.getContent() +
        System.lineSeparator() +
        "[" +
        passage.getLinks().get(0).getText() +
        "](" +
        passage.getLinks().get(0).getReference() +
        ")" +
        System.lineSeparator() +
        "[" +
        passage.getLinks().get(1).getText() +
        "](" +
        passage.getLinks().get(1).getReference() +
        ")" +
        System.lineSeparator() +
        System.lineSeparator();
      assertEquals(expected, line);
    }
  }

  /**
   *
   * Since the writeStory method is just a wrapper for the other write methods, we only need to test
   * that it calls the other methods correctly.
   * We make sure that the writeStory method calls the writePassage method correctly and
   * that it writes out the story title and opening passage and other passages correctly.
   *
   * @see {@link StoryFileWriter#writeStory(Story)}
   */
  @Nested
  public class StoryFileWriterWriteStoryTest {

    private Story story;

    private StoryFileWriter storyFileWriter;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileWriter = new StoryFileWriter(path.toFile());
      story = new Story("New Story", new Passage("Opening Passage", "Content"));
    }

    @Test
    public void write_story_writes_correctly_to_file() throws Exception {
      story.addPassage(new Passage("New Passage", "Content"));
      story.addPassage(new Passage("New Passage 2", "Content 2"));
      storyFileWriter.writeStory(story);
      String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
      final String expected =
        story.getTitle() +
        System.lineSeparator() +
        System.lineSeparator() +
        "::Opening Passage" +
        System.lineSeparator() +
        "Content" +
        System.lineSeparator() +
        System.lineSeparator() +
        "::New Passage" +
        System.lineSeparator() +
        "Content" +
        System.lineSeparator() +
        System.lineSeparator() +
        "::New Passage 2" +
        System.lineSeparator() +
        "Content 2" +
        System.lineSeparator() +
        System.lineSeparator();
      assertEquals(expected, line);
    }

    @Test
    public void write_story_with_no_passages_writes_correctly_to_file() {
      try {
        storyFileWriter.writeStory(story);
        String line = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        final String expected =
          story.getTitle() +
          System.lineSeparator() +
          System.lineSeparator() +
          "::Opening Passage" +
          System.lineSeparator() +
          "Content" +
          System.lineSeparator() +
          System.lineSeparator();
        assertEquals(expected, line);
      } catch (Exception e) {
        e.printStackTrace();
        fail();
      }
    }
  }

  /**
   * As the story file writer can throw an IOException, we need to test that it is thrown when
   * expected, and gives the correct message.
   * We do this by mocking the FileWriter and BufferedWriter classes, and throwing an IOException
   * when the write method is called.
   * This has to be done as it is only possible to throw an IOException through the FileWriter and the
   * BufferedWriter.
   * Mocking the FileWriter and BufferedWriter is quite difficult and we have to do it using the
   * MockedConstruction class from Mockito which then allows us to throw an IOException when the write
   * method is called on the constructed FileWriter and BufferedWriter.
   *
   * This was possible in JUnit 4 by using PowerMockito which was a bit simpler.
   *
   * @see {@link StoryFileWriter#writeStory(Story)}
   */
  @Nested
  public class StoryFileWriterWriteStoryThrowsException {

    private MockedConstruction<FileWriter> fileWriterMockedConstruction;

    private MockedConstruction<BufferedWriter> bufferedWriterMockedConstruction;

    private Story story;

    private StoryFileWriter storyFileWriter;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileWriter = new StoryFileWriter(path.toFile());
      story = new Story("New Story", new Passage("Opening Passage", "Content"));
    }

    /**
     * We need to close the mocked constructions after each test as they are not closed automatically.
     * This is done by calling the close method on the mocked construction.
     * If not we break all the other tests as the mocked constructions are still open.
     */
    @AfterEach
    public void tearDown() {
      fileWriterMockedConstruction.close();
      bufferedWriterMockedConstruction.close();
    }

    @Test
    public void write_story_throws_io_exception_when_buffered_reader_fails() throws Exception {
      fileWriterMockedConstruction =
        mockConstruction(
          FileWriter.class,
          (mock, context) -> {
            doThrow(IOException.class).when(mock).write(any(String.class));
          }
        );

      bufferedWriterMockedConstruction =
        mockConstruction(
          BufferedWriter.class,
          (mock, context) -> {
            doThrow(IOException.class).when(mock).write(any(String.class));
          }
        );

      try {
        storyFileWriter.writeStory(story);
        fail();
      } catch (Exception e) {
        assertEquals(IOException.class, e.getClass());
        assertEquals(e.getMessage(), "Failed to write to file: " + VALID_FILE_NAME + ".");
      }
    }
  }
}
