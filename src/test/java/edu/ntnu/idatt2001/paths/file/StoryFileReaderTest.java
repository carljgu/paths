package edu.ntnu.idatt2001.paths.file;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mockConstruction;

import edu.ntnu.idatt2001.paths.exceptions.BadFileNameException;
import edu.ntnu.idatt2001.paths.exceptions.StoryFormatException;
import edu.ntnu.idatt2001.paths.exceptions.WrongFileExtensionException;
import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.GoldAction;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryAddAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAllAction;
import edu.ntnu.idatt2001.paths.models.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.validation.StoryFileValidation;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.MockedConstruction;

/**
 * Tests for StoryFileReader.
 * These tests make sure that the file reader reads the file correctly and that
 * it throws the correct exceptions when it is given invalid files.
 * It also tests that the file reader can read all the different types of actions
 * that are currently supported and that skipped resources are handled correctly.
 * All tests use TempDir to create temporary files and directories, so that the tests
 * do not depend on the file system of the machine that runs the tests.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class StoryFileReaderTest {

  public static final String VALID_FILE_NAME = "writing_story.paths";

  /**
   * Tests for the constructor of the {@link StoryFileReader} class.
   * We test that the constructor throws the correct exceptions when given invalid file paths.
   * We also test that the constructor does not throw any exceptions when given valid file paths.
   */
  @Nested
  public class StoryFileReaderConstructorTest {

    @Test
    public void constructor_does_not_throw_exception_when_given_valid_file_path() {
      final File validFile = new File(VALID_FILE_NAME);
      StoryFileWriter storyFileWriter = new StoryFileWriter(validFile);

      assertEquals(storyFileWriter.getFile(), validFile);
    }

    @Test
    public void constructor_throws_bad_file_name_exception_when_given_blank_file_path() {
      assertThrows(BadFileNameException.class, () -> new StoryFileWriter(new File("")));
    }

    @Test
    public void constructor_throws_bad_file_name_exception_when_given_file_path_with_invalid_extension() {
      assertThrows(
        WrongFileExtensionException.class,
        () -> new StoryFileWriter(new File("invalid_file_name.txt"))
      );
    }

    @Test
    public void constructor_throws_null_pointer_exception_when_given_null_file_path() {
      assertThrows(NullPointerException.class, () -> new StoryFileWriter(null));
    }
  }

  /**
   * As we test the validity of the action string in the {@link StoryFileValidation#isValidAction} class, we can
   * assume that the action string is valid when we test the {@link StoryFileReader#readNextAction(String)} method.
   * Therefore, we only need to test the {@link StoryFileReader#readNextAction(String)} method with valid action
   * strings.
   * We test that the method returns the correct action when given a valid action string.
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileReaderReadNextActionTest#setUp} method and
   * {@link StoryFileReaderReadNextActionTest#read_next_action_returns_gold_action_when_given_gold_action_string}
   * method for more information.
   * The correct format of the action string is: <pre> {ActionType}{ActionValue} </pre>
   * @see StoryFileValidation#isValidAction()
   * @see StoryFileReader#readNextAction(String)
   */
  @Nested
  public class StoryFileReaderReadNextActionTest {

    private Action<?> action;

    // We need to use reflection to test the readNextAction method as it is private.
    private Method readNextActionMethod;

    private StoryFileReader storyFileReader;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());

      // We get the readNextAction method and make it accessible so that we can test it.
      readNextActionMethod =
        StoryFileReader.class.getDeclaredMethod("readNextAction", String.class);
      readNextActionMethod.setAccessible(true);
    }

    @Test
    public void read_next_action_returns_gold_action_when_given_gold_action_string()
      throws Exception {
      action = new GoldAction(10);
      // We invoke the readNextAction method with the action string as an argument, it
      // returns an Object, so we need to cast it to an Action.
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{" + this.action.getClass().getSimpleName() + "}(" + this.action.getValue() + ")"
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }

    @Test
    public void read_next_actionreturns_health_action_when_given_health_action_string()
      throws Exception {
      action = new HealthAction(10);
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{" + this.action.getClass().getSimpleName() + "}(" + this.action.getValue() + ")"
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }

    @Test
    public void read_next_action_returns_inventory_add_action_when_given_inventory_add_action_string()
      throws Exception {
      action = new InventoryAddAction("Sword");
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{" + this.action.getClass().getSimpleName() + "}(" + this.action.getValue() + ")"
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }

    @Test
    public void read_next_action_returns_inventory_remove_all_action_when_given_inventory_remove_all_action_string()
      throws Exception {
      action = new InventoryRemoveAllAction("Sword");
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{" + this.action.getClass().getSimpleName() + "}(" + this.action.getValue() + ")"
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }

    @Test
    public void read_next_action_return_score_action_when_given_score_action_string()
      throws Exception {
      action = new ScoreAction(10);
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{" + this.action.getClass().getSimpleName() + "}(" + this.action.getValue() + ")"
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }

    @Test
    public void read_next_action_returns_action_when_given_action_string_with_spaces()
      throws Exception {
      action = new GoldAction(10);
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{ " + this.action.getClass().getSimpleName() + " } ( " + this.action.getValue() + " ) "
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }

    @Test
    public void read_next_action_returns_action_when_given_action_string_with_spaces_and_tabs()
      throws Exception {
      action = new GoldAction(10);
      Action<?> readAction = (Action<?>) readNextActionMethod.invoke(
        storyFileReader,
        "{ \t" +
        this.action.getClass().getSimpleName() +
        "\t} ( \t" +
        this.action.getValue() +
        "\t) "
      );
      assertEquals(action.getClass(), readAction.getClass());
      assertEquals(action.getValue(), readAction.getValue());
    }
  }

  /**
   *
   * The read next action method should throw an exception when given an invalid action class or if
   * the action value is not a valid integer when expecting an integer value.
   * We can assume that the action string is valid when we test the {@link StoryFileReader#readNextAction(String)}
   * method as mentioned in the {@link StoryFileReaderReadNextActionTest} class.
   * We test that the method throws the correct exceptions when given invalid action strings.
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileReaderReadNextActionTest#setUp} method and
   * {@link StoryFileReaderReadNextActionTest#read_next_action_returns_gold_action_when_given_gold_action_string}
   * method for more information.
   * A correct action string is of the form: <pre>{ActionClass}(ActionValue)</pre>
   *
   * @see StoryFileValidation#isValidAction(String)
   * @see StoryFileReader#readNextAction(String)
   */
  @Nested
  public class StoryFileReaderReadNextActionTestThrows {

    private Method readNextActionMethod;

    private StoryFileReader storyFileReader;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());
      readNextActionMethod =
        StoryFileReader.class.getDeclaredMethod("readNextAction", String.class);
      readNextActionMethod.setAccessible(true);
    }

    @Test
    public void read_next_action_throws_story_format_exception_when_given_invalid_action_class()
      throws Exception {
      try {
        readNextActionMethod.invoke(storyFileReader, "{InvalidAction}(10)");
        fail("Should have thrown an exception");
      } catch (Exception e) {
        assertEquals(StoryFormatException.class, e.getCause().getClass());
      }
    }

    @Test
    public void read_next_action_throws_story_format_exception_when_given_invalid_action_value() {
      try {
        readNextActionMethod.invoke(storyFileReader, "{GoldAction}(InvalidValue)");
        fail("Should have thrown an exception");
      } catch (Exception e) {
        assertEquals(NumberFormatException.class, e.getCause().getClass());
      }
    }
  }

  /**
   *
   * As we test the validity of the link string in the {@link StoryFileValidation#isValidLink} class, we can
   * assume that the link string is valid when we test the {@link StoryFileReader#readNextLink(String)} method.
   * Therefore, we only need to test the {@link StoryFileReader#readNextLink(String)} method with valid link
   * strings.
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileReaderReadNextActionTest#setUp} method and
   * {@link StoryFileReaderReadNextActionTest#read_next_action_returns_gold_action_when_given_gold_action_string}
   * method for more information.
   * A correct link string is of the form: <pre>[LinkText](PassageName)</pre>
   * It can also have an indefinite amount of action strings in the form:
   * <pre>
   * [LinkText](PassageName)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|...>
   * </pre>
   *
   * @see StoryFileValidation#isValidLink(String)
   * @see StoryFileReader#readNextLink(String)
   */
  @Nested
  public class StoryFileReaderReadNextLinkTest {

    private Link link;

    private Method readNextLinkMethod;

    private StoryFileReader storyFileReader;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());
      readNextLinkMethod = StoryFileReader.class.getDeclaredMethod("readNextLink", String.class);
      readNextLinkMethod.setAccessible(true);
    }

    @Test
    public void read_next_link_returns_link_when_given_link_string() throws Exception {
      link = new Link("New Link", "Passage");
      Link readLink = (Link) readNextLinkMethod.invoke(
        storyFileReader,
        "[" + this.link.getText() + "](" + this.link.getReference() + ")"
      );
      assertEquals(link.getClass(), readLink.getClass());
      assertEquals(link.getText(), readLink.getText());
      assertEquals(link.getReference(), readLink.getReference());
    }

    @Test
    public void read_next_link_returns_link_with_action_when_given_link_with_one_action_string()
      throws Exception {
      link = new Link("New Link", "Passage");
      link.addAction(new GoldAction(10));
      Link readLink = (Link) readNextLinkMethod.invoke(
        storyFileReader,
        "[" +
        this.link.getText() +
        "](" +
        this.link.getReference() +
        ")<{" +
        this.link.getActions().get(0).getClass().getSimpleName() +
        "}(" +
        this.link.getActions().get(0).getValue() +
        ")>"
      );
      assertEquals(link.getClass(), readLink.getClass());
      assertEquals(link.getText(), readLink.getText());
      assertEquals(link.getReference(), readLink.getReference());
      assertEquals(link.getActions().get(0).getClass(), readLink.getActions().get(0).getClass());
      assertEquals(link.getActions().get(0).getValue(), readLink.getActions().get(0).getValue());
    }

    @Test
    public void read_next_link_returns_link_with_actions_when_given_link_with_multiple_action_strings()
      throws Exception {
      link = new Link("New Link", "Passage");
      link.addAction(new GoldAction(10));
      link.addAction(new ScoreAction(10));
      link.addAction(new HealthAction(10));
      link.addAction(new InventoryAddAction("Item"));
      link.addAction(new InventoryRemoveAllAction("Item"));

      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder
        .append("[")
        .append(this.link.getText())
        .append("](")
        .append(this.link.getReference())
        .append(")")
        .append("<");
      Iterator<Action<?>> iter = link.getActions().iterator();

      while (true) {
        Action<?> action = iter.next();
        stringBuilder
          .append("{")
          .append(action.getClass().getSimpleName())
          .append("}(")
          .append(action.getValue())
          .append(")");
        if (iter.hasNext()) {
          stringBuilder.append("|");
        } else {
          break;
        }
      }
      stringBuilder.append(">");

      Link readLink = (Link) readNextLinkMethod.invoke(storyFileReader, stringBuilder.toString());

      assertEquals(link.getClass(), readLink.getClass());
      assertEquals(link.getText(), readLink.getText());
      assertEquals(link.getReference(), readLink.getReference());
      assertEquals(link.getActions().size(), readLink.getActions().size());
      assertTrue(
        IntStream
          .range(0, link.getActions().size())
          .boxed()
          .allMatch(i ->
            link.getActions().get(i).getClass().equals(readLink.getActions().get(i).getClass())
          )
      );
    }

    @Test
    public void read_next_link_adds_actions_to_actions_skipped_if_wrong_format() throws Exception {
      Link readLink = (Link) readNextLinkMethod.invoke(
        storyFileReader,
        "[New Link](Passage)<{GoldAction}(Test)>"
      );

      assertEquals(0, readLink.getActions().size());
      assertEquals(1, storyFileReader.getActionsSkipped().size());
      assertEquals(
        "Line 1: {GoldAction}(Test) did not contain a number as a value.",
        storyFileReader.getActionsSkipped().get(0)
      );
    }

    @Test
    public void read_next_link_adds_actions_to_actions_skipped_if_wrong_format_multiple()
      throws Exception {
      Link readLink = (Link) readNextLinkMethod.invoke(
        storyFileReader,
        "[New Link](Passage)<{GoldAction}(Test)|{InvalidAction}(10)|{HealthAction}(10)>"
      );

      assertEquals(1, readLink.getActions().size());
      assertEquals(2, storyFileReader.getActionsSkipped().size());
      assertEquals(
        "Line 1: {GoldAction}(Test) did not contain a number as a value.",
        storyFileReader.getActionsSkipped().get(0)
      );
      assertEquals(
        "Line 1: {InvalidAction}(10) is not a valid action class.",
        storyFileReader.getActionsSkipped().get(1)
      );
    }
  }

  /**
   *
   * As we don't the validate passage strings with {@link StoryFileValidation} we need can't
   * assume that the passage string is valid when we test the {@link StoryFileReader#readNextPassage(String)} method,
   * like we do with the {@link StoryFileReader#readNextLink(String)} and {@link StoryFileReader#readNextAction(String)} methods.
   * As the method we are testing is private, we need to use reflection to test it, see
   * the comments in the {@link StoryFileReaderReadNextActionTest#setUp} method and
   * {@link StoryFileReaderReadNextActionTest#read_next_action_returns_gold_action_when_given_gold_action_string}
   * method for more information.
   * Passages must atleast have a title as follows:
   * <pre>
   * ::Title
   *
   *
   * </pre>
   * The title must be on a new line and start with "::" and directly followed by the title.
   * But it can also have a content as follows:
   * <pre>
   * ::Title
   * Content
   *
   *
   * </pre>
   * The body must be on a new line and directly follow the title.
   * It can also have indefinite amount of links after the content as follows:
   * <pre>
   * ::Title
   * Content
   * [Link](Passage)<{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   *
   * </pre>
   *
   * @see StoryFileValidation
   * @see StoryFileReader#readNextPassage(String)
   */
  @Nested
  public class StoryFileReaderReadNextPassageTest {

    private Passage passage;

    private Method readNextPassageMethod;

    private StoryFileReader storyFileReader;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());
      readNextPassageMethod =
        StoryFileReader.class.getDeclaredMethod("readNextPassage", String.class);
      readNextPassageMethod.setAccessible(true);
    }

    @Test
    public void read_next_passage_returns_passage_when_given_valid_passage_string()
      throws Exception {
      passage = new Passage("New Passage", "Text");
      Passage readPassage = (Passage) readNextPassageMethod.invoke(
        storyFileReader,
        "::" + passage.getTitle() + System.lineSeparator() + passage.getContent()
      );
      assertEquals(passage.getClass(), readPassage.getClass());
      assertEquals(passage.getTitle(), readPassage.getTitle());
      assertEquals(passage.getContent(), readPassage.getContent());
    }

    @Test
    public void read_next_passage_returns_passage_with_link_when_given_valid_passage_string_with_link()
      throws Exception {
      passage = new Passage("New Passage", "Text");
      passage.addLink(new Link("New Link", "Passage"));
      Passage readPassage = (Passage) readNextPassageMethod.invoke(
        storyFileReader,
        "::" +
        passage.getTitle() +
        System.lineSeparator() +
        passage.getContent() +
        System.lineSeparator() +
        "[New Link](Passage)"
      );
      assertEquals(passage.getClass(), readPassage.getClass());
      assertEquals(passage.getTitle(), readPassage.getTitle());
      assertEquals(passage.getContent(), readPassage.getContent());
      assertEquals(passage.getLinks().size(), readPassage.getLinks().size());
      assertEquals(passage.getLinks().get(0).getClass(), readPassage.getLinks().get(0).getClass());
      assertEquals(passage.getLinks().get(0).getText(), readPassage.getLinks().get(0).getText());
      assertEquals(
        passage.getLinks().get(0).getReference(),
        readPassage.getLinks().get(0).getReference()
      );
    }

    @Test
    public void read_next_passage_returns_passage_with_links_when_given_valid_passage_string_with_links()
      throws Exception {
      passage = new Passage("New Passage", "Text");
      passage.addLink(new Link("New Link", "Passage"));
      passage.addLink(new Link("New Link 2", "Passage 2"));
      Passage readPassage = (Passage) readNextPassageMethod.invoke(
        storyFileReader,
        "::" +
        passage.getTitle() +
        System.lineSeparator() +
        passage.getContent() +
        System.lineSeparator() +
        "[New Link](Passage)" +
        System.lineSeparator() +
        "[New Link 2](Passage 2)"
      );
      assertEquals(passage.getClass(), readPassage.getClass());
      assertEquals(passage.getTitle(), readPassage.getTitle());
      assertEquals(passage.getContent(), readPassage.getContent());
      assertEquals(passage.getLinks().size(), readPassage.getLinks().size());
      assertEquals(passage.getLinks().get(0).getClass(), readPassage.getLinks().get(0).getClass());
      assertEquals(passage.getLinks().get(0).getText(), readPassage.getLinks().get(0).getText());
      assertEquals(
        passage.getLinks().get(0).getReference(),
        readPassage.getLinks().get(0).getReference()
      );
      assertEquals(passage.getLinks().get(1).getClass(), readPassage.getLinks().get(1).getClass());
      assertEquals(passage.getLinks().get(1).getText(), readPassage.getLinks().get(1).getText());
      assertEquals(
        passage.getLinks().get(1).getReference(),
        readPassage.getLinks().get(1).getReference()
      );
    }

    @Test
    public void read_next_passage_adds_links_to_links_skipped_when_links_have_bad_format()
      throws Exception {
      passage = new Passage("New Passage", "Text");
      Passage readPassage = (Passage) readNextPassageMethod.invoke(
        storyFileReader,
        "::" +
        passage.getTitle() +
        System.lineSeparator() +
        passage.getContent() +
        System.lineSeparator() +
        "[New (Passage)" +
        System.lineSeparator() +
        "[New Link 2](Passage 2)"
      );
      assertEquals(passage.getClass(), readPassage.getClass());
      assertEquals(passage.getTitle(), readPassage.getTitle());
      assertEquals(passage.getContent(), readPassage.getContent());
      assertEquals(1, storyFileReader.getLinksSkipped().size());
      assertEquals(
        "Line 4: [New (Passage) did not have a valid format.",
        storyFileReader.getLinksSkipped().get(0)
      );
    }

    @Test
    public void read_next_passage_adds_links_to_links_skipped_when_links_have_are_not_valid()
      throws Exception {
      passage = new Passage("New Passage", "Text");
      Passage readPassage = (Passage) readNextPassageMethod.invoke(
        storyFileReader,
        "::" +
        passage.getTitle() +
        System.lineSeparator() +
        passage.getContent() +
        System.lineSeparator() +
        "[](Passage)" +
        System.lineSeparator() +
        "[New Link 2](Passage 2)" +
        System.lineSeparator() +
        "[New Link 3]"
      );
      assertEquals(passage.getClass(), readPassage.getClass());
      assertEquals(passage.getTitle(), readPassage.getTitle());
      assertEquals(passage.getContent(), readPassage.getContent());
      assertEquals(2, storyFileReader.getLinksSkipped().size());
      assertEquals(
        "Line 4: [](Passage) was not a valid link because: Text cannot be blank",
        storyFileReader.getLinksSkipped().get(0)
      );
      assertEquals(
        "Line 6: [New Link 3] did not have a valid format.",
        storyFileReader.getLinksSkipped().get(1)
      );
    }
  }

  /**
   * Checks that we throw a {@link StoryFileReaderException} when we try to read a passage string that is invalid.
   * The title validation is based on the {@link StoryFileValidation#isValidPassageStart(String)} method.
   * Other than that a valid passage should have the following format where only title is required:
   * <pre>
   * ::Title
   * Content
   * [Link](Passage)<{ActionClass}(ActionValue)>
   * [Link](Passage)
   * [Link](Passage)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   * </pre>
   * This means that a valid passage must atleast have a title title in the format: <pre>::Title</pre>
   * @see StoryFileReader#readNextPassage(String)
   */
  @Nested
  public class StoryFileReaderReadNextPassageThrowsTest {

    private Method readNextPassageMethod;

    private StoryFileReader storyFileReader;

    private Path path;

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());
      readNextPassageMethod =
        StoryFileReader.class.getDeclaredMethod("readNextPassage", String.class);
      readNextPassageMethod.setAccessible(true);
    }

    @Test
    public void read_next_passage_throws_exception_when_given_invalid_passage_string_no_title()
      throws Exception {
      try {
        readNextPassageMethod.invoke(storyFileReader, "::");
        fail("Should have thrown an exception");
      } catch (InvocationTargetException e) {
        assertEquals(StoryFormatException.class, e.getCause().getClass());
      }
    }

    @Test
    public void read_next_passage_throws_exception_when_given_invalid_passage_string_no_title_colon()
      throws Exception {
      try {
        readNextPassageMethod.invoke(storyFileReader, "Title");
        fail("Should have thrown an exception");
      } catch (InvocationTargetException e) {
        assertEquals(StoryFormatException.class, e.getCause().getClass());
      }
    }

    @Test
    public void read_next_passage_throws_exception_when_given_invalid_passage_string_one_colon()
      throws Exception {
      try {
        readNextPassageMethod.invoke(storyFileReader, ":Title");
        fail("Should have thrown an exception");
      } catch (InvocationTargetException e) {
        assertEquals(StoryFormatException.class, e.getCause().getClass());
      }
    }
  }

  /**
   * As the method {@link StoryFileReader#readStory()} is mainly a wrapper for the methods {@link StoryFileReader#readNextPassage(String)},
   * {@link StoryFileReader#readNextPassage(String)} and {@link StoryFileReader#readNextPassage(String)} we only need to test
   * that the method {@link StoryFileReader#readStory()} gives a story a title and handles the passages correctly.
   * A valid story should have the following format:
   * <pre>
   * Story Title
   *
   * ::Passage 1
   * Passage 1 Content
   * [Link](Passage)<{ActionClass}(ActionValue)>
   * [Link](Passage)
   *
   * ::Passage 2
   * Passage 2 Content
   * [Link](Passage)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   * ::Passage 3
   * Passage 3 Content
   * [Link](Passage)
   *
   * </pre>
   * @see StoryFileReader#readStory()
   */
  @Nested
  public class StoryFileReaderReadStoryTest {

    private Story story;

    private StoryFileReader storyFileReader;

    private Path path;

    private static final String VALID_STORY_STRING =
      "Story Title" +
      System.lineSeparator() +
      System.lineSeparator() +
      "::Passage 1" +
      System.lineSeparator() +
      "Passage 1 Content" +
      System.lineSeparator() +
      System.lineSeparator();

    private static final String VALID_STORY_STRING_WITH_MULTIPLE_PASSAGES =
      "Story Title" +
      System.lineSeparator() +
      System.lineSeparator() +
      "::Passage 1" +
      System.lineSeparator() +
      "Passage 1 Content" +
      System.lineSeparator() +
      System.lineSeparator() +
      "::Passage 2" +
      System.lineSeparator() +
      "Passage 2 Content" +
      System.lineSeparator() +
      System.lineSeparator() +
      "::Passage 3" +
      System.lineSeparator() +
      "Passage 3 Content" +
      System.lineSeparator() +
      System.lineSeparator();

    private static final String VALID_STORY_STRING_WITH_SOME_BAD_PASSAGES =
      "Story Title" +
      System.lineSeparator() +
      System.lineSeparator() +
      "::Passage 1" +
      System.lineSeparator() +
      "Passage 1 Content" +
      System.lineSeparator() +
      System.lineSeparator() +
      "Passage 2" +
      System.lineSeparator() +
      "Passage 2 Content" +
      System.lineSeparator() +
      System.lineSeparator() +
      ":Passage 3" +
      System.lineSeparator() +
      "Passage 3 Content" +
      System.lineSeparator() +
      System.lineSeparator();

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());
    }

    @Test
    public void read_story_returns_story_with_title_and_opening_passage() {
      try {
        Files.write(path, VALID_STORY_STRING.getBytes());
        story = storyFileReader.readStory();
      } catch (Exception e) {
        e.printStackTrace();
        fail("Should not have thrown an exception");
      }

      assertEquals("Story Title", story.getTitle());
      assertEquals("Passage 1", story.getOpeningPassage().getTitle());
      assertEquals("Passage 1 Content", story.getOpeningPassage().getContent());
    }

    @Test
    public void read_story_returns_story_with_title_and_multiple_passages() {
      try {
        Files.write(path, VALID_STORY_STRING_WITH_MULTIPLE_PASSAGES.getBytes());
        story = storyFileReader.readStory();
      } catch (Exception e) {
        e.printStackTrace();
        fail("Should not have thrown an exception");
      }

      assertEquals("Story Title", story.getTitle());
      assertEquals("Passage 1", story.getOpeningPassage().getTitle());
      assertEquals("Passage 1 Content", story.getOpeningPassage().getContent());
      assertEquals("Passage 2", story.getPassage(new Link("Passage 2", "Passage 2")).getTitle());
      assertEquals(
        "Passage 2 Content",
        story.getPassage(new Link("Passage 2", "Passage 2")).getContent()
      );
      assertEquals("Passage 3", story.getPassage(new Link("Passage 3", "Passage 3")).getTitle());
      assertEquals(
        "Passage 3 Content",
        story.getPassage(new Link("Passage 3", "Passage 3")).getContent()
      );
    }

    @Test
    public void read_story_returns_story_with_the_title_and_adds_to_passages_skipped() {
      try {
        Files.write(path, VALID_STORY_STRING_WITH_SOME_BAD_PASSAGES.getBytes());
        story = storyFileReader.readStory();
      } catch (Exception e) {
        e.printStackTrace();
        fail("Should not have thrown an exception");
      }

      assertEquals("Story Title", story.getTitle());
      assertEquals("Passage 1", story.getOpeningPassage().getTitle());
      assertEquals("Passage 1 Content", story.getOpeningPassage().getContent());
      assertEquals(2, storyFileReader.getPassagesSkipped().size());
      assertEquals(
        "Line 6: Passage 2" +
        System.lineSeparator() +
        "Passage 2 Content -> did not start with a valid passage start.",
        storyFileReader.getPassagesSkipped().get(0)
      );
      assertEquals(
        "Line 9: :Passage 3" +
        System.lineSeparator() +
        "Passage 3 Content -> did not start with a valid passage start.",
        storyFileReader.getPassagesSkipped().get(1)
      );
    }
  }

  /**
   * Tests for {@link StoryFileReader#readStory()} that check exceptions are thrown when
   * invalid story strings are given.
   *
   * A valid story should have the following format:
   * <pre>
   * Story Title
   *
   * ::Passage 1
   * Passage 1 Content
   * [Link](Passage)<{ActionClass}(ActionValue)>
   * [Link](Passage)
   *
   * ::Passage 2
   * Passage 2 Content
   * [Link](Passage)<{ActionClass}(ActionValue)|{ActionClass}(ActionValue)|{ActionClass}(ActionValue)>
   *
   * ::Passage 3
   * Passage 3 Content
   * [Link](Passage)
   *
   * </pre>
   * @see StoryFileReader#readStory()
   */
  @Nested
  public class StoryFileReaderReadStoryThrowsTest {

    private StoryFileReader storyFileReader;

    private Path path;

    private static final String INVALID_STORY_STRING_NO_OPENING_PASSAGE =
      "Story Title" + System.lineSeparator() + System.lineSeparator();

    private static final String INVALID_STORY_STRING_BAD_OPENING_PASSAGE =
      "Story Title" +
      System.lineSeparator() +
      System.lineSeparator() +
      ":Passage Title" +
      System.lineSeparator() +
      "Passage Content" +
      System.lineSeparator() +
      System.lineSeparator();

    @BeforeEach
    public void setUp(@TempDir Path tempDir) throws Exception {
      path = tempDir.resolve(VALID_FILE_NAME);
      path.toFile().createNewFile();
      storyFileReader = new StoryFileReader(path.toFile());
    }

    @Test
    public void read_story_throws_exception_when_given_invalid_story_string_no_opening_passage()
      throws Exception {
      Files.write(path, INVALID_STORY_STRING_NO_OPENING_PASSAGE.getBytes());
      assertThrows(StoryFormatException.class, () -> storyFileReader.readStory());
    }

    @Test
    public void read_story_throws_exception_when_given_invalid_story_string_bad_opening_passage()
      throws Exception {
      Files.write(path, INVALID_STORY_STRING_BAD_OPENING_PASSAGE.getBytes());
      assertThrows(StoryFormatException.class, () -> storyFileReader.readStory());
    }

    /**
     *
     * As the story file reader can throw an IOException, we need to test that it is thrown when
     * expected, and gives the correct message.
     * We do this by mocking the FileReader and BufferedReader classes, and throwing an IOException
     * when the read method is called.
     * This has to be done as it is only possible to throw an IOException through the FileReader and the
     * BufferedReader.
     * Mocking the FileReader and BufferedReader is quite difficult and we have to do it using the
     * MockedConstruction class from Mockito which then allows us to throw an IOException when the write
     * method is called on the constructed FileReader and BufferedReader.
     *
     */
    @Test
    public void read_story_throws_io_exception_when_buffered_reader_fails() {
      MockedConstruction<FileReader> fileReaderMockedConstruction;
      MockedConstruction<BufferedReader> bufferedReaderMockedConstruction;
      fileReaderMockedConstruction =
        mockConstruction(
          FileReader.class,
          (mock, context) -> {
            doThrow(IOException.class).when(mock).read();
          }
        );
      bufferedReaderMockedConstruction =
        mockConstruction(
          BufferedReader.class,
          (mock, context) -> {
            doThrow(IOException.class).when(mock).readLine();
          }
        );

      try {
        storyFileReader.readStory();
        fail("Should have thrown an exception");
      } catch (Exception e) {
        assertTrue(e instanceof IOException);
        assertEquals(
          "Could not read the characters from the file: '" +
          storyFileReader.getFile().getPath() +
          "'.",
          e.getMessage()
        );
      }

      fileReaderMockedConstruction.close();
      bufferedReaderMockedConstruction.close();
    }
  }
}
