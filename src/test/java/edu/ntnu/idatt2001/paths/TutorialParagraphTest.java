package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2001.paths.models.tutorial.TutorialParagraph;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for TutorialParagraph.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class TutorialParagraphTest {

  /**
   * Tests for TutorialParagraph constructor.
   */
  @Nested
  public class TutorialParagraphConstructorTest {

    @Test
    public void tutorial_paragraph_constructor_instance() {
      assertDoesNotThrow(() -> {
        new TutorialParagraph("Test", "Test", "Test");
      });
    }

    @Test
    public void tutorial_paragraph_constructor_null() {
      assertThrows(
        NullPointerException.class,
        () -> {
          new TutorialParagraph(null, null, null);
        }
      );
    }

    @Test
    public void tutorial_paragraph_constructor_empty() {
      assertThrows(
        IllegalArgumentException.class,
        () -> {
          new TutorialParagraph("", "", "");
        }
      );
    }

    @Test
    public void tutorial_paragraph_constructor_whitespace() {
      assertThrows(
        IllegalArgumentException.class,
        () -> {
          new TutorialParagraph(" ", " ", " ");
        }
      );
    }

    @Test
    public void tutorial_paragraph_constructor_no_image() {
      assertDoesNotThrow(() -> {
        new TutorialParagraph("Test", "Test");
      });
    }

    @Test
    public void tutorial_paragraph_constructor_no_image_null() {
      assertThrows(
        NullPointerException.class,
        () -> {
          new TutorialParagraph(null, null);
        }
      );
    }

    @Test
    public void tutorial_paragraph_constructor_no_image_empty() {
      assertThrows(
        IllegalArgumentException.class,
        () -> {
          new TutorialParagraph("", "");
        }
      );
    }
  }
}
