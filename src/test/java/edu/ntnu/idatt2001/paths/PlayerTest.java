package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for the Player class.
 * These tests make sure that the player is created correctly and that it
 * throws the correct exceptions when it is given invalid arguments.
 * We test all modifying functions to make sure that they work as intended
 * and that they throw the correct exceptions when given invalid arguments.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PlayerTest {

  /**
   * Tests for Player constructor.
   * Tests that the constructor cannot have null or blank name.
   * We also test that the constructor cannot have null inventory.
   * We test that the constructor cannot have negative health or gold.
   * We also test that the constructor can have negative score.
   */
  @Nested
  public class PlayerConstructorTest {

    @Test
    public void player_cannot_have_nullable_name() {
      final String nullString = null;
      assertThrows(NullPointerException.class, () -> new Player(nullString));
    }

    @Test
    public void player_cannot_have_blank_name() {
      assertThrows(IllegalArgumentException.class, () -> new Player("  "));
    }

    @Test
    public void player_can_be_instantiated_with_name_only() {
      new Player("Player 1");
      assertTrue(true);
    }

    @Test
    public void player_set_with_name_only_has_defaults_for_health_score_and_gold() {
      Player player = new Player("Player 1");
      assertEquals(100, player.getHealth());
      assertEquals(0, player.getScore());
      assertEquals(0, player.getGold());
    }

    @Test
    public void inventory_no_set_becomes_empty_list() {
      Player player = new Player("Player 1");
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void getting_inventory_returns_read_only_list() {
      Player player = new Player("Player 1");
      assertThrows(UnsupportedOperationException.class, () -> player.getInventory().add("Sword"));
    }

    @Test
    public void inventory_cannot_be_null() {
      assertThrows(NullPointerException.class, () -> new Player("Player 1", 1, 0, 0, null));
    }

    @Test
    public void health_cannot_start_at_zero_or_below() {
      assertThrows(IllegalArgumentException.class, () -> new Player("Player 1", 0, 1, 1));
      assertThrows(IllegalArgumentException.class, () -> new Player("Player 1", -1, 1, 1));
    }

    @Test
    public void gold_cannot_be_negative() {
      assertThrows(IllegalArgumentException.class, () -> new Player("Player 1", 1, 1, -1));
    }

    @Test
    public void score_can_start_as_negative() {
      assertDoesNotThrow(() -> new Player("Player 1", 1, -1, 1));
    }

    @Test
    public void gold_and_score_can_be_zero() {
      new Player("Player 1", 1, 0, 0);
      assertTrue(true);
    }

    @Test
    public void adding_inventory_items_with_whitespace_or_uppercase_letters_are_changed() {
      List<String> inventory = List.of("Sword", "Shield  ", "  POTION");
      Player player = new Player("Player 1", 1, 0, 0, inventory);
      assertEquals("sword", player.getInventory().get(0));
      assertEquals("shield", player.getInventory().get(1));
      assertEquals("potion", player.getInventory().get(2));
    }

    @Test
    public void adding_inventory_items_which_are_null_throws_exception() {
      List<String> inventory = new ArrayList<>(List.of("Sword", "Potion"));
      inventory.add(null);
      assertThrows(NullPointerException.class, () -> new Player("Player 1", 1, 0, 0, inventory));
    }
  }

  /**
   * Tests for Player equality.
   * Tests that two players with the same name are not equal.
   */
  @Nested
  public class PlayerEqualityTest {

    @Test
    public void players_with_equal_name_are_not_equal() {
      Player player1 = new Player("Player");
      Player player2 = new Player("Player");
      assertNotEquals(player1, player2);
    }
  }

  /**
   * Tests for Player modifying functions.
   * Tests that the player cannot have negative health or gold.
   * We also test that the player can have negative score.
   */
  @Nested
  public class PlayerModifyingScoreGoldHealthTest {

    @Test
    public void set_health_accepts_negative_numbers() {
      Player player = new Player("Player", 10, 0, 0);
      player.addHealth(-5);
      assertEquals(5, player.getHealth());
    }

    @Test
    public void can_add_health_to_player() {
      Player player = new Player("Player", 10, 0, 0);
      player.addHealth(5);
      assertEquals(15, player.getHealth());
    }

    @Test
    public void set_health_cannot_give_negative_health() {
      Player player = new Player("Player", 1, 0, 0);
      player.addHealth(-5);
      assertEquals(0, player.getHealth());
    }

    @Test
    public void set_score_accepts_negative_numbers() {
      Player player = new Player("Player", 10, 10, 0);
      player.addScore(-5);
      assertEquals(5, player.getScore());
    }

    @Test
    public void can_add_score_to_player() {
      Player player = new Player("Player", 10, 10, 0);
      player.addScore(5);
      assertEquals(15, player.getScore());
    }

    @Test
    public void set_score_can_give_negative_score() {
      Player player = new Player("Player", 1, 0, 0);
      player.addScore(-5);
      assertEquals(-5, player.getScore());
    }

    @Test
    public void set_gold_accepts_negative_numbers() {
      Player player = new Player("Player", 10, 0, 10);
      player.addGold(-5);
      assertEquals(5, player.getGold());
    }

    @Test
    public void can_add_gold_to_player() {
      Player player = new Player("Player", 1, 0, 10);
      player.addGold(5);
      assertEquals(15, player.getGold());
    }

    @Test
    public void set_gold_cannot_give_negative_gold() {
      Player player = new Player("Player", 1, 0, 1);
      player.addGold(-5);
      assertEquals(0, player.getGold());
    }

    @Test
    public void player_is_not_alive_when_health_is_zero() {
      Player player = new Player("Player", 1, 0, 0);
      player.addHealth(-5);
      assertEquals(0, player.getHealth());
      assertFalse(player.isAlive());
    }
  }

  /**
   * Tests for Player inventory.
   * Tests that the player can add items to their inventory.
   * Tests that the player cannot add empty strings or null to their inventory.
   * Tests that the player can add duplicate items to their inventory.
   * Tests that the player can remove items from their inventory.
   * Tests that the player cannot remove empty strings or null from their inventory.
   */
  @Nested
  public class ModifyingInventoryTest {

    private Player player;

    @BeforeEach
    public void setup() {
      player = new Player("Player");
    }

    @Test
    public void can_add_item_to_inventory() {
      player.addToInventory("Item");
      assertEquals(1, player.getInventory().size());
      assertTrue(player.isInInventory("Item"));
    }

    @Test
    public void cannot_add_empty_text_to_inventory() {
      assertThrows(IllegalArgumentException.class, () -> player.addToInventory(""));
    }

    @Test
    public void cannot_add_null_to_inventory() {
      assertThrows(NullPointerException.class, () -> player.addToInventory(null));
    }

    @Test
    public void can_add_duplicate_item_to_inventory() {
      player.addToInventory("Item");
      player.addToInventory("Item");
      assertEquals(2, player.getInventory().size());
      assertTrue(player.isInInventory("Item"));
      assertEquals("item", player.getInventory().get(0));
      assertEquals("item", player.getInventory().get(1));
    }

    @Test
    public void no_effect_when_removing_non_existing_item() {
      player.addToInventory("Item");
      player.removeInstancesFromInventory("Item 2");
      assertEquals(1, player.getInventory().size());
      assertTrue(player.isInInventory("Item"));
    }

    @Test
    public void no_effect_removing_item_from_empty_inventory() {
      player.removeInstancesFromInventory("Item");
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void all_instances_of_item_are_removed() {
      player.addToInventory("Item");
      player.addToInventory("Item");
      player.addToInventory("Item");
      player.addToInventory("Item 2");
      player.removeInstancesFromInventory("Item");
      assertEquals(1, player.getInventory().size());
      assertTrue(player.isInInventory("Item 2"));
    }

    @Test
    public void is_in_inventory_throws_when_item_is_null() {
      assertThrows(NullPointerException.class, () -> player.isInInventory(null));
    }

    @Test
    public void is_in_inventory_throws_when_item_is_empty() {
      assertThrows(IllegalArgumentException.class, () -> player.isInInventory(""));
    }

    @Test
    public void is_in_inventory_returns_true_if_item_exists() {
      player.addToInventory("Item");
      assertTrue(player.isInInventory("Item"));
    }

    @Test
    public void is_in_inventory_returns_false_if_item_exists() {
      assertFalse(player.isInInventory("Item"));
    }
  }

  /**
   * Tests for Player change listeners
   * Uses a listener called list of booleans to have
   * a final variable that can be accessed in the lambda expression.
   * We can then check if it was changed during the call.
   */
  @Nested
  public class PlayerChangeListener {

    private List<Boolean> listenerCalled;

    @BeforeEach
    public void setup() {
      listenerCalled = new ArrayList<>();
    }

    @Test
    public void player_change_listener_is_notified_when_player_health_changes() {
      Player player = new Player("Player", 10, 0, 0);
      player.addPlayerChangeListener(p -> {
        listenerCalled.add(true);
        assertEquals(p.getHealth(), 5);
      });
      player.addHealth(-5);
      assertEquals(1, listenerCalled.size());
      assertTrue(listenerCalled.get(0));
    }

    @Test
    public void player_change_listener_is_notified_when_player_gold_changes() {
      Player player = new Player("Player", 10, 0, 10);
      player.addPlayerChangeListener(p -> {
        listenerCalled.add(true);
        assertEquals(p.getGold(), 5);
      });
      player.addGold(-5);
    }

    @Test
    public void player_change_listener_is_notified_when_player_score_changes() {
      Player player = new Player("Player", 10, 10, 0);
      player.addPlayerChangeListener(p -> {
        listenerCalled.add(true);
        assertEquals(p.getScore(), 5);
      });
      player.addScore(-5);
      assertEquals(1, listenerCalled.size());
      assertTrue(listenerCalled.get(0));
    }

    @Test
    public void player_change_listener_is_notified_when_player_inventory_changes() {
      Player player = new Player("Player");
      player.addPlayerChangeListener(p -> {
        listenerCalled.add(true);
        assertEquals("test item", p.getInventory().get(0));
      });
      player.addToInventory("test item");
      assertEquals(1, listenerCalled.size());
      assertTrue(listenerCalled.get(0));
    }
  }
}
