package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2001.paths.models.game.GameDifficultyEnum;
import edu.ntnu.idatt2001.paths.models.player.Player;
import edu.ntnu.idatt2001.paths.models.player.PlayerBuilder;
import edu.ntnu.idatt2001.paths.models.player.PlayerStatEnum;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for PlayerBuilder.
 * Tests that the builder sets the correct values for the player.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PlayerBuilderTest {

  /**
   * Tests for the constructor.
   * Tests that the constructor sets the correct default values for the player.
   * We also test that the constructor sets the correct default values for the player
   * when the difficulty is set to easy, medium and hard.
   */
  @Nested
  public class PlayerBuilderConstructorTest {

    @Test
    public void should_set_default_values_for_player() {
      PlayerBuilder playerBuilder = new PlayerBuilder();

      Player player = playerBuilder.build();

      assertEquals("Player", player.getName());
      assertEquals(PlayerStatEnum.DEFAULT_HEALTH.getValue(), player.getHealth());
      assertEquals(PlayerStatEnum.DEFAULT_SCORE.getValue(), player.getScore());
      assertEquals(PlayerStatEnum.DEFAULT_GOLD.getValue(), player.getGold());
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void should_set_default_values_for_player_with_difficulty_easy() {
      PlayerBuilder playerBuilder = new PlayerBuilder(GameDifficultyEnum.EASY);

      Player player = playerBuilder.build();

      assertEquals("Player", player.getName());
      assertEquals(PlayerStatEnum.EASY_HEALTH.getValue(), player.getHealth());
      assertEquals(PlayerStatEnum.EASY_SCORE.getValue(), player.getScore());
      assertEquals(PlayerStatEnum.EASY_GOLD.getValue(), player.getGold());
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void should_set_default_values_for_player_with_difficulty_medium() {
      PlayerBuilder playerBuilder = new PlayerBuilder(GameDifficultyEnum.MEDIUM);

      Player player = playerBuilder.build();

      assertEquals("Player", player.getName());
      assertEquals(PlayerStatEnum.MEDIUM_HEALTH.getValue(), player.getHealth());
      assertEquals(PlayerStatEnum.MEDIUM_SCORE.getValue(), player.getScore());
      assertEquals(PlayerStatEnum.MEDIUM_GOLD.getValue(), player.getGold());
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void should_set_default_values_for_player_with_difficulty_hard() {
      PlayerBuilder playerBuilder = new PlayerBuilder(GameDifficultyEnum.HARD);

      Player player = playerBuilder.build();

      assertEquals("Player", player.getName());
      assertEquals(PlayerStatEnum.HARD_HEALTH.getValue(), player.getHealth());
      assertEquals(PlayerStatEnum.HARD_SCORE.getValue(), player.getScore());
      assertEquals(PlayerStatEnum.HARD_GOLD.getValue(), player.getGold());
      assertEquals(0, player.getInventory().size());
    }
  }

  /**
   * Tests for the build method.
   * Tests that a player is built with the correct values
   * if the values are valid for the Player class.
   * @see Player
   */
  @Nested
  public class PlayerBuilderBuildTest {

    @Test
    public void should_set_values_when_they_are_valid() {
      PlayerBuilder playerBuilder = new PlayerBuilder();

      Player player = playerBuilder
        .setName("Test")
        .setHealth(100)
        .setScore(100)
        .setGold(100)
        .build();

      assertEquals("Test", player.getName());
      assertEquals(100, player.getHealth());
      assertEquals(100, player.getScore());
      assertEquals(100, player.getGold());
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void should_set_values_when_they_are_valid_with_difficulty() {
      PlayerBuilder playerBuilder = new PlayerBuilder(GameDifficultyEnum.EASY);

      Player player = playerBuilder
        .setName("Test")
        .setHealth(100)
        .setScore(100)
        .setGold(100)
        .build();

      assertEquals("Test", player.getName());
      assertEquals(100, player.getHealth());
      assertEquals(100, player.getScore());
      assertEquals(100, player.getGold());
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void should_throw_illegal_argument_exception_when_values_are_invalid() {
      PlayerBuilder playerBuilder = new PlayerBuilder();

      playerBuilder.setName("Test").setHealth(-100).setScore(100).setGold(100);

      assertThrows(IllegalArgumentException.class, playerBuilder::build);
    }

    @Test
    public void should_throw_null_pointer_exception_when_values_are_null() {
      PlayerBuilder playerBuilder = new PlayerBuilder();

      playerBuilder.setName(null).setHealth(100).setScore(100).setGold(100);

      assertThrows(NullPointerException.class, playerBuilder::build);
    }
  }
}
