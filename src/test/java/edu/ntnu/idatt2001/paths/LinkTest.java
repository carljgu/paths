package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for the Link class.
 * These tests make sure that the link is created correctly and that it
 * throws the correct exceptions when it is given invalid arguments.
 * We also test that the link is equal to another link if they have the same
 * text and reference.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class LinkTest {

  /**
   * Tests for Link constructor.
   * Tests that the constructor cannot have null, empty or blank
   * text or reference.
   * We also test that the constructor cannot have null actions, or actions
   * containing null.
   */
  @Nested
  public class LinkConstructorTest {

    @Test
    public void link_cannot_have_null_reference() {
      assertThrows(NullPointerException.class, () -> new Link("Example link to a passage", null));
    }

    @Test
    public void link_cannot_have_whitespace_reference() {
      assertThrows(
        IllegalArgumentException.class,
        () -> new Link("Example link to a passage", "  ")
      );
    }

    @Test
    public void link_cannot_have_null_text() {
      assertThrows(NullPointerException.class, () -> new Link(null, "Example reference"));
    }

    @Test
    public void link_cannot_have_empty_text() {
      assertThrows(IllegalArgumentException.class, () -> new Link("", "Example reference"));
    }

    @Test
    public void illegal_argument_with_actions_as_null() {
      assertThrows(NullPointerException.class, () -> new Link("Link text", "Reference", null));
    }

    @Test
    public void link_cannot_have_have_actions_containing_null() {
      List<Action<?>> actions = new ArrayList<>();
      actions.add(null);
      assertThrows(NullPointerException.class, () -> new Link("Link text", "Reference", actions));
    }

    @Test
    public void new_array_list_created_when_actions_is_undefined() {
      Link link = new Link("Link text", "Reference");
      assertEquals(0, link.getActions().size());
      assertTrue(link.getActions() instanceof ArrayList);
    }

    @Test
    public void is_possible_to_add_existing_list_of_actions() {
      List<Action<?>> actions = new ArrayList<>();
      actions.add(new HealthAction(1));
      Link link = new Link("Link text", "Reference", actions);
      assertEquals(1, link.getActions().size());
    }
  }

  /**
   * Tests for the Link class' equals method.
   * We test that two links are equal if they have the same text and reference.
   * We also test that two links are not equal if they have the same text but
   * different reference.
   */
  @Nested
  public class LinkEqualityTest {

    @Test
    public void links_are_not_equal_if_they_have_same_text_but_not_reference() {
      Link link1 = new Link("Example link to a passage", "Reference 1");
      Link link2 = new Link("Example link to a passage", "Reference 2");
      assertNotEquals(link1, link2);
    }

    @Test
    public void links_with_equal_reference_but_not_text_are_equal() {
      Link link1 = new Link("First link", "Reference");
      Link link2 = new Link("Second link", "Reference");
      assertEquals(link1, link2);
    }
  }

  /**
   * Tests for the Link class' addAction method.
   * We test that the method cannot add null actions.
   */
  @Nested
  public class LinkActionTest {

    @Test
    public void cannot_add_null_action() {
      Link link = new Link("Link text", "Reference");
      assertThrows(NullPointerException.class, () -> link.addAction(null));
    }
  }
}
