package edu.ntnu.idatt2001.paths.actions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.InventoryAddAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAllAction;
import edu.ntnu.idatt2001.paths.models.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for InventoryActions -> InventoryAddAction and InventoryRemoveAllAction.
 * These tests make sure that the actions are executed correctly and that they
 * throw the correct exceptions when they are given invalid arguments.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InventoryActionTest {

  private Player player;
  private Action<?> action;
  private final String testItem = "Test item";

  @BeforeEach
  public void setUp() {
    player = new Player("Test player");
  }

  /**
   * Tests for InventoryAddAction.
   * Tests that the action cannot have null or blank item, and
   * that it cannot be executed with a null player.
   * We also test that the action is executed correctly by checking that the
   * player has the item in its inventory after the action is executed and that
   * the item is not added several times.
   */
  @Nested
  public class InventoryAddActionTest {

    @Test
    public void action_cannot_have_null_item() {
      assertThrows(NullPointerException.class, () -> new InventoryAddAction(null));
    }

    @Test
    public void action_cannot_have_blank_item() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryAddAction("  "));
    }

    @Test
    public void execute_cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new InventoryAddAction("Item").execute(null));
    }

    @Test
    public void inventory_add_action_executed_adds_item_to_player() {
      action = new InventoryAddAction(testItem);
      action.execute(player);
      assertTrue(player.isInInventory(testItem));
    }

    @Test
    public void executing_several_times_adds_items_to_player() {
      action = new InventoryAddAction(testItem);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(3, player.getInventory().size());
      assertTrue(player.isInInventory(testItem));
    }
  }

  /**
   * Tests for InventoryRemoveAllAction.
   * Tests that the action cannot have null or blank item, and
   * that it cannot be executed with a null player.
   * We also test that the action is executed correctly by checking that the
   * player does not have the item in its inventory after the action is executed
   * and that all instances of the item are removed.
   */
  @Nested
  public class InventoryRemoveAllActionTest {

    @Test
    public void action_cannot_have_null_item() {
      assertThrows(NullPointerException.class, () -> new InventoryRemoveAllAction(null));
    }

    @Test
    public void action_cannot_have_blank_item() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryRemoveAllAction("  "));
    }

    @Test
    public void execute_cannot_have_null_player() {
      assertThrows(
        NullPointerException.class,
        () -> new InventoryRemoveAllAction("Item").execute(null)
      );
    }

    @Test
    public void action_executed_removes_item_from_player() {
      player.addToInventory(testItem);
      action = new InventoryRemoveAllAction(testItem);
      action.execute(player);
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void action_removes_all_instances_of_item_from_player() {
      player.addToInventory(testItem);
      player.addToInventory(testItem);
      player.addToInventory(testItem);
      action = new InventoryRemoveAllAction(testItem);
      action.execute(player);
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void executing_several_times_has_no_effect() {
      player.addToInventory(testItem);
      action = new InventoryRemoveAllAction(testItem);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(0, player.getInventory().size());
    }
  }

  @Nested
  public class InventoryRemoveActionTest {

    @Test
    public void action_cannot_have_null_item() {
      assertThrows(NullPointerException.class, () -> new InventoryRemoveAction(null));
    }

    @Test
    public void action_cannot_have_blank_item() {
      assertThrows(IllegalArgumentException.class, () -> new InventoryRemoveAction("  "));
    }

    @Test
    public void execute_cannot_have_null_player() {
      assertThrows(
        NullPointerException.class,
        () -> new InventoryRemoveAction("Item").execute(null)
      );
    }

    @Test
    public void action_executed_removes_item_from_player() {
      player.addToInventory(testItem);
      action = new InventoryRemoveAction(testItem);
      action.execute(player);
      assertEquals(0, player.getInventory().size());
    }

    @Test
    public void action_does_not_remove_all_instances_of_item_from_player() {
      player.addToInventory(testItem);
      player.addToInventory(testItem);
      player.addToInventory(testItem);
      action = new InventoryRemoveAction(testItem);
      action.execute(player);
      assertEquals(2, player.getInventory().size());
    }

    @Test
    public void executing_several_times_has_no_effect() {
      player.addToInventory(testItem);
      action = new InventoryRemoveAction(testItem);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(0, player.getInventory().size());
    }
  }
}
