package edu.ntnu.idatt2001.paths.actions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2001.paths.models.actions.Action;
import edu.ntnu.idatt2001.paths.models.actions.GoldAction;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import edu.ntnu.idatt2001.paths.models.actions.ScoreAction;
import edu.ntnu.idatt2001.paths.models.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for ScoreGoldHealthActions -> ScoreAction, GoldAction and HealthAction.
 * These tests make sure that the actions are executed correctly and that they
 * throw the correct exceptions when they are given invalid arguments.
 * We also test that the actions are executed correctly by checking that the
 * player has the correct score, gold and health after the action is executed.
 * We also test that the actions are executed correctly when they are executed
 * several times.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ScoreGoldHealthActionTest {

  private Player player;
  private final int startHealth = 100;
  private final int startScore = 100;
  private final int startGold = 100;
  private Action<?> action;

  @BeforeEach
  public void setUp() {
    player = new Player("Test", startHealth, startScore, startGold);
  }

  /**
   * Tests for ScoreAction.
   * Tests that the action cannot have null player, and that
   * the score cannot be negative.
   * We also test that the action is executed correctly by checking that the
   * player has the correct score after the action is executed and that the
   * score is not added several times.
   */
  @Nested
  public class HealthActionTest {

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new HealthAction(10).execute(null));
    }

    @Test
    public void positive_score_action_executed_adds_score_to_player() {
      action = new HealthAction(10);
      action.execute(player);
      assertEquals(startHealth + 10, player.getHealth());
    }

    @Test
    public void negative_score_action_executed_subtracts_score_from_player() {
      action = new HealthAction(-10);
      action.execute(player);
      assertEquals(startHealth - 10, player.getHealth());
    }

    @Test
    public void several_executions_of_positive_score_action_adds_score_to_player() {
      action = new HealthAction(10);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(startHealth + 30, player.getHealth());
    }

    @Test
    public void several_executions_of_negative_score_action_subtracts_score_from_player() {
      action = new HealthAction(-10);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(startHealth - 30, player.getHealth());
    }

    @Test
    public void positive_and_negative_addition_from_different_actions_yields_start_score() {
      action = new HealthAction(10);
      action.execute(player);
      action = new HealthAction(-10);
      action.execute(player);
      assertEquals(startHealth, player.getHealth());
    }

    @Test
    public void subtraction_execution_to_negative_health_is_set_to_zero() {
      action = new HealthAction(-1 * (startHealth + 100));
      action.execute(player);
      assertEquals(0, player.getHealth());
    }
  }

  /**
   * Tests for ScoreAction.
   * Tests that the action cannot have null player, and that
   * the score cannot be negative.
   * We also test that the action is executed correctly by checking that the
   * player has the correct score after the action is executed and that the
   * score is not added several times.
   */
  @Nested
  public class ScoreActionTest {

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new ScoreAction(10).execute(null));
    }

    @Test
    public void positive_score_action_executed_adds_score_to_player() {
      action = new ScoreAction(10);
      action.execute(player);
      assertEquals(startScore + 10, player.getScore());
    }

    @Test
    public void negative_score_action_executed_subtracts_score_from_player() {
      action = new ScoreAction(-10);
      action.execute(player);
      assertEquals(startScore - 10, player.getScore());
    }

    @Test
    public void several_executions_of_positive_score_action_adds_score_to_player() {
      action = new ScoreAction(10);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(startScore + 30, player.getScore());
    }

    @Test
    public void several_executions_of_negative_score_action_subtracts_score_from_player() {
      action = new ScoreAction(-10);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(startScore - 30, player.getScore());
    }

    @Test
    public void positive_and_negative_addition_from_different_actions_yields_start_score() {
      action = new ScoreAction(10);
      action.execute(player);
      action = new ScoreAction(-10);
      action.execute(player);
      assertEquals(startScore, player.getScore());
    }
  }

  /**
   * Tests for GoldAction.
   * Tests that the action cannot have null player, and that
   * the score cannot be negative.
   * We also test that the action is executed correctly by checking that the
   * player has the correct score after the action is executed and that the
   * score is not added several times.
   */
  @Nested
  public class GoldActionTest {

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new GoldAction(10).execute(null));
    }

    @Test
    public void score_action_executed_adds_score_to_player() {
      action = new GoldAction(10);
      action.execute(player);
      assertEquals(startGold + 10, player.getGold());
    }

    @Test
    public void score_action_executed_subtracts_score_from_player() {
      action = new GoldAction(-10);
      action.execute(player);
      assertEquals(startGold - 10, player.getGold());
    }

    @Test
    public void several_executions_of_score_action_adds_score_to_player() {
      action = new GoldAction(10);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(startGold + 30, player.getGold());
    }

    @Test
    public void several_executions_of_score_action_subtracts_score_from_player() {
      action = new GoldAction(-10);
      action.execute(player);
      action.execute(player);
      action.execute(player);
      assertEquals(startGold - 30, player.getGold());
    }

    @Test
    public void positive_and_negative_addition_from_different_actions_yields_start_score() {
      action = new GoldAction(10);
      action.execute(player);
      action = new GoldAction(-10);
      action.execute(player);
      assertEquals(startGold, player.getGold());
    }
  }
}
