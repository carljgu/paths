package edu.ntnu.idatt2001.paths.actions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2001.paths.models.actions.ActionTypeEnum;
import edu.ntnu.idatt2001.paths.models.actions.GoldAction;
import edu.ntnu.idatt2001.paths.models.actions.HealthAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryAddAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAction;
import edu.ntnu.idatt2001.paths.models.actions.InventoryRemoveAllAction;
import edu.ntnu.idatt2001.paths.models.actions.ScoreAction;
import org.junit.jupiter.api.Test;

/**
 * Tests for the ActionTypeEnum class.
 * The only tested method is the getActionType method, which is the only method
 * that is not a constructor or a getter.
 * This method is especially important to test because it is used to get the
 * action type from a string, which is used to create the correct action in
 * the file reader.
 */
public class ActionTypeEnumTest {

  private final String GoldActionType = GoldAction.class.getSimpleName();

  private final String HealthActionType = HealthAction.class.getSimpleName();

  private final String ScoreActionType = ScoreAction.class.getSimpleName();

  private final String InventoryAddActionType = InventoryAddAction.class.getSimpleName();

  private final String InventoryRemoveActionType = InventoryRemoveAction.class.getSimpleName();

  private final String InventoryRemoveAllActionType =
    InventoryRemoveAllAction.class.getSimpleName();

  @Test
  public void get_action_type_returns_correct_action_type() {
    assertEquals(ActionTypeEnum.GOLD, ActionTypeEnum.getActionType(GoldActionType));
  }

  @Test
  public void get_action_type_returns_correct_action_type_when_given_health_action() {
    assertEquals(ActionTypeEnum.HEALTH, ActionTypeEnum.getActionType(HealthActionType));
  }

  @Test
  public void get_action_type_returns_correct_action_type_when_given_score_action() {
    assertEquals(ActionTypeEnum.SCORE, ActionTypeEnum.getActionType(ScoreActionType));
  }

  @Test
  public void get_action_type_returns_correct_action_type_when_given_inventory_add_action() {
    assertEquals(
      ActionTypeEnum.INVENTORY_ADD,
      ActionTypeEnum.getActionType(InventoryAddActionType)
    );
  }

  @Test
  public void get_action_type_returns_correct_action_type_when_given_inventory_remove_action() {
    assertEquals(
      ActionTypeEnum.INVENTORY_REMOVE,
      ActionTypeEnum.getActionType(InventoryRemoveActionType)
    );
  }

  @Test
  public void get_action_type_returns_correct_action_type_when_given_inventory_remove_all_action() {
    assertEquals(
      ActionTypeEnum.INVENTORY_REMOVE_ALL,
      ActionTypeEnum.getActionType(InventoryRemoveAllActionType)
    );
  }

  @Test
  public void get_action_type_returns_unknown_action_type_when_given_unknown_action() {
    assertEquals(ActionTypeEnum.UNKNOWN, ActionTypeEnum.getActionType("Unknown action"));
  }

  @Test
  public void get_action_type_returns_unknown_action_type_when_given_null() {
    assertEquals(ActionTypeEnum.UNKNOWN, ActionTypeEnum.getActionType(null));
  }
}
