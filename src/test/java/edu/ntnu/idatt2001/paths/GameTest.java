package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2001.paths.exceptions.UnplayableStoryException;
import edu.ntnu.idatt2001.paths.models.*;
import edu.ntnu.idatt2001.paths.models.game.Game;
import edu.ntnu.idatt2001.paths.models.goals.Goal;
import edu.ntnu.idatt2001.paths.models.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for the Game class.
 * These tests make sure that the game is created correctly and that it
 * throws the correct exceptions when it is given invalid arguments.
 * We also test that different methods in the game class work as intended.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class GameTest {

  private Story story;
  private Player player;
  private List<Goal<?>> goals;

  @BeforeEach
  public void setUp() {
    goals = new ArrayList<>();
    goals.add(new HealthGoal(2));
    player = new Player("Player name");
    Passage openingPassage = new Passage("Passage name");
    story = new Story("new story", openingPassage);
  }

  /**
   * Tests for the Game constructor.
   * Tests that the constructor cannot have null parameters.
   * We also test that the constructor cannot have null or empty list of goals.
   * We also test that the constructor cannot have null goals in the list.
   */
  @Nested
  public class ConstructorTest {

    @Test
    public void player_cannot_be_null() {
      assertThrows(NullPointerException.class, () -> new Game(null, story, goals));
    }

    @Test
    public void story_cannot_be_null() {
      assertThrows(NullPointerException.class, () -> new Game(player, null, goals));
    }

    @Test
    public void list_of_goals_cannot_be_null() {
      assertThrows(NullPointerException.class, () -> new Game(player, story, null));
    }

    @Test
    public void list_of_goals_cannot_contain_null() {
      goals.add(null);
      assertThrows(NullPointerException.class, () -> new Game(player, story, goals));
    }

    @Test
    public void list_of_goals_cannot_be_empty() {
      assertThrows(
        IllegalArgumentException.class,
        () -> new Game(player, story, new ArrayList<>())
      );
    }

    @Test
    public void game_is_created_with_all_non_null_parameters() {
      Game game = new Game(player, story, goals);
      assertEquals(player, game.getPlayer());
      assertEquals(story, game.getStory());
      assertEquals(goals, game.getGoals());
    }
  }

  /**
   * Tests for the Game traversal methods.
   * Tests that the game can begin and that it returns the correct passage.
   * We also test that the game can go to a passage and that it returns the correct passage.
   * We also test that the game throws the correct exceptions when it is given invalid arguments.
   * We check that the game throws an exception if the story is not playable.
   */
  @Nested
  public class GameTraversal {

    @Test
    public void begin_game_returns_the_opening_passage() {
      Passage openingPassage = new Passage("Passage name");
      Story story = new Story("new story", openingPassage);
      Game game = new Game(player, story, goals);
      assertEquals(openingPassage, game.begin());
    }

    @Test
    public void go_to_passage_returns_a_passage_connected_to_the_link() {
      String passageName = "next passage";
      Passage nextPassage = new Passage(passageName);
      Link link = new Link("Some text about link", passageName);
      Story story = new Story("new story", new Passage("Passage name"));
      story.addPassage(nextPassage);
      assertEquals(nextPassage, story.getPassage(link));
    }

    @Test
    public void get_passage_throws_if_link_is_null() {
      Story story = new Story("new story", new Passage("Passage name"));
      assertThrows(NullPointerException.class, () -> story.getPassage(null));
    }

    @Test
    public void passage_is_null_if_link_references_non_existent_passage() {
      Game game = new Game(player, story, goals);
      assertNull(game.go(new Link("link", "non existent passage")));
    }

    @Test
    public void unplayable_exception_if_story_is_not_playable() {
      Story story = new Story("new story", new Passage("Passage name"));
      Game game = new Game(player, story, goals);
      story.removePassage(
        new Link(story.getOpeningPassage().getTitle(), story.getOpeningPassage().getTitle())
      );
      assertFalse(story.isPlayable());
      assertThrows(UnplayableStoryException.class, game::begin);
    }
  }
}
