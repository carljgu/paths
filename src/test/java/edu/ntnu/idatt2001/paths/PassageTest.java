package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for the Passage class.
 * These tests make sure that the passage is created correctly and that it
 * throws the correct exceptions when it is given invalid arguments.
 * We also test that the passage is equal to another passage if they have the same
 * title and content.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PassageTest {

  /**
   * Tests for Passage constructor.
   * Tests that the constructor cannot have null, empty or blank
   * title or content.
   * We also test that the constructor cannot have null links, or links
   * containing null.
   */
  @Nested
  public class PassageConstructorTest {

    @Test
    public void passage_cannot_have_null_content() {
      assertThrows(
        NullPointerException.class,
        () -> new Passage("Example passage to a passage", null)
      );
    }

    @Test
    public void passage_content_can_be_whitespace() {
      new Passage("Example passage to a passage", "   ");
      assertTrue(true);
    }

    @Test
    public void passage_cannot_have_null_title() {
      assertThrows(NullPointerException.class, () -> new Passage(null, "Example content"));
    }

    @Test
    public void passage_cannot_have_empty_or_blank_title() {
      assertThrows(IllegalArgumentException.class, () -> new Passage("  ", "Example content"));
    }

    @Test
    public void passage_can_no_have_null_list_of_links() {
      assertThrows(
        NullPointerException.class,
        () -> new Passage("Example passage", "Example content", null)
      );
    }

    @Test
    public void list_of_links_can_be_empty_array_list() {
      new Passage("Example passage", "Example content", new ArrayList<>());
      assertTrue(true);
    }

    @Test
    public void list_of_links_can_be_list_of() {
      new Passage(
        "Example passage",
        "Example content",
        List.of(new Link("Example link", "Example passage"))
      );
      assertTrue(true);
    }

    @Test
    public void not_specifying_links_is_equivalent_to_empty_arraylist() {
      Passage passage = new Passage("Example passage", "Example content");
      assertTrue(passage.getLinks() instanceof ArrayList<Link>);
      assertEquals(0, passage.getLinks().size());
    }

    @Test
    public void only_specifying_title_creates_empty_content() {
      Passage passage = new Passage("Example passage");
      assertEquals("", passage.getContent());
    }
  }

  /**
   * Tests for the equals method.
   * We test that two passages are equal if they have the same title.
   * We also test that two passages are not equal if they have the same content,
   * but not the same title.
   */
  @Nested
  public class PassageEqualTests {

    @Test
    public void passages_are_equal_if_they_have_same_title_but_not_content() {
      Passage passage1 = new Passage("Example passage to a passage", "Content 1");
      Passage passage2 = new Passage("Example passage to a passage", "Content 2");
      assertEquals(passage1, passage2);
    }

    @Test
    public void passages_are_not_equal_if_they_have_same_content_but_not_title() {
      Passage passage1 = new Passage("First passage", "Content");
      Passage passage2 = new Passage("Second passage", "Content");
      assertNotEquals(passage1, passage2);
    }

    @Test
    public void passages_with_equal_title_and_content_but_not_links_are_equal() {
      Passage passage1 = new Passage(
        "Example passage",
        "Example content",
        List.of(new Link("Example link", "Example reference"))
      );
      Passage passage2 = new Passage(
        "Example passage",
        "Example content",
        List.of(new Link("Example link", "Example reference"))
      );
      assertEquals(passage1, passage2);
    }
  }

  /**
   * Tests for the addLink method.
   * We test that adding a link to a passage is successful if the list of links
   * is empty.
   * We also test that adding a link to a passage is unsuccessful if the list of links
   * already contains the link.
   * We also test that a passage has links after adding links.
   */
  @Nested
  public class PassageStoringLinksTests {

    @Test
    public void adding_link_to_passage_is_successful_on_empty_list() {
      Passage passage = new Passage("Example passage", "Example content");
      Link link = new Link("Example link", "Example reference");
      assertTrue(passage.addLink(link));
      assertEquals(1, passage.getLinks().size());
      assertEquals(link, passage.getLinks().get(0));
    }

    @Test
    public void adding_link_to_passage_fails_on_duplicate_link() {
      Passage passage = new Passage("Example passage", "Example content");
      Link link = new Link("Example link", "Example reference");
      assertTrue(passage.addLink(link));
      assertFalse(passage.addLink(link));
      assertEquals(1, passage.getLinks().size());
      assertEquals(link, passage.getLinks().get(0));
    }

    @Test
    public void passage_has_links_after_adding_links() {
      Passage passage = new Passage("Example passage", "Example content");
      Link link = new Link("Example link", "Example reference");
      assertTrue(passage.addLink(link));
      assertTrue(passage.hasLinks());
    }

    @Test
    public void passage_has_no_links_at_start_without_defining_links() {
      Passage passage = new Passage("Example passage", "Example content");
      assertFalse(passage.hasLinks());
    }
  }
}
