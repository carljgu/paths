package edu.ntnu.idatt2001.paths.validation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for {@link Validation}.
 * These tests make sure that the methods in {@link Validation} work as intended.
 * Rigorous tests are written for each method as validation serves as a crucial part of the
 * program and must work as intended.
 * These methods only throw exceptions when given invalid arguments,
 * if the arguments are valid, they just return the object.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ValidationTest {

  /**
   * Tests for all validation methods that take in a collection.
   * Tests that the methods do not throw exceptions when given valid arguments.
   * We also test that the methods throw the correct exceptions when given invalid arguments.
   */
  @Nested
  public class CollectionValidationTest {

    private final List<String> validStringList = List.of("Test1", "Test2", "Test3");
    private final List<String> nullStringList = new ArrayList<>(
      Arrays.asList("Test1", "Test2", null)
    );
    private final List<String> blankStringList = List.of(" ", "", "  ");
    private final Collection<String> nullCollection = null;

    @Test
    public void require_non_null_or_empty() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrEmpty(validStringList));
    }

    @Test
    public void require_non_null_or_empty_throws_if_null() {
      Collection<String> nullCollection = null;
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrEmpty(nullCollection)
      );
    }

    @Test
    public void require_non_null_or_empty_throws_if_empty() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireNonNullOrEmpty(List.of())
      );
    }

    @Test
    public void require_non_null_or_contains_null() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrContainsNull(validStringList));
    }

    @Test
    public void require_non_null_or_contains_null_throws_if_null() {
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrContainsNull(nullCollection)
      );
    }

    @Test
    public void require_non_null_or_contains_null_throws_if_contains_null() {
      nullStringList.add(null);
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrContainsNull(nullStringList)
      );
    }

    @Test
    public void require_non_null_or_empty_or_contains_null() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrEmptyOrContainsNull(validStringList));
    }

    @Test
    public void require_non_null_or_empty_or_contains_null_throws_if_contains_null() {
      nullStringList.add(null);
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrEmptyOrContainsNull(nullStringList)
      );
    }

    @Test
    public void require_non_null_or_empty_or_contains_null_or_blank() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrEmptyOrContainsNull(validStringList));
    }

    @Test
    public void require_non_null_or_empty_or_contains_null_or_blank_throws_if_contains_blank() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireNonNullOrEmptyOrContainsNullOrBlank(blankStringList)
      );
    }
  }

  /**
   * Tests for all validation methods that take in a map.
   * Tests that the methods do not throw exceptions when given valid arguments.
   * We also test that the methods throw the correct exceptions when given invalid arguments.
   */
  @Nested
  public class MapValidationTest {

    private final Map<String, String> validStringMap = Map.of("Test1", "Test2", "Test3", "Test4");
    private final Map<String, String> emptyStringMap = Map.of();
    private final Map<String, String> nullStringMap = new HashMap<>();

    {
      nullStringMap.put("Test1", "Test2");
      nullStringMap.put("Test3", null);
    }

    @Test
    public void require_non_null_or_empty() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrEmpty(validStringMap));
    }

    @Test
    public void require_non_null_or_empty_throws_if_null() {
      Map<String, String> nullMap = null;
      assertThrows(NullPointerException.class, () -> Validation.requireNonNullOrEmpty(nullMap));
    }

    @Test
    public void require_non_null_or_empty_throws_if_empty() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireNonNullOrEmpty(emptyStringMap)
      );
    }

    @Test
    public void require_non_null_or_contains_null() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrContainsNull(validStringMap));
    }

    @Test
    public void require_non_null_or_contains_null_throws_if_is_null() {
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrContainsNull(nullStringMap)
      );
    }

    @Test
    public void require_non_null_or_contains_null_throws_if_contains_null() {
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrContainsNull(nullStringMap)
      );
    }

    @Test
    public void require_non_null_or_empty_or_contains_null() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrEmptyOrContainsNull(validStringMap));
    }

    @Test
    public void require_non_null_or_empty_or_contains_null_throws_if_contains_null() {
      assertThrows(
        NullPointerException.class,
        () -> Validation.requireNonNullOrEmptyOrContainsNull(nullStringMap)
      );
    }
  }

  /**
   * Tests for all validation methods that take in a string.
   * Tests that the methods do not throw exceptions when given valid arguments.
   * We also test that the methods throw the correct exceptions when given invalid arguments.
   */
  @Nested
  public class StringValidationTest {

    private final String validString = "Test";
    private final String blankString = " ";

    @Test
    public void require_non_null_or_blank() {
      assertDoesNotThrow(() -> Validation.requireNonNullOrBlank(validString));
    }

    @Test
    public void require_non_null_or_blank_throws_if_is_null() {
      assertThrows(NullPointerException.class, () -> Validation.requireNonNullOrBlank(null));
    }

    @Test
    public void require_non_null_or_blank_throws_if_is_blank() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireNonNullOrBlank(blankString)
      );
    }
  }

  /**
   * Tests for all validation methods that take in a number.
   * Tests that the methods do not throw exceptions when given valid arguments.
   * We also test that the methods throw the correct exceptions when given invalid arguments.
   */
  @Nested
  public class NumberValidationTest {

    private final Integer positiveInteger = 1;
    private final Integer negativeInteger = -1;
    private final Integer zeroInteger = 0;

    @Test
    public void require_positive() {
      assertDoesNotThrow(() -> Validation.requirePositive(positiveInteger));
    }

    @Test
    public void require_positive_zero() {
      assertDoesNotThrow(() -> Validation.requirePositive(zeroInteger));
    }

    @Test
    public void require_positive_throws_if_is_negative() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requirePositive(negativeInteger)
      );
    }

    @Test
    public void require_smaller_than() {
      assertDoesNotThrow(() -> Validation.requireSmallerThan(positiveInteger, 2));
    }

    @Test
    public void require_smaller_than_throws_if_is_equal() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireSmallerThan(positiveInteger, 1)
      );
    }

    @Test
    public void require_smaller_than_throws_if_is_greater() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireSmallerThan(positiveInteger, 0)
      );
    }

    @Test
    public void require_large_than() {
      assertDoesNotThrow(() -> Validation.requireLargerThan(positiveInteger, 0));
    }

    @Test
    public void require_large_than_throws_if_is_equal() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireLargerThan(positiveInteger, 1)
      );
    }

    @Test
    public void require_large_than_throws_if_is_smaller() {
      assertThrows(
        IllegalArgumentException.class,
        () -> Validation.requireLargerThan(positiveInteger, 2)
      );
    }
  }
}
