package edu.ntnu.idatt2001.paths.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/**
 * Tests for {@link StoryFileValidation}.
 * These tests make sure that the methods in {@link StoryFileValidation} work as intended.
 * Rigorous tests are written for each method as validation serves as a crucial part of the
 * program and must work as intended.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class StoryFileValidationTest {

  /**
   * Tests for {@link StoryFileValidation#isValidFileType(File)}.
   * We test that the method returns true for a valid file type and false for an invalid file type.
   * The valid file type is a file with the extension .paths.
   * We also test that the method throws a {@link NullPointerException} if the file is null.
   */
  @Nested
  public class IsStoryFileTypeTest {

    @TempDir
    Path tempDir;

    private File pathsFile;
    private File jpgFile;

    private final String PATH_FILE = "example.paths";
    private final String JPG_FILE = "example.jpg";

    @BeforeEach
    public void setUp() {
      pathsFile = tempDir.resolve(PATH_FILE).toFile();
      jpgFile = tempDir.resolve(JPG_FILE).toFile();
    }

    @Test
    public void is_valid_file_type_paths_file_returns_true() {
      assertTrue(StoryFileValidation.isValidFileType(pathsFile));
    }

    @Test
    public void is_valid_file_type_jpg_file_returns_false() {
      assertFalse(StoryFileValidation.isValidFileType(jpgFile));
    }

    @Test
    public void is_valid_file_type_null_throws_exception() {
      assertThrows(NullPointerException.class, () -> StoryFileValidation.isValidFileType(null));
    }
  }

  /**
   * Tests for {@link StoryFileValidation#isValidPassageStart(String)}.
   * The regex used for this method is tested in {@link RegexEnumTest#PassageTitleRegexTest}.
   * We also test here in case the method is changed.
   */
  @Nested
  public class IsValidPassageStartTest {

    private final String validPassageTitle = "::Passage Title";
    private final String invalidPassageTitleNoTitle = "::";
    private final String invalidPassageTitleOneColon = ":Passage Title";
    private final String invalidPassageTitleNoColon = "Passage Title";

    @Test
    public void valid_passage_title_returns_true() {
      assertTrue(StoryFileValidation.isValidPassageStart(validPassageTitle));
    }

    @Test
    public void no_title_passage_title_returns_false() {
      assertFalse(StoryFileValidation.isValidPassageStart(invalidPassageTitleNoTitle));
    }

    @Test
    public void one_colon_passage_title_returns_false() {
      assertFalse(StoryFileValidation.isValidPassageStart(invalidPassageTitleOneColon));
    }

    @Test
    public void no_colon_passage_title_returns_false() {
      assertFalse(StoryFileValidation.isValidPassageStart(invalidPassageTitleNoColon));
    }

    @Test
    public void null_passage_title_throws_exception() {
      assertThrows(NullPointerException.class, () -> StoryFileValidation.isValidPassageStart(null));
    }
  }

  /**
   * Tests for {@link StoryFileValidation#isValidLink(String)}.
   * The regex used for this method is tested in {@link RegexEnumTest#LinkRegexTest}.
   * We also test here in case the method is changed.
   */
  @Nested
  public class IsValidLinkTest {

    private final String validLink = "[link text](passage title)";
    private final String validLinkWithActions =
      "[link text](passage title)<{ActionType}(ActionValue)|{ActionType}(ActionValue)>";
    private final String invalidLinkNoOpeningBracket = "link text](passage title)";
    private final String invalidLinkNoClosingBracket = "[link text(passage title)";
    private final String invalidLinkNoOpeningParenthesis = "[link text]passage title)";
    private final String invalidLinkNoClosingParenthesis = "[link text](passage title";
    private final String invalidLinkFirstCharacterNotOpeningBracket = "l[ink text](passage title)";
    private final String invalidLinkLastCharacterNotClosingBracket = "[link text](passage title)l";
    private final String invalidLinkFirstCharacterNotOpeningParenthesis =
      "[link text]p(passage title)";
    private final String invalidLinkParenthesisIsBeforeBracket = "[link text(]passage title)";
    private final String invalidLinkWithActions =
      "[link text](passage title<{ActionType}(ActionValue)|{ActionType}(ActionValue)>";

    @Test
    public void valid_link_returns_true() {
      assertTrue(StoryFileValidation.isValidLink(validLink));
    }

    @Test
    public void valid_link_with_actions_returns_true() {
      assertTrue(StoryFileValidation.isValidLink(validLinkWithActions));
    }

    @Test
    public void no_opening_bracket_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkNoOpeningBracket));
    }

    @Test
    public void no_closing_bracket_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkNoClosingBracket));
    }

    @Test
    public void no_opening_parenthesis_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkNoOpeningParenthesis));
    }

    @Test
    public void no_closing_parenthesis_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkNoClosingParenthesis));
    }

    @Test
    public void first_character_not_opening_bracket_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkFirstCharacterNotOpeningBracket));
    }

    @Test
    public void last_character_not_closing_bracket_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkLastCharacterNotClosingBracket));
    }

    @Test
    public void first_character_not_opening_parenthesis_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkFirstCharacterNotOpeningParenthesis));
    }

    @Test
    public void parenthesis_is_before_bracket_link_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkParenthesisIsBeforeBracket));
    }

    @Test
    public void link_with_actions_returns_false() {
      assertFalse(StoryFileValidation.isValidLink(invalidLinkWithActions));
    }
  }

  /**
   * Tests for {@link StoryFileValidation#isValidAction(String)}.
   * The regex used for this method is tested in {@link RegexEnumTest#ActionRegexTest}.
   * We also test here in case the method is changed.
   */
  @Nested
  public class IsValidActionTest {

    private final String validAction = "{ActionType}(ActionValue)";
    private final String invalidActionNoOpeningBracket = "ActionType}(ActionValue)";
    private final String invalidActionNoClosingBracket = "{ActionType(ActionValue)";
    private final String invalidActionNoOpeningParenthesis = "{ActionType}ActionValue)";
    private final String invalidActionNoClosingParenthesis = "{ActionType}(ActionValue";
    private final String invalidActionFirstCharacterNotOpeningBracket = "A{ctionType}(ActionValue)";
    private final String invalidActionLastCharacterNotClosingBracket = "{ActionType}(ActionValue)A";
    private final String invalidActionFirstCharacterNotOpeningParenthesis =
      "{ActionType}A(ActionValue)";
    private final String invalidActionParenthesisIsBeforeBracket = "{ActionType(A)(ActionValue)";

    @Test
    public void valid_action_returns_true() {
      assertTrue(StoryFileValidation.isValidAction(validAction));
    }

    @Test
    public void no_opening_bracket_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionNoOpeningBracket));
    }

    @Test
    public void no_closing_bracket_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionNoClosingBracket));
    }

    @Test
    public void no_opening_parenthesis_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionNoOpeningParenthesis));
    }

    @Test
    public void no_closing_parenthesis_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionNoClosingParenthesis));
    }

    @Test
    public void first_character_not_opening_bracket_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionFirstCharacterNotOpeningBracket));
    }

    @Test
    public void last_character_not_closing_bracket_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionLastCharacterNotClosingBracket));
    }

    @Test
    public void first_character_not_opening_parenthesis_action_returns_false() {
      assertFalse(
        StoryFileValidation.isValidAction(invalidActionFirstCharacterNotOpeningParenthesis)
      );
    }

    @Test
    public void parenthesis_is_before_bracket_action_returns_false() {
      assertFalse(StoryFileValidation.isValidAction(invalidActionParenthesisIsBeforeBracket));
    }
  }

  /**
   * Tests for {@link StoryFileValidation#containsActions(String)}.
   * This method is used to check if a link contains actions.
   * The regex used for this method is tested in {@link RegexEnumTest#LinkContainsActionsRegexTest}.
   * We also test here in case the method is changed.
   */
  @Nested
  public class ContainsActionsTest {

    private final String validLinkWithActions =
      "[link text](passage title)<{ActionType}(ActionValue)|{ActionType}(ActionValue)>";
    private final String validLinkWithOneAction =
      "[link text](passage title)<{ActionType}(ActionValue)>";
    private final String validLinkWithNoActions = "[link text](passage title)";
    private final String invalidLinkContainsActionsNoClosingParenthesis =
      "[link text](passage title<{ActionType}(ActionValue)>";
    private final String invalidLinkContainsActionsNoOpeningAngleBracket =
      "[link text](passage title){ActionType}(ActionValue)>";
    private final String invalidLinkContainsActionsNoClosingAngleBracket =
      "[link text](passage title)<{ActionType}(ActionValue)";
    private final String invalidLinkContainsActionsFirstCharacterNotAngleBracket =
      "[link text](passage title)d<{";
    private final String invalidLinkContainsActionsLastCharacterNotAngleBracket =
      "[link text](passage title)<{ActionType}(ActionValue)d";
    private final String invalidLinkContainsActionsLastChars =
      "[link text](passage title)<{ActionType}(ActionValue)|{ActionType}(ActionValue>";

    @Test
    public void valid_link_with_actions_returns_true() {
      assertTrue(StoryFileValidation.containsActions(validLinkWithActions));
    }

    @Test
    public void valid_link_with_one_action_returns_true() {
      assertTrue(StoryFileValidation.containsActions(validLinkWithOneAction));
    }

    @Test
    public void valid_link_with_no_actions_returns_false() {
      assertFalse(StoryFileValidation.containsActions(validLinkWithNoActions));
    }

    @Test
    public void invalid_link_contains_actions_no_closing_parenthesis_returns_false() {
      assertFalse(
        StoryFileValidation.containsActions(invalidLinkContainsActionsNoClosingParenthesis)
      );
    }

    @Test
    public void invalid_link_contains_actions_no_opening_angle_bracket_returns_false() {
      assertFalse(
        StoryFileValidation.containsActions(invalidLinkContainsActionsNoOpeningAngleBracket)
      );
    }

    @Test
    public void invalid_link_contains_actions_no_closing_angle_bracket_returns_false() {
      assertFalse(
        StoryFileValidation.containsActions(invalidLinkContainsActionsNoClosingAngleBracket)
      );
    }

    @Test
    public void invalid_link_contains_actions_first_character_not_angle_bracket_returns_false() {
      assertFalse(
        StoryFileValidation.containsActions(invalidLinkContainsActionsFirstCharacterNotAngleBracket)
      );
    }

    @Test
    public void invalid_link_contains_actions_last_character_not_angle_bracket_returns_false() {
      assertFalse(
        StoryFileValidation.containsActions(invalidLinkContainsActionsLastCharacterNotAngleBracket)
      );
    }

    @Test
    public void invalid_link_contains_actions_last_chars_returns_false() {
      assertFalse(StoryFileValidation.containsActions(invalidLinkContainsActionsLastChars));
    }
  }
}
