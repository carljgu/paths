package edu.ntnu.idatt2001.paths.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for RegexEnum.
 * These tests make sure that the regexes are correct and that they match
 * the correct strings.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RegexEnumTest {

  /**
   * Tests for PassageTitleRegex.
   * Tests that the regex matches valid passage titles and that it does not
   * match invalid passage titles.
   * {@link RegexEnum#PASSAGE_TITLE}
   */
  @Nested
  public class PassageTitleRegexTest {

    private final String validPassageTitle = "::Passage Title";
    private final String invalidPassageTitleNoTitle = "::";
    private final String invalidPassageTitleOneColon = ":Passage Title";
    private final String invalidPassageTitleNoColon = "Passage Title";

    @Test
    public void valid_passage_title_matches_regex() {
      assertTrue(RegexEnum.matches(validPassageTitle, RegexEnum.PASSAGE_TITLE));
    }

    @Test
    public void no_title_passage_title_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidPassageTitleNoTitle, RegexEnum.PASSAGE_TITLE));
    }

    @Test
    public void one_colon_passage_title_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidPassageTitleOneColon, RegexEnum.PASSAGE_TITLE));
    }

    @Test
    public void no_colon_passage_title_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidPassageTitleNoColon, RegexEnum.PASSAGE_TITLE));
    }
  }

  /**
   * Tests for LinkRegex.
   * Tests that the regex matches valid links and that it does not match
   * invalid links.
   * {@link RegexEnum#LINK}
   */
  @Nested
  public class LinkRegexTest {

    private final String validLink = "[link text](passage title)";
    private final String invalidLinkNoOpeningBracket = "link text](passage title)";
    private final String invalidLinkNoClosingBracket = "[link text(passage title)";
    private final String invalidLinkNoOpeningParenthesis = "[link text]passage title)";
    private final String invalidLinkNoClosingParenthesis = "[link text](passage title";
    private final String invalidLinkFirstCharacterNotOpeningBracket = "l[ink text](passage title)";
    private final String invalidLinkLastCharacterNotClosingBracket = "[link text](passage title)l";
    private final String invalidLinkFirstCharacterNotOpeningParenthesis =
      "[link text]p(passage title)";
    private final String invalidLinkParenthesisIsBeforeBracket = "[link text(]passage title)";

    @Test
    public void valid_link_matches_regex() {
      assertTrue(RegexEnum.matches(validLink, RegexEnum.LINK));
    }

    @Test
    public void no_opening_bracket_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkNoOpeningBracket, RegexEnum.LINK));
    }

    @Test
    public void no_closing_bracket_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkNoClosingBracket, RegexEnum.LINK));
    }

    @Test
    public void no_opening_parenthesis_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkNoOpeningParenthesis, RegexEnum.LINK));
    }

    @Test
    public void no_closing_parenthesis_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkNoClosingParenthesis, RegexEnum.LINK));
    }

    @Test
    public void first_character_not_opening_bracket_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkFirstCharacterNotOpeningBracket, RegexEnum.LINK));
    }

    @Test
    public void last_character_not_closing_bracket_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkLastCharacterNotClosingBracket, RegexEnum.LINK));
    }

    @Test
    public void first_character_not_opening_parenthesis_link_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidLinkFirstCharacterNotOpeningParenthesis, RegexEnum.LINK)
      );
    }

    @Test
    public void parenthesis_is_before_bracket_link_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidLinkParenthesisIsBeforeBracket, RegexEnum.LINK));
    }
  }

  /**
   * Tests for LinkParenthesesRegex.
   * Tests that the regex matches valid link parentheses and that it does not
   * match invalid link parentheses.
   * {@link RegexEnum#LINK_PARENTHESES}
   */
  @Nested
  public class LinkParenthesesTest {

    private final String validLinkParentheses = "(passage title)";
    private final String validLinkParenthesesAndActionFormat =
      "(passage title)<{ActionType}(actionValue)>";
    private final String invalidLinkParenthesesNoClosingParenthesisBeforeAction =
      "(passage title<{action}()>";
    private final String invalidLinkParenthesesActionIsWrongFormat =
      "(passage title)<(ActionType){actionValue}>";

    @Test
    public void valid_link_parentheses_matches_regex() {
      assertTrue(RegexEnum.matches(validLinkParentheses, RegexEnum.LINK_PARENTHESES));
    }

    @Test
    public void valid_link_parentheses_and_action_format_matches_regex() {
      assertTrue(
        RegexEnum.matches(validLinkParenthesesAndActionFormat, RegexEnum.LINK_PARENTHESES)
      );
    }

    @Test
    public void invalid_link_parentheses_no_closing_parenthesis_before_action_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(
          invalidLinkParenthesesNoClosingParenthesisBeforeAction,
          RegexEnum.LINK_PARENTHESES
        )
      );
    }

    @Test
    public void invalid_link_parentheses_action_is_wrong_format_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidLinkParenthesesActionIsWrongFormat, RegexEnum.LINK_PARENTHESES)
      );
    }
  }

  /**
   * Tests for LinkContainsActionsRegex.
   * Tests that the regex matches valid links that contain actions and that it
   * does not match invalid links that contain actions.
   * {@link RegexEnum#CONTAINS_ACTIONS}
   */
  @Nested
  public class LinkContainsActionsRegexTest {

    private final String validContainsActions = ")<{ActionType}(actionValue)>";
    private final String validContainsMultipleActions =
      ")<{ActionType}(actionValues)|{ActionType}(actionValues)>";
    private final String invalidContainsActionsNoClosingLink = "<{ActionType}(actionValue)>";
    private final String invalidContainsActionsNoClosingTag = ")<{ActionType}(actionValue)";
    private final String invalidContainsActionsNoOpeningTag = "){ActionType}(actionValue)>";

    @Test
    public void valid_contains_actions_matches_regex() {
      assertTrue(RegexEnum.matches(validContainsActions, RegexEnum.CONTAINS_ACTIONS));
    }

    @Test
    public void valid_contains_multiple_actions_matches_regex() {
      assertTrue(RegexEnum.matches(validContainsMultipleActions, RegexEnum.CONTAINS_ACTIONS));
    }

    @Test
    public void invalid_contains_actions_no_closing_link_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidContainsActionsNoClosingLink, RegexEnum.CONTAINS_ACTIONS)
      );
    }

    @Test
    public void invalid_contains_actions_no_closing_tag_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidContainsActionsNoClosingTag, RegexEnum.CONTAINS_ACTIONS)
      );
    }

    @Test
    public void invalid_contains_actions_no_opening_tag_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidContainsActionsNoOpeningTag, RegexEnum.CONTAINS_ACTIONS)
      );
    }
  }

  /**
   * Tests for LinkRegex.
   * Tests that the regex matches valid links and that it does not match
   * invalid links.
   * {@link RegexEnum#LINK}
   */
  @Nested
  public class ActionRegexTest {

    private final String validAction = "{ActionType}(ActionValue)";
    private final String invalidActionNoOpeningBracket = "ActionType}(ActionValue)";
    private final String invalidActionNoClosingBracket = "{ActionType(ActionValue)";
    private final String invalidActionNoOpeningParenthesis = "{ActionType}ActionValue)";
    private final String invalidActionNoClosingParenthesis = "{ActionType}(ActionValue";
    private final String invalidActionFirstCharacterNotOpeningBracket = "A{ctionType}(ActionValue)";
    private final String invalidActionLastCharacterNotClosingBracket = "{ActionType}(ActionValue)A";
    private final String invalidActionFirstCharacterNotOpeningParenthesis =
      "{ActionType}A(ActionValue)";
    private final String invalidActionParenthesisIsBeforeBracket = "{ActionType(}ActionValue)";

    @Test
    public void valid_action_matches_regex() {
      assertTrue(RegexEnum.matches(validAction, RegexEnum.ACTION));
    }

    @Test
    public void no_opening_bracket_action_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidActionNoOpeningBracket, RegexEnum.ACTION));
    }

    @Test
    public void no_closing_bracket_action_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidActionNoClosingBracket, RegexEnum.ACTION));
    }

    @Test
    public void no_opening_parenthesis_action_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidActionNoOpeningParenthesis, RegexEnum.ACTION));
    }

    @Test
    public void no_closing_parenthesis_action_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidActionNoClosingParenthesis, RegexEnum.ACTION));
    }

    @Test
    public void first_character_not_opening_bracket_action_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidActionFirstCharacterNotOpeningBracket, RegexEnum.ACTION)
      );
    }

    @Test
    public void last_character_not_closing_bracket_action_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidActionLastCharacterNotClosingBracket, RegexEnum.ACTION));
    }

    @Test
    public void first_character_not_opening_parenthesis_action_does_not_match_regex() {
      assertFalse(
        RegexEnum.matches(invalidActionFirstCharacterNotOpeningParenthesis, RegexEnum.ACTION)
      );
    }

    @Test
    public void parenthesis_is_before_bracket_action_does_not_match_regex() {
      assertFalse(RegexEnum.matches(invalidActionParenthesisIsBeforeBracket, RegexEnum.ACTION));
    }
  }

  /**
   * Tests for the matches method.
   * Tests that the matches method throws a NullPointerException when the
   * string is null.
   * {@link RegexEnum#matches(String, RegexEnum)}
   */
  @Nested
  public class TestMatchesException {

    private final String nullString = null;

    @Test
    public void null_string_throws_exception() {
      assertThrows(
        NullPointerException.class,
        () -> RegexEnum.matches(nullString, RegexEnum.PASSAGE_TITLE)
      );
    }
  }
}
