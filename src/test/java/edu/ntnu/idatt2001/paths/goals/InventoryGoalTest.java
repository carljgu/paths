package edu.ntnu.idatt2001.paths.goals;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.goals.InventoryGoal;
import edu.ntnu.idatt2001.paths.models.player.Player;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 *
 * Tests for InventoryGoal.
 * These tests make sure that the goal is fulfilled correctly and that it
 * throws the correct exceptions when it is given invalid arguments.
 * We also test that the goal is fulfilled correctly when the player has
 * several copies of the mandatory items.
 * We also test that the goal is fulfilled correctly when the goal checks
 * for untrimmed (as in {@link String#trim()}) items.
 *
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InventoryGoalTest {

  /**
   * Tests for InventoryGoal constructor.
   * Tests that the constructor cannot have null or empty list of items.
   * We also test that the constructor cannot have null items in the list.
   * We also test that the constructor cannot have blank items in the list.
   */
  @Nested
  public class InventoryGoalConstructorTest {

    @Test
    public void cannot_have_null_items_in_list() {
      List<String> goals = new ArrayList<>();
      goals.add(null);
      assertThrows(NullPointerException.class, () -> new InventoryGoal(goals));
    }

    @Test
    public void cannot_have_empty_list() {
      List<String> goals = new ArrayList<>();
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(goals));
    }

    @Test
    public void cannot_have_null_list() {
      assertThrows(NullPointerException.class, () -> new InventoryGoal(null));
    }

    @Test
    public void cannot_have_blank_items_in_list() {
      List<String> goals = new ArrayList<>();
      goals.add("  ");
      assertThrows(IllegalArgumentException.class, () -> new InventoryGoal(goals));
    }
  }

  /**
   * Tests for {@link InventoryGoal#isFulfilled(Player)}.
   * Tests that the method cannot have null player.
   * We also test that the method returns false when the player does not have
   * all the mandatory items.
   * We also test that the method returns true when the player has all the
   * mandatory items, but also has some extra items.
   * We also test that the method returns true when the player has all the
   * mandatory items.
   */
  @Nested
  public class InventoryGoalFulfilledTest {

    @Test
    public void cannot_have_null_player() {
      assertThrows(
        NullPointerException.class,
        () -> new InventoryGoal(List.of("Item 1")).isFulfilled(null)
      );
    }

    @Test
    public void is_fulfilled_when_player_has_all_mandatory_items() {
      List<String> items = List.of("Item 1", "Item 2");
      Player player = new Player("Name", 1, 1, 1, items);
      InventoryGoal goal = new InventoryGoal(items);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_player_has_several_copies_of_all_mandatory_items() {
      List<String> items = List.of("Item 1", "Item 2");
      Player player = new Player("Name", 1, 1, 1, items);
      for (String item : items) {
        player.addToInventory(item);
      }
      InventoryGoal goal = new InventoryGoal(items);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_inventory_goal_checks_for_untrimmed_items() {
      Player player = new Player("Name", 1, 1, 1, List.of("Item 1", "Item 2"));
      InventoryGoal goal = new InventoryGoal(List.of(" Item 1 ", " Item 2 "));
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_inventory_goal_checks_for_items_with_different_case() {
      Player player = new Player("Name", 1, 1, 1, List.of("Item 1", "Item 2"));
      InventoryGoal goal = new InventoryGoal(List.of("item 1", "item 2"));
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_player_inventory_has_untrimmed_and_uppercase_items() {
      Player player = new Player("Name", 1, 1, 1, List.of("ITEM 1", "ITEM 2"));
      InventoryGoal goal = new InventoryGoal(List.of(" Item 1 ", " Item 2 "));
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_not_fulfilled_when_player_only_has_one_of_the_mandatory_items() {
      Player player = new Player("Name", 1, 1, 1, List.of("Item 1"));
      List<String> items = List.of("Item 1", "Item 2");
      InventoryGoal goal = new InventoryGoal(items);
      assertFalse(goal.isFulfilled(player));
    }
  }

  @Nested
  public class InventoryGoalGetCriteriaTest {

    @Test
    public void get_criteria_returns_the_same_criteria_as_given() {
      List<String> items = List.of("Item 1", "Item 2");
      InventoryGoal goal = new InventoryGoal(items);
      assertTrue(goal.getFulfillmentCriteria().containsAll(items));
    }
  }
}
