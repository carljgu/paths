package edu.ntnu.idatt2001.paths.goals;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.goals.GoldGoal;
import edu.ntnu.idatt2001.paths.models.goals.HealthGoal;
import edu.ntnu.idatt2001.paths.models.goals.MaximumScoreGoal;
import edu.ntnu.idatt2001.paths.models.goals.MinimumScoreGoal;
import edu.ntnu.idatt2001.paths.models.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Tests for the classes - GoldGoal, HealthGoal, MaximumScoreGoal and MinimumScoreGoal.
 * These tests make sure that the goal is fulfilled correctly and that it
 * throws the correct exceptions when it is given invalid arguments.
 * We also test that the constructor cannot have negative values where relevant.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ScoreGoldHealthGoalTest {

  private Player player;
  private final int startHealth = 100;
  private final int startScore = 100;
  private final int startGold = 100;

  @BeforeEach
  public void setUp() {
    player = new Player("Test", startHealth, startScore, startGold);
  }

  /**
   * Tests for GoldGoal.
   * Tests that the goal cannot have null player, and that
   * the gold cannot be negative.
   * We also test that the goal is fulfilled correctly by checking that the
   * goal is fulfilled when the player has more than the minimum gold and that
   * the goal is not fulfilled before the player has more than the minimum gold.
   */
  @Nested
  public class HealthGoalTest {

    @Test
    public void cannot_have_negative_minimum_health() {
      assertThrows(IllegalArgumentException.class, () -> new HealthGoal(-1));
    }

    @Test
    public void cannot_have_one_minimum_health() {
      assertThrows(IllegalArgumentException.class, () -> new HealthGoal(1));
    }

    @Test
    public void can_have_two_minimum_health() {
      assertDoesNotThrow(() -> new HealthGoal(2));
    }

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new HealthGoal(2).isFulfilled(null));
    }

    @Test
    public void goal_is_fulfilled_when_player_has_more_than_minimum_health() {
      HealthGoal goal = new HealthGoal(startHealth - 2);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void goal_is_not_fulfilled_before_player_has_more_than_minimum_health() {
      HealthGoal goal = new HealthGoal(startHealth + 2);
      assertFalse(goal.isFulfilled(player));
      player.addHealth(5);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_player_has_the_minimum_health() {
      HealthGoal goal = new HealthGoal(startHealth);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void get_health_fulfillment_criteria_returns_the_same_as_inputted() {
      int minimumHealth = 2;
      HealthGoal goal = new HealthGoal(minimumHealth);
      assertTrue(goal.getFulfillmentCriteria() == minimumHealth);
    }
  }

  /**
   * Tests for GoldGoal.
   * Tests that the goal cannot have null player, and that
   * the gold cannot be smaller than one.
   * We also test that the goal is fulfilled correctly by checking that the
   * goal is fulfilled when the player has more than the minimum gold and that
   * the goal is not fulfilled before the player has more than the minimum gold.
   */
  @Nested
  public class GoldGoalTest {

    @Test
    public void can_have_goal_with_minimum_gold_as_one() {
      assertDoesNotThrow(() -> new GoldGoal(1));
    }

    @Test
    public void cannot_have_goal_with_minimum_gold_as_zero() {
      assertThrows(IllegalArgumentException.class, () -> new GoldGoal(0));
    }

    @Test
    public void cannot_have_negative_minimum_gold() {
      assertThrows(IllegalArgumentException.class, () -> new GoldGoal(-1));
    }

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new GoldGoal(2).isFulfilled(null));
    }

    @Test
    public void is_fulfilled_when_player_has_equal_to_the_minimum_gold() {
      GoldGoal goal = new GoldGoal(startGold);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_player_has_more_than_the_minimum_gold() {
      GoldGoal goal = new GoldGoal(startGold - 2);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void get_gold_fulfillment_criteria_returns_the_same_as_inputted() {
      int minimumGold = 2;
      GoldGoal goal = new GoldGoal(minimumGold);
      assertTrue(goal.getFulfillmentCriteria() == minimumGold);
    }
  }

  /**
   * Tests for MinimumScoreGoal.
   * Tests that the goal cannot have null player.
   * We also test that the goal is fulfilled correctly by checking that the
   * goal is fulfilled when the player has more than the minimum score and that
   * the goal is not fulfilled before the player has more than the minimum score.
   */
  @Nested
  public class MinimumScoreGoalTest {

    @Test
    public void can_have_negative_minimum_score_goal() {
      assertDoesNotThrow(() -> new MinimumScoreGoal(-1));
    }

    @Test
    public void can_have_positive_minimum_score_goal() {
      assertDoesNotThrow(() -> new MinimumScoreGoal(1));
    }

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new MinimumScoreGoal(2).isFulfilled(null));
    }

    @Test
    public void minimum_score_is_fulfilled_when_player_has_equal_to_the_minimum_score() {
      MinimumScoreGoal goal = new MinimumScoreGoal(startScore);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void minimum_score_is_fulfilled_when_player_has_more_than_the_minimum_score() {
      MinimumScoreGoal goal = new MinimumScoreGoal(startScore - 5);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_not_fulfilled_when_player_has_less_than_minimum_score() {
      MinimumScoreGoal goal = new MinimumScoreGoal(startScore + 5);
      assertFalse(goal.isFulfilled(player));
    }

    @Test
    public void get_minimum_score_fulfillment_criteria_returns_the_same_as_inputted() {
      int minimumScore = 2;
      MinimumScoreGoal goal = new MinimumScoreGoal(minimumScore);
      assertTrue(goal.getFulfillmentCriteria() == minimumScore);
    }
  }

  /**
   * Tests for MaximumScoreGoal.
   * Tests that the goal cannot have null player.
   * We also test that the goal is fulfilled correctly by checking that the
   * goal is fulfilled when the player has more than the minimum score and that
   * the goal is not fulfilled before the player has more than the minimum score.
   */
  @Nested
  public class MaximumScoreGoalTest {

    @Test
    public void can_have_negative_maximum_score_goal() {
      assertDoesNotThrow(() -> new MaximumScoreGoal(-1));
    }

    @Test
    public void can_have_positive_maximum_score_goal() {
      assertDoesNotThrow(() -> new MaximumScoreGoal(1));
    }

    @Test
    public void cannot_have_null_player() {
      assertThrows(NullPointerException.class, () -> new MaximumScoreGoal(2).isFulfilled(null));
    }

    @Test
    public void is_fulfilled_when_player_has_equal_to_the_maximum_score() {
      MaximumScoreGoal goal = new MaximumScoreGoal(startScore);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_fulfilled_when_player_has_less_than_the_maximum_score() {
      MaximumScoreGoal goal = new MaximumScoreGoal(startScore + 5);
      assertTrue(goal.isFulfilled(player));
    }

    @Test
    public void is_not_fulfilled_when_player_has_more_than_maximum_score() {
      MaximumScoreGoal goal = new MaximumScoreGoal(startScore - 5);
      assertFalse(goal.isFulfilled(player));
    }

    @Test
    public void get_maximum_score_fulfillment_criteria_returns_the_same_as_inputted() {
      int maximumScore = 2;
      MaximumScoreGoal goal = new MaximumScoreGoal(maximumScore);
      assertTrue(goal.getFulfillmentCriteria() == maximumScore);
    }
  }
}
