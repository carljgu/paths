package edu.ntnu.idatt2001.paths;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.paths.models.Link;
import edu.ntnu.idatt2001.paths.models.Passage;
import edu.ntnu.idatt2001.paths.models.Story;
import java.util.HashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for the Story class.
 * Tests the constructor and the methods of the class.
 */
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class StoryTest {

  /**
   * Tests for the constructor of the Story class.
   * Test that the constructor throws the correct exceptions.
   * Test that the constructor sets the correct values.
   */
  @Nested
  public class StoryConstructorTest {

    private Passage passage;

    @BeforeEach
    public void setUp() {
      passage = new Passage("new valid passage");
    }

    @Test
    public void story_cannot_be_instantiated_with_null_title() {
      assertThrows(NullPointerException.class, () -> new Story(null, passage));
    }

    @Test
    public void story_cannot_be_instantiated_with_whitespace_title() {
      assertThrows(IllegalArgumentException.class, () -> new Story("   ", passage));
    }

    @Test
    public void story_cannot_be_instantiated_with_null_passages() {
      assertThrows(NullPointerException.class, () -> new Story("new valid title", null, passage));
    }

    @Test
    public void story_can_be_instantiated_with_empty_passages() {
      new Story("new story", new HashMap<Link, Passage>(), passage);
      assertTrue(true);
    }

    @Test
    public void story_can_be_instantiated_with_null_opening_passage() {
      assertDoesNotThrow(() -> new Story("new story", new HashMap<Link, Passage>(), null));
    }

    @Test
    public void story_instantiated_without_passages_has_map_of_only_opening_passage() {
      Story story = new Story("new story", passage);
      assertNotNull(story.getPassages());
      assertEquals(1, story.getPassages().size());
    }
  }

  /**
   * Test the equals method of the Story class.
   * Test that two stories are not equal if they have the same title and or
   * passage.
   */
  @Nested
  public class StoryEqualsTest {

    @Test
    public void two_stories_are_not_equal_if_they_have_different_titles() {
      Passage passage = new Passage("new valid passage");
      Story story1 = new Story("new story", passage);
      Story story2 = new Story("new story 2", passage);
      assertNotEquals(story1, story2);
    }

    @Test
    public void two_stories_are_not_equal_even_if_they_have_equal_titles_and_passage() {
      Passage passage = new Passage("new valid passage");
      String storyName = "new story";
      Story story1 = new Story(storyName, passage);
      Story story2 = new Story(storyName, passage);
      assertNotEquals(story1, story2);
    }
  }

  /**
   * Test the addPassage method of the Story class.
   * Test that the method throws the correct exceptions.
   * Test that the method adds the passage to the map of passages.
   */
  @Nested
  public class StoryAddPassageTest {

    @Test
    public void adding_passage_creates_link_with_passage_title_as_text_and_reference_in_passages_map() {
      String passageName = "new valid passage";
      String passageName2 = "new valid passage 2";
      Passage passage = new Passage(passageName);
      Passage passage2 = new Passage(passageName2);
      Story story = new Story("new story", passage);
      story.addPassage(passage2);
      assertEquals(2, story.getPassages().size());
      assertTrue(story.getPassages().contains(passage2));
      assertEquals(passage2, story.getPassage(new Link(passageName2, passageName2)));
    }

    @Test
    public void getting_passage_after_adding_passage_checks_link_equality_and_not_text() {
      String openingPassageName = "opening valid passage";
      String passageName = "new valid passage";
      Passage openingPassage = new Passage(openingPassageName);
      Passage passage = new Passage(passageName);
      Story story = new Story("A new story", openingPassage);
      story.addPassage(passage);
      assertEquals(2, story.getPassages().size());
      assertTrue(story.getPassages().contains(passage));
      assertNull(story.getPassage(new Link(passageName, "not the same value")));
    }

    @Test
    public void adding_null_passage_throws_exception() {
      Story story = new Story("new story", new Passage("opening passage"));
      assertThrows(NullPointerException.class, () -> story.addPassage(null));
    }

    @Test
    public void adding_same_passage_object_throws_exception() {
      String passageName = "new valid passage";
      Passage passage = new Passage(passageName);
      Story story = new Story("new story", passage);
      assertThrows(IllegalArgumentException.class, () -> story.addPassage(passage));
    }

    @Test
    public void adding_passage_with_duplicate_title_throws_exception() {
      String passageName = "new valid passage";
      Passage passage = new Passage(passageName);
      Passage passage2 = new Passage(passageName);
      Story story = new Story("new story", passage);
      assertThrows(IllegalArgumentException.class, () -> story.addPassage(passage2));
    }
  }

  /**
   * Test if a story is playable.
   * Test that a story is playable if it has an opening passage and that passage
   * is in passages.
   * Test that a story is not playable if it has no opening passage.
   */
  @Nested
  public class PlayableStoryTest {

    @Test
    public void story_is_playable_if_it_has_an_opening_passage_and_that_passage_is_in_passages() {
      Passage passage = new Passage("new valid passage");
      Story story = new Story("new story", passage);
      assertTrue(story.isPlayable());
    }

    @Test
    public void story_is_not_playable_if_it_has_no_opening_passage_in_passages() {
      Story story = new Story("new story", new Passage("opening passage"));
      story.removePassage(
        new Link(story.getOpeningPassage().getTitle(), story.getOpeningPassage().getTitle())
      );
      assertFalse(story.isPlayable());
    }
  }

  /**
   * Test the removePassage method of the Story class.
   * Test that the method throws the correct exceptions.
   * Test that the method removes the passage from the map of passages.
   */
  @Nested
  public class RemovePassageTest {

    @Test
    public void remove_passage_that_does_exist_returns_true() {
      Passage passage = new Passage("new valid passage");
      Story story = new Story("new story", passage);
      assertTrue(story.removePassage(new Link("new valid passage", "new valid passage")));
    }

    @Test
    public void remove_passasage_that_does_not_exist_returns_false() {
      Passage passage = new Passage("new valid passage");
      Link link = new Link("new valid link", "new valid link");
      Story story = new Story("new story", passage);
      assertFalse(story.removePassage(link));
    }

    @Test
    public void remove_passage_with_null_link_throws_exception() {
      Passage passage = new Passage("new valid passage");
      Story story = new Story("new story", passage);
      assertThrows(NullPointerException.class, () -> story.removePassageWithPassage(null));
    }

    @Test
    public void remove_passage_that_references_other_passages() {
      Passage passage = new Passage("new valid passage");
      Passage passage2 = new Passage("new valid passage 2");
      Passage passage3 = new Passage("new valid passage 3");

      passage.addLink(new Link("new valid link", "new valid link"));
      passage2.addLink(new Link("new valid link", "new valid passage"));
      passage3.addLink(new Link("new valid link", "new valid passage"));
      Story story = new Story("new story", passage);
      story.addPassage(passage2);
      story.addPassage(passage3);

      assertFalse(story.removePassage(new Link("new valid passage", "new valid passage")));
      assertEquals(3, story.getPassages().size());
      assertTrue(story.getPassages().contains(passage));
      assertTrue(story.getPassages().contains(passage2));
      assertTrue(story.getPassages().contains(passage3));
    }

    @Test
    public void remove_passage_that_is_not_referenced_by_other_passages() {
      Passage passage = new Passage("new valid passage");
      Passage passage2 = new Passage("new valid passage 2");
      Passage passage3 = new Passage("new valid passage 3");

      passage.addLink(new Link("new valid link", "new valid link"));
      passage2.addLink(new Link("new valid link", "new valid passage"));
      passage3.addLink(new Link("new valid link", "new valid passage"));
      Story story = new Story("new story", passage);
      story.addPassage(passage2);
      story.addPassage(passage3);

      assertTrue(story.removePassage(new Link("new valid passage 2", "new valid passage 2")));
      assertEquals(2, story.getPassages().size());
      assertTrue(story.getPassages().contains(passage));
      assertFalse(story.getPassages().contains(passage2));
      assertTrue(story.getPassages().contains(passage3));
    }
  }

  /**
   * Test the getBrokenLinks method of the Story class.
   * Test that the method returns the correct number of broken links.
   * Test that the method returns the correct number of broken links when there
   * are no broken links.
   */
  @Nested
  public class GetBrokenLinksTest {

    @Test
    public void get_broken_links_returns_broken_links_if_there_are_broken_links() {
      Passage passage = new Passage("new valid passage");
      Passage passage2 = new Passage("new valid passage 2");
      Passage passage3 = new Passage("new valid passage 3");

      // Broken link
      passage.addLink(new Link("new valid link", "new valid link"));

      passage2.addLink(new Link("new valid link", "new valid passage"));
      passage3.addLink(new Link("new valid link", "new valid passage"));
      Story story = new Story("new story", passage);
      story.addPassage(passage2);
      story.addPassage(passage3);

      assertEquals(story.getBrokenLinks().size(), 1);
    }

    @Test
    public void get_broken_links_returns_empty_list_if_there_are_no_broken_links() {
      Passage passage = new Passage("new valid passage");
      Passage passage2 = new Passage("new valid passage 2");
      Passage passage3 = new Passage("new valid passage 3");

      passage.addLink(new Link("new valid link", "new valid passage 2"));
      passage2.addLink(new Link("new valid link", "new valid passage"));
      passage3.addLink(new Link("new valid link", "new valid passage"));
      Story story = new Story("new story", passage);
      story.addPassage(passage2);
      story.addPassage(passage3);

      assertTrue(story.getBrokenLinks().isEmpty());
    }

    @Test
    public void get_broken_links_returns_empty_list_if_there_are_no_links() {
      Passage passage = new Passage("new valid passage");
      Passage passage2 = new Passage("new valid passage 2");
      Passage passage3 = new Passage("new valid passage 3");

      Story story = new Story("new story", passage);
      story.addPassage(passage2);
      story.addPassage(passage3);

      assertTrue(story.getBrokenLinks().isEmpty());
    }

    @Test
    public void get_broken_links_returns_empty_list_if_there_are_no_passages() {
      Passage passage = new Passage("new valid passage");
      Story story = new Story("new story", passage);
      story.removePassage(new Link("new valid passage", "new valid passage"));
      assertTrue(story.getBrokenLinks().isEmpty());
    }
  }
}
