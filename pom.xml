<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>edu.ntnu.idatt2001</groupId>
    <artifactId>paths</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>paths</name>
    <packaging>jar</packaging>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>

    <dependencies>
        <!-- JavaFX dependencies -->
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
            <version>20-ea+9</version>
        </dependency>

        <!-- Test dependencies -->
        <!-- Dependency for for Junit 5 -->
        <!-- This is used to write and run unit tests -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>5.9.1</version>
            <scope>test</scope>
        </dependency>

        <!-- Dependency for Mockito -->
        <!-- This is used to mock objects in unit tests -->
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <version>5.3.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <!-- Source directories -->
        <!-- Used to tell shade plugin where to find files -->
        <!-- This is where the java files are located -->
        <sourceDirectory>src/main/java</sourceDirectory>
        <!-- This is where the resource files are located -->
        <resources>
            <resource>
                <directory>src/main/resources</directory>
            </resource>
        </resources>
        <!-- This is where the test files are located -->
        <testSourceDirectory>src/test/java</testSourceDirectory>
        <plugins>
            <!-- Maven compiler plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.10.1</version>
                <configuration>
                    <source>17</source>
                    <target>17</target>
                </configuration>
            </plugin>
            <!-- Maven surefire plugin -->
            <!-- This is used to run unit tests -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M7</version>
            </plugin>
            <!-- Maven javadoc plugin -->
            <!-- This is used to generate javadoc -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.4.1</version>
            </plugin>
            <!-- Maven shade plugin -->
            <!-- This is used to create a jar file with all dependencies -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <!-- Define when shade:shade is to be executed -->
                    <!-- In this case it is executed when we run: mvn package -->
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <!-- Define the configurations for the shade plugin -->
                        <configuration>
                            <!-- This is used to create a manifest file with the main class -->
                            <transformers>
                                <transformer
                                    implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>edu.ntnu.idatt2001.paths.Main</mainClass>
                                </transformer>
                            </transformers>
                            <!-- This is to tell shade not to autogenerate a pom file with reduced dependencies -->
                            <createDependencyReducedPom>
                                false
                            </createDependencyReducedPom>
                            <!-- This is to give the generated jar file the name of the project -->
                            <finalName>
                                ${project.name}
                            </finalName>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Maven prettier plugin -->
            <!-- This is used to format and check the formatting of java files -->
            <plugin>
                <groupId>com.hubspot.maven.plugins</groupId>
                <artifactId>prettier-maven-plugin</artifactId>
                <version>0.19</version>
                <!-- We have decided to follow the google java style guide -->
                <!-- https://google.github.io/styleguide/javaguide.html -->
                <configuration>
                    <prettierJavaVersion>2.0.0</prettierJavaVersion>
                    <printWidth>100</printWidth>
                    <tabWidth>2</tabWidth>
                    <useTabs>false</useTabs>
                    <ignoreConfigFile>true</ignoreConfigFile>
                    <ignoreEditorConfig>true</ignoreEditorConfig>
                    <inputGlobs>
                        <inputGlob>src/main/java/**/*.java</inputGlob>
                        <inputGlob>src/test/java/**/*.java</inputGlob>
                    </inputGlobs>
                </configuration>
            </plugin>

            <!-- Maven javafx plugin -->
            <!-- This is used to run the javafx application -->
            <plugin>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-maven-plugin</artifactId>
                <version>0.0.8</version>
                <executions>
                    <execution>
                        <!-- Default configuration for running with: mvn clean javafx:run -->
                        <id>default-cli</id>
                        <configuration>
                            <!-- This is why we have the file Main.java in the folder paths -->
                            <!-- It serves no purpose other than to JavaFX work -->
                            <mainClass>
                                edu.ntnu.idatt2001.paths/edu.ntnu.idatt2001.paths.Main
                            </mainClass>
                            <launcher>app</launcher>
                            <jlinkZipName>app</jlinkZipName>
                            <jlinkImageName>app</jlinkImageName>
                            <noManPages>true</noManPages>
                            <stripDebug>true</stripDebug>
                            <noHeaderFiles>true</noHeaderFiles>

                            <release>${maven.compiler.target}</release>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- Maven jacoco plugin -->
            <!-- This is used to generate a test coverage report -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.10</version>
                <configuration>
                    <!-- We have decided to exclude the views and exceptions packages from the test
                    coverage report -->
                    <!-- This is because we don't want to test the views and exceptions packages -->
                    <excludes>
                        <exclude>edu/ntnu/idatt2001/paths/views/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/views/**/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/exceptions/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/controllers/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/controllers/storyeditor/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/controllers/playgame/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/controllers/startmenu/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/controllers/storyimporter/*</exclude>
                        <exclude>edu/ntnu/idatt2001/paths/controllers/storystarter/*</exclude>
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <!-- Maven reporting -->
    <!-- This is used to generate a test coverage report -->
    <reporting>
        <plugins>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.10</version>
            </plugin>
        </plugins>
    </reporting>

</project>